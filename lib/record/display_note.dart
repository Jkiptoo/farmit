
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';


class DisplayNote extends StatefulWidget {
  const DisplayNote({Key? key}) : super(key: key);

  @override
  State<DisplayNote> createState() => _DisplayNoteState();
}

class _DisplayNoteState extends State<DisplayNote> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightGreen[900],
        title: const Text('View Note'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: getOwnedDocumentsStream(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeNote = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map a = documentSnapshot.data() as Map<String, dynamic>;
            storeNote.add(a);
            a['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (var i = 0; i < storeNote.length; i++) ...[

                    SizedBox(
                      width: double.infinity,
                      child: Material(
                        child: InkWell(
                          onTap: (){
                            setState(() {
                              isExpanded = !isExpanded;
                            });
                          },
                            child: Card(
                              elevation: 2,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8.0), // Adjust the border radius as per your requirement
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(0.0), // Adjust the spacing as per your requirement
                                child: ListTile(
                                  leading:  const Image(height: 25, image: AssetImage('assets/icons/records.png'),),
                                  title: Text(
                                    storeNote[i]['note'],
                                    style: TextStyle(
                                      color: Colors.grey[800],
                                      fontSize: 20,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    overflow: TextOverflow.ellipsis,
                                    maxLines: isExpanded ? null : 2,
                                  ),

                                  trailing: Icon(
                                    isExpanded ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                                  ),
                                ),
                              ),
                            )
                        ),
                      ),
                    )
                  ]
                ],
              ),
            ),
          );
        },
      ),

    );
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('farms');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('note');

    Query<Map<String, dynamic>> query =
    subcollectionRef
        .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the stream of documents in the subcollection
    return query.snapshots();
  }

}
