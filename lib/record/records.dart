import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/record/display_note.dart';

class FarmNote extends StatefulWidget {
  final String? id;
  const FarmNote({Key? key, required this.id}) : super(key: key);

  @override
  State<FarmNote> createState() => _FarmNoteState();
}

class _FarmNoteState extends State<FarmNote> {
  final _formKey = GlobalKey<FormState>();
  final _noteController = TextEditingController();

  CollectionReference<Map<String, dynamic>> farm = FirebaseFirestore.instance
      .collection('farm')
      .doc('farms')
      .collection('farm-data');

  String farmName = '';

  clearForm() {
    _noteController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.of(context)
                .pushNamedAndRemoveUntil('/home', (route) => false);
          },
          icon: const Icon(Icons.arrow_back),
        ),
        title: const Text(
          'Add Note',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
        ),
        actions: [
          IconButton(
              onPressed: () {
                if (_formKey.currentState != null) {
                  if (_formKey.currentState!.validate()) {
                    var collectionReference = FirebaseFirestore.instance
                        .collection('farm')
                        .doc('farms')
                        .collection('note')
                        .doc();
                    var documentReference =
                        collectionReference; // Generate a new document reference

                    documentReference.set({
                      'rid': FirebaseAuth.instance.currentUser?.uid,
                      'documentId': documentReference.id,
                      // Include the document ID in the document data
                      'farm': farmName,
                      'note': _noteController.text,
                    }).then((value) {
                      clearForm();
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text('Note added successfully')));
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const DisplayNote()));
                    });
                  }
                }
              },
              icon: const Icon(Icons.check_box_outlined, size: 30)),
        ],
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
              future: FirebaseFirestore.instance
                  .collection('farm')
                  .doc('farms')
                  .collection('farm-data')
                  .doc(widget.id)
                  .get(),
              builder: (_, snapshot) {
                if (snapshot.hasError) {
                  return const Text('Something Went Wrong');
                }
                if (snapshot.connectionState == ConnectionState.waiting) {
                  return const CircularProgressIndicator();
                }
                var data = snapshot.data!.data();
                farmName = data!['name'];

                return Column(children: [
                  TextFormField(
                    onChanged: (val) {
                      setState(() {
                        farmName = val;
                      });
                    },
                    readOnly: true,
                    initialValue: farmName,
                    validator: (value) {
                      if (value == null ||
                          !RegExp(r'^[a-zA-Z ]+$').hasMatch(value)) {
                        return 'Please enter a valid Farm name';
                      }
                      return null;
                    },
                    decoration: const InputDecoration(
                      labelText: 'Farm Name',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  TextFormField(
                    validator: (value) {
                      if (value == null ||
                          !RegExp(r'[a-z A-Z]+$').hasMatch(value)) {
                        return 'Enter valid note';
                      }
                      return null;
                    },
                    controller: _noteController,
                    maxLines: null, // Allow multiple lines
                    decoration: const InputDecoration(
                      labelText: 'Note',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                ]);
              },
            ),
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen[900],
        onPressed: () {
          Navigator.push(context,
              MaterialPageRoute(builder: (context) => const DisplayNote()));
        },
        child: const Icon(Icons.next_plan_outlined),
      ),
    );
  }
}

