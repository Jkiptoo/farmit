import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:lottie/lottie.dart';

import '../farm_dash/home.dart';

class OnBoarding extends StatefulWidget {
  const OnBoarding({Key? key}) : super(key: key);

  @override
  State<OnBoarding> createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: IntroductionScreen(
      animationDuration: 3,
      pages: [

        PageViewModel(
          title: "Shamba app",
          body: 'To the next level with you',
          image: buildLottie('assets/icons/modern-farm.json'),
          decoration: getPageDecoration(),
        ),
        PageViewModel(
          title: "Modern farming",
          body: 'Get your records with a touch of a button',
          image: buildLottie('asset/digital-farming.json'),
          decoration: getPageDecoration(),
        ),
              ],
      done: Text(
        'Explore',
        style: TextStyle(
            fontSize: 25, fontWeight: FontWeight.bold, color: Colors.grey[800]),
      ),
      onDone: () {
        Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => const HomePage()));
      },
      next: const Icon(Icons.arrow_forward),
    ));
  }
}

PageDecoration getPageDecoration() {
  return PageDecoration(
      titleTextStyle:
          const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
      bodyTextStyle: const TextStyle(
        fontSize: 20,
      ),
      bodyPadding: const EdgeInsets.all(16).copyWith(bottom: 0),
      imagePadding: const EdgeInsets.all(24));
}

Widget buildLottie(String path) {
  return Center(
    child: Lottie.asset(
      path,
    ),
  );
}

Widget buildImage(String path) {
  return Center(
    child: Image.asset(
      path,
    ),
  );
}
