import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:meek/authentication/reset_password.dart';
import 'package:meek/authentication/sign_in.dart';
import 'package:meek/authentication/sign_up.dart';
import 'package:meek/board/on_boarding.dart';
import 'package:meek/drawer/category/add_category.dart';
import 'package:meek/drawer/invest/Animal/animal.dart';
import 'package:meek/drawer/invest/crop/add_crop.dart';
import 'package:meek/drawer/invest/farm/data/add_farm.dart';
import 'package:meek/drawer/invest/farm/records/leased_farm.dart';
import 'package:meek/drawer/invest/machinery_page/machinery.dart';
import 'package:meek/farm_dash/home.dart';

void main()async{
  WidgetsFlutterBinding.ensureInitialized();
  runApp(const MyApp());
  Firebase.initializeApp().whenComplete(() {
    // ignore: avoid_print
    print("completed");
  });

}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  GetMaterialApp(
        theme: ThemeData(fontFamily: 'Schyler'),
        debugShowCheckedModeBanner: false,
        home: const OnBoarding(),
        routes: {
          '/home': (context) => const HomePage(),
          '/crop': (context) => const AddCrop(),
          '/farm': (context) => const FarmList(),
          '/machine': (context) => const MachineryApp(),
          '/leased': (context) => const LeasedFarm(),
          '/animal': (context) => const ManageAnimal(),
          '/category': (context) => const AddCategory(),
          '/register': (context) => const SignUp(),
          '/login': (context) => const SignInPage(),
          '/reset': (context) => const ResetPassword(),
        }
    );
  }
}