// ignore_for_file: prefer_typing_uninitialized_variables
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/Animal/animal.dart';
import 'package:meek/drawer/invest/crop/add_crop.dart';
import 'package:meek/drawer/invest/farm/records/activities_summary.dart';
import 'package:meek/drawer/invest/farm/data/add_farm.dart';
import 'package:meek/drawer/invest/farm/records/leased_farm.dart';
import 'package:meek/drawer/invest/farm/records/notes.dart';
import 'package:meek/drawer/invest/machinery_page/machinery.dart';
import 'package:meek/drawer/transaction/expense.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../drawer/category/add_category.dart';
import '../drawer/weather/weather_home.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var firebaseUser = FirebaseAuth.instance.currentUser;
  // Get the current date and time
  DateTime currentDate = DateTime.now();


  int placeCount = 0;

  Future<void> countDocuments() async {
    QuerySnapshot myDoc2 = await FirebaseFirestore.instance
        .collection('crop')
        .doc('crops')
        .collection('crop-data')
        .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid)
        .get();

    List<DocumentSnapshot> myDocCount2 = myDoc2.docs;
    placeCount = myDocCount2.length;
    setState(() {
      placeCount = myDocCount2.length;
    });
  }

  int livestockCount = 0;

  Future<void> countLivestock() async {
    QuerySnapshot myDoc2 =
    await FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get();

    List<DocumentSnapshot> myLivestockCount = myDoc2.docs;
    livestockCount = myLivestockCount.length;
    setState(() {
      livestockCount = myLivestockCount.length;
    });
  }

  int farmNum = 0;

  Future<void> countFarm() async {
    QuerySnapshot myFarm = await FirebaseFirestore.instance
        .collection('farm')
        .doc('farms')
        .collection('farm-data')
        .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid)
        .get();

    List<DocumentSnapshot> myFarmCount = myFarm.docs;
    farmNum = myFarmCount.length;
    setState(() {
      farmNum = myFarmCount.length;
    });
  }

  /*=================================================================================================
                 Function to find sum of a specific field
  =================================================================================================*/
  int total = 0;


  Future<int> calculateTotal() async {

    QuerySnapshot snapshot = await FirebaseFirestore.instance
        .collection('farm').doc('farms').collection('farm-data').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid) // Replace 'your_collection' with the actual collection name
        .get();

    for (var document in snapshot.docs) {
      // Replace 'your_field' with the actual field name you want to calculate the total for
      final fieldValue = document.get('size');
      if (fieldValue != null) {
        total += (fieldValue as int);
      }
    }

    return total;
  }



  CollectionReference<Map<String, dynamic>> collectionReference = FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data');

  Future<int> Function() sumSizeField() {
    return () async {
      int sum = 0;

      QuerySnapshot<Map<String, dynamic>> snapshot =
      await collectionReference.where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get();

      for (var doc in snapshot.docs) {
        int size = doc.data()['size'] ?? 0;
        sum += size;
      }

      return sum;
    };
  }

  double field1Total = 0.0;
  double field2Total = 0.0;
  late SharedPreferences logindata;
  late String username;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    countDocuments();
    countLivestock();
    sumSizeField();
    countFarm();
    initial();
    calculateTotal().then((int calculatedTotal) {
      setState(() {
        total = calculatedTotal;
      });
    }).catchError((error) {
    });
  }

  void initial() async {
    logindata = await SharedPreferences.getInstance();
    setState(() {
      username = logindata.getString('username')!;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.lightGreen[900],
        toolbarHeight: 65,
        title: const Text(
          'FARM MANAGEMENT',
          style: TextStyle(fontSize: 25),
        ),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: getCurrentActivities(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeFarm = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map a = documentSnapshot.data() as Map<String, dynamic>;
            storeFarm.add(a);
            a['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Padding(
                        padding: EdgeInsets.all(16.0),
                        child: Text(
                          'Farm Overview',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                      ),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>const AddCrop()));
                              },
                              child: Card(
                                elevation: 4,
                                color: Colors.white, // Set the background color of the card
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8), // Set border radius for the card
                                  side: const BorderSide(
                                    color: Colors.grey, // Set border color for the card
                                    width: 1, // Set border width for the card
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      const Image(height: 50, image: AssetImage('assets/icons/sprout.png')),
                                      const SizedBox(height: 8),
                                      const Text(
                                        'Crops',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: Text(
                                          'No: $placeCount',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),                            )
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>const ManageAnimal()));
                              },
                              child: Card(
                                elevation: 4,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  side: const BorderSide(
                                    color: Colors.grey,
                                    width: 1,
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      const Image(height: 50, image: AssetImage('assets/icons/animal.png')),
                                      const SizedBox(height: 8),
                                      const Text(
                                        'Livestock',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: Text(
                                          'No: $livestockCount',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context) => const FarmList()));

        },
                              child: Card(
                                elevation: 4,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  side: const BorderSide(
                                    color: Colors.grey,
                                    width: 1,
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    children: [
                                      const Image(height: 50, image: AssetImage('assets/icons/icons8-land-64.png')),
                                      const SizedBox(height: 8),
                                      const Text(
                                        'Farm',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: Text(
                                          'No: $farmNum',
                                          style: const TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Colors.blue,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Expanded(
                            child: Card(
                              elevation: 4,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: const BorderSide(
                                  color: Colors.grey,
                                  width: 1,
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Image(height: 47, image: AssetImage('assets/icons/shovels.png')),
                                    const SizedBox(height: 8),
                                    const Text(
                                      'Acreage',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: FutureBuilder<QuerySnapshot>(
                                        future: FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                          if (snapshot.hasError) {
                                            return const Text("Something went wrong");
                                          }

                                          if (snapshot.connectionState == ConnectionState.waiting) {
                                            return const Text("Loading...");
                                          }

                                          if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                                            return const Text("0.00");
                                          }

                                          double sumTotal = 0.0;
                                          for (var doc in snapshot.data!.docs) {
                                            if (doc["size"] != null) {
                                              sumTotal += double.parse(doc["size"]);
                                            }
                                          }

                                          return Text("$sumTotal");
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: InkWell(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context)=> const LeasedFarm()));
                              },
                              child: Card(
                                elevation: 4,
                                color: Colors.white,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  side: const BorderSide(
                                    color: Colors.grey,
                                    width: 1,
                                  ),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(16.0),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      const Image(height: 48, image: AssetImage('assets/icons/rent.png')),
                                      const SizedBox(height: 8),
                                      const Text(
                                        'Leased',
                                        style: TextStyle(
                                          fontSize: 16,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8),
                                        child: FutureBuilder<QuerySnapshot>(
                                          future: FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data').where('ownership', isEqualTo: 'Lease').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                          builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                            if (snapshot.hasError) {
                                              return const Text("Something went wrong");
                                            }

                                            if (snapshot.connectionState == ConnectionState.waiting) {
                                              return const Text("Loading...");
                                            }

                                            if (!snapshot.hasData || snapshot.data!.docs.isEmpty) {
                                              return const Text("0.00");
                                            }

                                            double sumTotal = 0.0;
                                            for (var doc in snapshot.data!.docs) {
                                              if (doc["size"] != null) {
                                                sumTotal += double.parse(doc["size"]);
                                              }
                                            }

                                            return Text("$sumTotal");
                                          },
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Expanded(
                            child: Card(
                              elevation: 4,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: const BorderSide(
                                  color: Colors.grey,
                                  width: 1,
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Image(height: 30, image: AssetImage('assets/icons/gross.png')),
                                    const SizedBox(height: 8),
                                    const Text(
                                      'Income vs Expense',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: FutureBuilder<QuerySnapshot>(
                                        future: FirebaseFirestore.instance.collection('farm').doc('activities').collection('farm-activity').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1) {
                                          if (snapshot1.hasError) {
                                            return const Text("Something went wrong");
                                          }

                                          if (snapshot1.connectionState == ConnectionState.waiting) {
                                            return const Text("Loading...");
                                          }

                                          if (!snapshot1.hasData || snapshot1.data!.docs.isEmpty) {
                                            return const Text(" ");
                                          }

                                          double sumTotal1 = 0.0;
                                          for (var doc in snapshot1.data!.docs) {
                                            if (doc["expense"] != null) {
                                              sumTotal1 += double.parse(doc["expense"]);
                                            }
                                          }
                                          field1Total = sumTotal1;

                                          return FutureBuilder<QuerySnapshot>(
                                            future: FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data').where('ownership', isEqualTo: 'Lease').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot2){
                                              if(snapshot2.hasError){
                                                return const Center(
                                                  child: Text('Something went wrong'),
                                                );
                                              }
                                              if(snapshot2.connectionState == ConnectionState.waiting){
                                                return const Center(
                                                  child: Text('Loading...'),
                                                );
                                              }
                                              if(!snapshot2.hasData  || snapshot2.data!.docs.isEmpty){
                                                return const Center(
                                                  child: Text(' '),
                                                );
                                              }
                                              double sumTotal2 = 0.0;
                                              for (var doc in snapshot2.data!.docs) {
                                                if(doc['ammount'] != null){
                                                  sumTotal2 += double.parse(doc['ammount']);
                                                }
                                              }
                                              field2Total = sumTotal2;
                                              double sum = field2Total + field1Total;
                                               return FutureBuilder<QuerySnapshot>(
                                                future: FirebaseFirestore.instance.collection('farm').doc('activities').collection('farm-income').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot2){
                                                  if(snapshot2.hasError){
                                                    return const Center(
                                                      child: Text('Something went wrong'),
                                                    );
                                                  }
                                                  if(snapshot2.connectionState == ConnectionState.waiting){
                                                    return const Center(
                                                      child: Text('Loading...'),
                                                    );
                                                  }
                                                  if(!snapshot2.hasData  || snapshot2.data!.docs.isEmpty){
                                                    return const Center(
                                                      child: Text('0.00'),
                                                    );
                                                  }
                                                  double sumTotal2 = 0.0;
                                                  for (var doc in snapshot2.data!.docs) {
                                                    if(doc['cost'] != null){
                                                      sumTotal2 += double.parse(doc['cost']);
                                                    }
                                                  }
                                                  field2Total = sumTotal2;
                                                  double difference = field2Total - sum;
                                                  return Text("$difference");
                                                },
                                              );
                                            },
                                          );
                                        },
                                      )
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),

                        ],
                      ),
                      Row(
                        children: [
                          Expanded(
                            child: Card(
                              elevation: 4,
                              color: Colors.white,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8),
                                side: const BorderSide(
                                  color: Colors.grey,
                                  width: 1,
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(16.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    const Image(height: 47, image: AssetImage('assets/icons/income.png')),
                                    const SizedBox(height: 8),
                                    const Text(
                                      'Expense',
                                      style: TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.bold,
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(8),
                                      child: FutureBuilder<QuerySnapshot>(
                                        future: FirebaseFirestore.instance.collection('farm').doc('activities').collection('farm-activity').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot1) {
                                          if (snapshot1.hasError) {
                                            return const Text("Something went wrong");
                                          }

                                          if (snapshot1.connectionState == ConnectionState.waiting) {
                                            return const Text("Loading...");
                                          }

                                          if (!snapshot1.hasData || snapshot1.data!.docs.isEmpty) {
                                            return const Text("0.00");
                                          }

                                          double sumTotal1 = 0.0;
                                          for (var doc in snapshot1.data!.docs) {
                                            if (doc["expense"] != null) {
                                              sumTotal1 += double.parse(doc["expense"]);
                                            }
                                          }
                                          field1Total = sumTotal1;

                                          return FutureBuilder<QuerySnapshot>(
                                            future: FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data').where('ownership', isEqualTo: 'Lease').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get(),
                                            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot2){
                                              if(snapshot2.hasError){
                                                return const Center(
                                                  child: Text('Something went wrong'),
                                                );
                                              }
                                              if(snapshot2.connectionState == ConnectionState.waiting){
                                                return const Center(
                                                  child: Text('Loading...'),
                                                );
                                              }
                                              if(!snapshot2.hasData  || snapshot2.data!.docs.isEmpty){
                                                return const Center(
                                                  child: Text('0.00'),
                                                );
                                              }
                                              double sumTotal2 = 0.0;
                                              for (var doc in snapshot2.data!.docs) {
                                                if(doc['ammount'] != null){
                                                  sumTotal2 += double.parse(doc['ammount']);
                                                }
                                              }
                                              field2Total = sumTotal2;
                                              double sum = field2Total + field1Total;
                                              return Text("$sum");
                                            },
                                          );
                                        },
                                      ),
                                    ),

                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 20),
                      const Padding(
                        padding: EdgeInsets.symmetric(horizontal: 16.0),
                        child: Text(
                          'Recent Activity',
                          style: TextStyle(
                              fontSize: 24, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  for (var i = 0; i < storeFarm.length; i++) ...[
                    SizedBox(
                      height: 70,
                      width: double.infinity,
                      child: Padding(
                        padding: const EdgeInsets.all(
                            0.0), // Adjust the spacing as per your requirement
                        child: ListTile(
                          leading: _getActivityIcon(storeFarm[i]['activity']),
                          title: Text(
                            storeFarm[i]['activity'],
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          subtitle: Text(
                            storeFarm[i]['farm'],
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                          trailing: Text(
                            storeFarm[i]['date'],
                            style: TextStyle(
                              color: Colors.grey[700],
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ]
                ],
              ),
            ),
          );
        },
      ),
      drawer: SizedBox(
        width: 200,
        child: Drawer(
          child: ListView(
            children: [
              DrawerHeader(
                curve: Curves.slowMiddle,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  image: const DecorationImage(
                    image: AssetImage('asset/image.jpg'),
                    fit: BoxFit.cover,
                  ),
                ),
                child: Container(
                  alignment: Alignment.center,
                  child: Text(
                    'CROP MANAGER',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.grey[500],
                    ),
                  ),
                ),
              ),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/icons8-machine-48.png'),
                ),
                title: const Text('MACHINERY'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const MachineryApp()),
                  );
                },
              ),
              const Divider(),
              // ListTile(
              //   leading: const Image(
              //     height: 25,
              //     image: AssetImage('assets/icons/expense.png'),
              //   ),
              //   title: const Text('INCOME'),
              //   onTap: () {
              //     Navigator.push(
              //       context,
              //       MaterialPageRoute(builder: (context) => const AddIncome()),
              //     );
              //   },
              // ),
              // const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/lease.png'),
                ),
                title: const Text('LEASED'),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const LeasedFarm()));

                },
              ),
              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/animal.png'),
                ),
                title: const Text('ANIMAL'),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => const ManageAnimal()),
                  );
                },
              ),
              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/menu.png'),
                ),
                title: const Text('CATEGORY'),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => const AddCategory()),
                  );
                },
              ),
              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/note.png'),
                ),
                title: const Text('NOTE'),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => const GeneralNote()),
                  );
                },
              ),

              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/cloudy.png'),
                ),
                title: const Text('WEATHER'),
                onTap: () {
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => const WeatherReport()),
                  );
                },
              ),
              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/summary.png'),
                ),
                title: const Text('SUMMARY'),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ActivitiesSummary(id: '')));
                },
              ),

              const Divider(),
              ListTile(
                leading: const Image(
                  height: 25,
                  image: AssetImage('assets/icons/budget.png'),
                ),
                title: const Text('EXPENSE'),
                onTap: () {
                  Navigator.push(context, MaterialPageRoute(builder: (context)=> const ExpenseApp()));
                },
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedLabelStyle: const TextStyle(
          fontSize: 20,
        ),
        selectedItemColor: Colors.lightGreen[900],
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Material(
              child: InkWell(
                  onTap: () {},
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: const [
                      Image(
                        height: 45,
                        image: AssetImage('image/menu.png'),
                      ),
                      Text('Home')
                    ],
                  )),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Material(
              child: InkWell(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const AddCrop()));
                  },
                  child: Column(
                    children: const [
                      Image(
                        height: 45,
                        image: AssetImage('assets/icons/sprout.png'),
                      ),
                      Text('Crop')
                    ],
                  )),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
              icon: Material(
                child: InkWell(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => const FarmList()));
                    },
                    child: Column(
                      children: const [
                        Image(
                          height: 45,
                          image: AssetImage('assets/icons/icons8-land-64.png'),
                        ),
                        Text('Farm')
                      ],
                    )),
              ),
              label: '')
        ],
      ),
    );
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getCurrentActivities() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    Query<Map<String, dynamic>> query =
    subcollectionRef.where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);


    // Return the stream of documents in the subcollection
    return query.orderBy('date', descending: true).limit(5).snapshots();
  }
  Image? _getActivityIcon(String activity) {
    final Map<String, Image> activityIcons = {
      'Harvest': const Image(height: 30,  image: AssetImage('assets/icons/harvest.png')),
      'Planting': const Image(height:30, image: AssetImage('assets/icons/planting.png')),
      'Weeding': const Image(height:30, image: AssetImage('assets/icons/weeding.png')),
      'Plough': const Image(height:30, image: AssetImage('assets/icons/plough.png')),
      'Till': const Image(height:30, image: AssetImage('assets/icons/tilling.png')),
      'Top-Dress': const Image(height:30, image: AssetImage('assets/icons/top-dress.png')),
      // Add more activities and their corresponding images here
    };

    return activityIcons.containsKey(activity)
        ? activityIcons[activity]
        : const Image(image: AssetImage('assets/icons/default.png')); // Default image if activity is not found
  }
}
// Check if user is logged in based on Shared Preferences data
void checkLoginStatus() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String? userId = prefs.getString('userId');
  String? email = prefs.getString('email');

  if (userId != null && email != null) {
    // User is logged in, navigate to home screen
    // ...
  } else {
    // User is not logged in, navigate to login screen
    // ...
  }
}

// Perform logout and clear Shared Preferences data
void logout() async {
  await FirebaseAuth.instance.signOut();

  SharedPreferences prefs = await SharedPreferences.getInstance();
  await prefs.clear();

  // Notify listeners or update any necessary UI
  // ...
}
