import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;
import 'package:printing/printing.dart';

// ignore: must_be_immutable
class GetData extends StatefulWidget {
  DocumentSnapshot docid;
  GetData({super.key, required this.docid});
  @override
  // ignore: no_logic_in_create_state
  State<GetData> createState() => _GetDataState(docid: docid);
}

class _GetDataState extends State<GetData> {
  DocumentSnapshot docid;
  _GetDataState({required this.docid});
  final pdf = pw.Document();
  // ignore: prefer_typing_uninitialized_variables
  var type;
  // ignore: prefer_typing_uninitialized_variables
  var date;

  @override
  void initState() {
    setState(() {
      type = widget.docid.get('type');
      date = widget.docid.get('date');

    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return PdfPreview(
      // maxPageWidth: 1000,
      // useActions: false,
      // canChangePageFormat: true,
      canChangeOrientation: false,
      // pageFormats:pageformat,
      canDebug: false,

      build: (format) => generateDocument(
        format,
      ),
    );
  }

  Future<Uint8List> generateDocument(PdfPageFormat format) async {
    final doc = pw.Document(pageMode: PdfPageMode.outlines);

    final font1 = await PdfGoogleFonts.openSansRegular();
    final font2 = await PdfGoogleFonts.openSansBold();
    // final image = await imageFromAssetBundle('assets/r2.svg');

    String? logo = await rootBundle.loadString('assets/r2.svg');

    doc.addPage(
      pw.Page(
        pageTheme: pw.PageTheme(
          pageFormat: format.copyWith(
            marginBottom: 0,
            marginLeft: 0,
            marginRight: 0,
            marginTop: 0,
          ),
          orientation: pw.PageOrientation.portrait,
          theme: pw.ThemeData.withFont(
            base: font1,
            bold: font2,
          ),
        ),
        build: (context) {
          return pw.Center(
              child: pw.Column(
                mainAxisAlignment: pw.MainAxisAlignment.center,
                children: [
                  pw.Flexible(
                    child: pw.SvgImage(
                      svg: logo,
                      height: 100,
                    ),
                  ),
                  pw.SizedBox(
                    height: 20,
                  ),
                  pw.Center(
                    child: pw.Text(
                      'Final Report card',
                      style: const pw.TextStyle(
                        fontSize: 50,
                      ),
                    ),
                  ),
                  pw.SizedBox(
                    height: 20,
                  ),
                  pw.Divider(),
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.center,
                    children: [
                      pw.Text(
                        'name : ',
                        style: const pw.TextStyle(
                          fontSize: 50,
                        ),
                      ),
                      pw.Text(
                        type,
                        style: const pw.TextStyle(
                          fontSize: 50,
                        ),
                      ),
                    ],
                  ),
                  pw.Row(
                    mainAxisAlignment: pw.MainAxisAlignment.center,
                    children: [
                      pw.Text(
                        'Maths : ',
                        style: const pw.TextStyle(
                          fontSize: 50,
                        ),
                      ),
                      pw.Text(
                        date,
                        style: const pw.TextStyle(
                          fontSize: 50,
                        ),
                      ),
                    ],
                  ),
                ],
              ));
        },
      ),
    );

    return doc.save();
  }
}