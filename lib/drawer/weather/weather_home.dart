
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:weather_icons/weather_icons.dart';

import 'package:meek/farm_dash/home.dart';

class WeatherReport extends StatefulWidget {
  const WeatherReport({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _WeatherReportState createState() => _WeatherReportState();
}

class _WeatherReportState extends State<WeatherReport> {
  static const String apiKey = '7ee04220c5e4fec63c17dee1c6dc1d15'; // Replace with your actual API key

  late String weatherReport = '';
  late Future<void> weatherDataFuture;

  @override
  void initState() {
    super.initState();
    weatherDataFuture = getCurrentLocationWeather();
  }

  Future<void> getCurrentLocationWeather() async {
    LocationPermission permission;
    bool serviceEnabled;

    // Check if location services are enabled
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are disabled, handle it accordingly
      setState(() {
        weatherReport = 'Location services are disabled.';
      });
      return;
    }

    // Request location permission
    permission = await Geolocator.requestPermission();
    if (permission == LocationPermission.denied) {
      // Permission denied, handle it accordingly
      setState(() {
        weatherReport = 'Location permission denied.';
      });
      return;
    }

    // Check the permission status again (to handle scenarios where the user granted permission after denying it)
    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      // Permission permanently denied, handle it accordingly
      setState(() {
        weatherReport = 'Location permission permanently denied.';
      });
      return;
    }

    final position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high,
    );

    final latitude = position.latitude;
    final longitude = position.longitude;

    final weatherData = await fetchWeatherData(latitude, longitude);
    final weeklyForecast = parseWeeklyForecast(weatherData);

    setState(() {
      for (final dayForecast in weeklyForecast) {
        final date = dayForecast['date'];
        final temperature = dayForecast['temperature'];
        final description = dayForecast['description'];

        weatherReport += '$date\n';
        weatherReport += '$temperature°C\n';
        weatherReport += '$description\n';
        weatherReport += '---\n';
      }
    });
  }

  IconData getWeatherIcon(String description) {
    if (description.contains('clear sky')) {
      return WeatherIcons.day_sunny;
    } else if (description.contains('clouds')) {
      return WeatherIcons.cloudy;
    } else if (description.contains('rain')) {
      return WeatherIcons.rain;
    } else if (description.contains('snow')) {
      return WeatherIcons.snow;
    } else if (description.contains('thunderstorm')) {
      return WeatherIcons.thunderstorm;
    } else {
      return WeatherIcons.na;
    }
  }

  Future<Map<String, dynamic>> fetchWeatherData(double latitude, double longitude) async {
    final url = 'https://api.openweathermap.org/data/2.5/onecall?lat=$latitude&lon=$longitude&exclude=current,minutely,hourly,alerts&appid=$apiKey&units=metric';

    final response = await http.get(Uri.parse(url));
    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to fetch weather data');
    }
  }

  List<Map<String, dynamic>> parseWeeklyForecast(Map<String, dynamic> weatherData) {
    final dailyForecasts = weatherData['daily'] as List<dynamic>;

    return dailyForecasts.map((dailyForecast) {
      final date = DateTime.fromMillisecondsSinceEpoch(
        dailyForecast['dt'] * 1000,
        isUtc: true,
      ).toLocal();

      final temperature = dailyForecast['temp']['day'];
      final description = dailyForecast['weather'][0]['description'];

      return {
        'date':        date,
        'temperature': temperature,
        'description': description,
      };
    }).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(context, MaterialPageRoute(builder: (context) => const HomePage()));
          },
        ),
        title: const Text('Weekly Forecast'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: FutureBuilder<void>(
          future: weatherDataFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            } else if (snapshot.hasError) {
              return Center(
                child: Text(
                  'Error: ${snapshot.error}',
                  style: const TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w400,
                    color: Colors.red,
                  ),
                ),
              );
            } else {
              return SingleChildScrollView(
                child: Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text(
                        weatherReport,
                        style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          color: Colors.lightGreen[800],
                        ),
                      )
                    ],
                  ),
                )
              );
            }
          },
        ),
      ),
    );
  }
}

