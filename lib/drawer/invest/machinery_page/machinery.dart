import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/machinery_page/machine_income.dart';
//import 'package:pdf/widgets.dart' as pw;
// import 'package:pdf/pdf.dart';
// import 'package:printing/printing.dart';
import 'package:meek/drawer/invest/machinery_page/edit_machine.dart';
import 'package:meek/drawer/invest/machinery_page/service_machine.dart';

class MachineryApp extends StatefulWidget {
  const MachineryApp({Key? key}) : super(key: key);

  @override
  State<MachineryApp> createState() => _MachineryAppState();
}

class _MachineryAppState extends State<MachineryApp> {
  /*
  * Controllers to pick data from the form
  * */
  final _formKey = GlobalKey<FormState>();
  final _dateController = TextEditingController();
  final _typeController = TextEditingController();

  /* ======================================================================================================
                             Checking the current user in the app
     ======================================================================================================*/
  var firebaseUser = FirebaseAuth.instance.currentUser;

/* =========================================================================================================
                                 Adding date picker
   =========================================================================================================*/
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    } else {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('No Date Picked')));
    }
    _dateController.text = _dateFormatter.format(date!);
  }

  /*========================================================================================================
                                       Clearing a form
  =========================================================================================================*/
  clearText() {
    _dateController.clear();
    _typeController.clear();
  }

  /*=======================================================================================================
                                      Deleting machinery data
    =======================================================================================================*/
  CollectionReference collectionReference =
      FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data');

  Future<void> deleteMachine(id) async {
    return await collectionReference.doc(id).delete().then((value) {
      // ignore: avoid_print
      print('Machine Deleted Successfully');
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Machinery Deleted')));
    }).catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Something went wrong: $error')));
    });
  }

  /*===========================================================================================================
                                      Print machinery data
  =============================================================================================================*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.lightGreen[900],
          title: const Text(
            'MACHINERY',
            style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24),
          ),
          centerTitle: true,
        ),
        body: StreamBuilder<QuerySnapshot>(
          stream:
              FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.connectionState == ConnectionState.waiting) {
              const Center(
                child: Icon(
                  Icons.warning_amber_rounded,
                  size: 70,
                ),
              );
            }
            final List machineryData = [];
            snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
              Map a = documentSnapshot.data() as Map<String, dynamic>;
              machineryData.add(a);
              a['id'] = documentSnapshot.id;
            }).toList();
            return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                padding: const EdgeInsets.all(10),
                child: Column(
                  children: [
                    for (var i = 0; i < machineryData.length; i++) ...[
                      SizedBox(
                        height: 65,
                        child: Material(
                          child: InkWell(
                            onTap: () {},
                            child: Card(
                                elevation: 2,
                                child: SizedBox(
                                  width: double.infinity,
                                  child: SingleChildScrollView(
                                    scrollDirection: Axis.horizontal,
                                    child: Row(
                                      children: [
                                        const SizedBox(
                                          width: 18,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text('Type  :',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey[700],
                                                  fontSize: 20,
                                                )),
                                            Text('Date  :',
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.grey[800],
                                                  fontSize: 20,
                                                )),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 50,
                                        ),
                                        Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceEvenly,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text(machineryData[i]['type'],
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.green[700],
                                                  fontSize: 18,
                                                )),
                                            Text(machineryData[i]['date'],
                                                textAlign: TextAlign.start,
                                                style: TextStyle(
                                                  color: Colors.green[700],
                                                  fontSize: 18,
                                                )),
                                          ],
                                        ),
                                        const SizedBox(
                                          width: 70,
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.end,
                                          children: [
                                            IconButton(
                                                onPressed: () {
                                                  showDialog(
                                                      context: context,
                                                      builder: (context) =>
                                                          AlertDialog(
                                                              title: Text(
                                                                'More Options',
                                                                textAlign:
                                                                    TextAlign
                                                                        .center,
                                                                style: TextStyle(
                                                                    fontStyle:
                                                                        FontStyle
                                                                            .normal,
                                                                    fontSize:
                                                                        18,
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                              content: SizedBox(
                                                                height: 200,
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceBetween,
                                                                  children: [
                                                                    Material(
                                                                      child:
                                                                          InkWell(
                                                                        onTap:
                                                                            () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>MachineIncome(id: machineryData[i]['id'])));
                                                                            },
                                                                        child:
                                                                            Text(
                                                                          'Add Income',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                17,
                                                                            color:
                                                                                Colors.grey[600],
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const Divider(),
                                                                    Material(
                                                                      child:
                                                                          InkWell(
                                                                        onTap:
                                                                            () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=> ServiceMachine(id: machineryData[i]['id'])));
                                                                            },
                                                                        child:
                                                                            Text(
                                                                          'Add Service',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                17,
                                                                            color:
                                                                                Colors.grey[600],
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const Divider(),
                                                                    Material(
                                                                      child:
                                                                          InkWell(
                                                                        onTap:
                                                                            () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>UpdateMachine(id: machineryData[i]['id'],)));
                                                                            },
                                                                        child:
                                                                            Text(
                                                                          'Edit Record',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                17,
                                                                            color:
                                                                                Colors.grey[600],
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),

                                                                    const Divider(),
                                                                    Material(
                                                                      child:
                                                                          InkWell(
                                                                        onTap:
                                                                            () {
                                                                          showDialog(
                                                                              context: context,
                                                                              builder: (context) {
                                                                                return AlertDialog(
                                                                                    title: const Text('DELETE MACHINE'),
                                                                                    content: SizedBox(
                                                                                      height: 135,
                                                                                      child: Column(
                                                                                        children: [
                                                                                          Text(
                                                                                            'All records associated with this Machinery will be deleted such as Service, miscellaneous and updates. Are you sure?',
                                                                                            style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500, color: Colors.grey[600]),
                                                                                          ),
                                                                                          Row(
                                                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                                                            children: [
                                                                                              ElevatedButton(
                                                                                                  style: ElevatedButton.styleFrom(
                                                                                                    backgroundColor: Colors.orange[800],
                                                                                                    shape: RoundedRectangleBorder(
                                                                                                      borderRadius: BorderRadius.circular(5),
                                                                                                    ),
                                                                                                  ),
                                                                                                  onPressed: () {
                                                                                                    Navigator.of(context).pop();
                                                                                                  },
                                                                                                  child: const Text("CANCEL")),
                                                                                              const SizedBox(
                                                                                                width: 20,
                                                                                              ),
                                                                                              ElevatedButton(
                                                                                                  style: ElevatedButton.styleFrom(
                                                                                                    shape: RoundedRectangleBorder(
                                                                                                      borderRadius: BorderRadius.circular(5),
                                                                                                    ),
                                                                                                    backgroundColor: Colors.red[800],
                                                                                                  ),
                                                                                                  onPressed: () {
                                                                                                    deleteMachine(machineryData[i]['id']).then((value) {
                                                                                                      Navigator.of(context).pop();
                                                                                                    });
                                                                                                  },
                                                                                                  child: Text(
                                                                                                    'DELETE',
                                                                                                    style: TextStyle(
                                                                                                      fontSize: 17,
                                                                                                      color: Colors.grey[600],
                                                                                                      fontWeight: FontWeight.w400,
                                                                                                    ),
                                                                                                  ))
                                                                                            ],
                                                                                          )
                                                                                        ],
                                                                                      ),
                                                                                    ));
                                                                              }).then((value) {
                                                                            Navigator.pop(context);
                                                                          });
                                                                        },
                                                                        child:
                                                                            Text(
                                                                          'Delete',
                                                                          style:
                                                                              TextStyle(
                                                                            fontSize:
                                                                                17,
                                                                            color:
                                                                                Colors.grey[600],
                                                                            fontWeight:
                                                                                FontWeight.w500,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                              )));
                                                },
                                                icon: const Icon(
                                                  Icons.more_vert,
                                                  size: 40,
                                                ))
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                )),
                          ),
                        ),
                      )
                    ]
                  ],
                ),
              ),
            );
          },
        ),
        floatingActionButton: SizedBox(
          height: 40,
          width: 101,
          child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.lightGreen[800],
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              onPressed: () {
                showDialog(
                    context: context,
                    builder: (context) {
                      return AlertDialog(
                          title: Text(
                            'Add Machinery',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              color: Colors.grey[600],
                            ),
                          ),
                          content: SizedBox(
                            height: 210,
                            child: Form(
                                key: _formKey,
                                child: Column(
                                  children: [
                                    TextFormField(
                                      controller: _typeController,
                                      validator: (value) {
                                        if (value!.isEmpty &&
                                            !RegExp(r'^[A-Z a-z]+$')
                                                .hasMatch(value)) {
                                          return 'Input valid input type';
                                        }
                                        return null;
                                      },
                                      decoration: const InputDecoration(
                                        hintText: 'Type',
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    TextFormField(
                                      controller: _dateController,
                                      validator: (value) {
                                        if (value == null || value.isEmpty) {
                                          return 'Input a valid date';
                                        }
                                        return null;
                                      },
                                      readOnly: true,
                                      onTap: _handleDatePicker,
                                      decoration: const InputDecoration(
                                        hintText: 'Date',
                                      ),
                                    ),
                                    const SizedBox(
                                      height: 20,
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor:
                                                    Colors.lightGreen[800]),
                                            onPressed: () {
                                              Navigator.of(context).pop();
                                            },
                                            child: const Text(
                                              'Cancel',
                                              style: TextStyle(
                                                  fontSize: 20,
                                                  fontWeight: FontWeight.bold),
                                            )),
                                        const SizedBox(
                                          width: 15,
                                        ),
                                        ElevatedButton(
                                            style: ElevatedButton.styleFrom(
                                                backgroundColor:
                                                    Colors.lightGreen[800]),
                                            onPressed: () {
                                              if (_formKey.currentState!
                                                  .validate()) {
                                                FirebaseFirestore.instance
                                                    .collection('machine')
                                                    .doc('data').collection('machine-data').doc()
                                                    .set({
                                                  'rid': firebaseUser?.uid,
                                                  'type': _typeController.text,
                                                  'date': _dateController.text
                                                }).then((value) {
                                                  clearText();
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(const SnackBar(
                                                          content: Text(
                                                              'Message added successfully')));
                                                }).onError(
                                                        (error, stackTrace) =>
                                                            null);
                                              }
                                            },
                                            child: const Text('Add',
                                                style: TextStyle(
                                                    fontSize: 20,
                                                    fontWeight:
                                                        FontWeight.bold))),
                                      ],
                                    )
                                  ],
                                )),
                          ));
                    });
              },
              child: Row(
                children: const [
                  Text(
                    'Add',
                    style: TextStyle(fontSize: 20),
                  ),
                  Icon(
                    Icons.add,
                    size: 30,
                    color: Colors.yellow,
                  ),
                ],
              )),
        ));
  }

  /// create PDF & print it
  // void _createPdf() async {
  //   final doc = pw.Document();

    /// for using an image from assets
    // final image = await imageFromAssetBundle('assets/image.png');

    // doc.addPage(
    //   pw.Page(
    //     pageFormat: PdfPageFormat.a4,
    //     build: (pw.Context context) {
    //       return pw.Center(
    //         child: pw.Text('Print pdf'),
    //       ); // Center
    //     },
    //   ),
    // ); // Page

    /// print the document using the iOS or Android print service:
    // await Printing.layoutPdf(
    //     onLayout: (PdfPageFormat format) async => doc.save());

    /// share the document to other applications:
    // await Printing.sharePdf(bytes: await doc.save(), filename: 'my-document.pdf');

    /// tutorial for using path_provider: https://www.youtube.com/watch?v=fJtFDrjEvE8
    /// save PDF with Flutter library "path_provider":
    // final output = await getTemporaryDirectory();
    // final file = File('${output.path}/example.pdf');
    // await file.writeAsBytes(await doc.save());
 // }
}
