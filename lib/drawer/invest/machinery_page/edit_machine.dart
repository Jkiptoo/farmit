import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/machinery_page/machinery.dart';

class UpdateMachine extends StatefulWidget {
  final String id;
  const UpdateMachine({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateMachine> createState() => _UpdateMachineState();
}

class _UpdateMachineState extends State<UpdateMachine> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference update = FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data');
  Future<void> machineUpdate(id, type) async {
    update.doc(id).update({
      'type': type,

    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Machine Successfully Updated'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('UPDATE Machine'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('machine')
                      .doc('data')
                  .collection('machine-data')
                  .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    var type = data!['type'];

                    return Column(
                      children: [
                        TextFormField(
                          onChanged: (val) {
                            type = val;
                          },
                          initialValue: type,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Type';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Type', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.teal[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(10))),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    machineUpdate(widget.id, type,
                                        )
                                        .then((value) => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const MachineryApp())));
                                  }
                                },
                                child: const Text('Update'))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}
