import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class ServiceMachine extends StatefulWidget {
  final String id;
  const ServiceMachine({Key? key, required this.id}) : super(key: key);

  @override
  State<ServiceMachine> createState() => _ServiceMachineState();
}

class _ServiceMachineState extends State<ServiceMachine> {
  final _formKey = GlobalKey<FormState>();
  final _dateController = TextEditingController();
  final _costController = TextEditingController();
  final _typeController = TextEditingController();



  CollectionReference update =
      FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data');
  Future<void> machineUpdate(id, type) async {
    update.doc(id).update({
      'type': type,
    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Machine Successfully Updated'))));
  }

  /* ======================================================================================================
                             Checking the current user in the app
     ======================================================================================================*/
  var firebaseUser = FirebaseAuth.instance.currentUser;

/* =========================================================================================================
                                 Adding date picker
   =========================================================================================================*/
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    } else {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('No Date Picked')));
    }
    _dateController.text = _dateFormatter.format(date!);
  }
  clearText(){
    _dateController.clear();
    _costController.clear();
    _typeController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Service Machine'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('machine')
                      .doc('data').collection('machine-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    var type = data!['type'];

                    return Column(
                      children: [
                        TextFormField(
                          // controller: _typeController,
                          onChanged: (val) {
                            type = val;
                          },
                          initialValue: type,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Type';
                            }
                            return null;
                          },
                          readOnly: true,
                          decoration: const InputDecoration(
                              labelText: 'Type', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                        TextFormField(
                          controller: _dateController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Input a valid date';
                            }
                            return null;
                          },
                          readOnly: true,
                          onTap: _handleDatePicker,
                          decoration: const InputDecoration(
                            hintText: 'Date',
                            border: OutlineInputBorder()
                          ),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          keyboardType: TextInputType.number,
                          controller: _costController,
                          validator: (value) {
                            if (value == null || value.isEmpty) {
                              return 'Input valid cost';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Cost', border: OutlineInputBorder()),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.teal[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10))),
                                onPressed: () {
                                  if (_formKey.currentState!
                                      .validate()) {
                                    FirebaseFirestore.instance
                                        .collection('machine')
                                        .doc('data').collection('service').doc()
                                        .set({
                                      'id': firebaseUser?.uid,
                                      'type': type,
                                      'date': _dateController.text,
                                      'cost': _costController.text
                                    }).then((value) {
                                      //Navigator.push(context, MaterialPageRoute(builder: (context)=>));
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(const SnackBar(
                                          content: Text(
                                              'Message added successfully')));
                                      clearText();
                                    }).onError(
                                            (error, stackTrace) =>
                                        null);
                                  }
                                },
                                child: const Text('Service'))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
