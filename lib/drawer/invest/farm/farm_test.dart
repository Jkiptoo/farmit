import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class FarmTest extends StatefulWidget {
  const FarmTest({Key? key}) : super(key: key);

  @override
  State<FarmTest> createState() => _FarmTestState();
}

class _FarmTestState extends State<FarmTest> {
  var cost = '';
  var quantity = '';
  var date = '';

  /*
  * Controllers to pick data from the form
  * */

  final _cropIncomeFormKey = GlobalKey<FormState>();
  final _machineryIncomeFormKey = GlobalKey<FormState>();
  final _animalIncomeFormKey = GlobalKey<FormState>();
  final _animalFormKey = GlobalKey<FormState>();


  final _dateController = TextEditingController();
  final _costController = TextEditingController();
  final _quantityController = TextEditingController();
  final _descController = TextEditingController();

  /* ======================================================================================================
                             Checking the current user in the app
     ======================================================================================================*/
  var firebaseUser = FirebaseAuth.instance.currentUser;

/* =========================================================================================================
                                 Adding date picker
   =========================================================================================================*/
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    } else {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('No Date Picked')));
    }
    _dateController.text = _dateFormatter.format(date!);
  }

  /*========================================================================================================
                                       Clearing a form
  =========================================================================================================*/
  clearText() {
    _dateController.clear();
    _quantityController.clear();
    _costController.clear();
  }

  /*=======================================================================================================
                                      Deleting machinery data
    =======================================================================================================*/
  CollectionReference collectionReference =
  FirebaseFirestore.instance.collection('expense');

  Future<void> deleteMachine(id) async {
    return await collectionReference.doc(id).delete().then((value) {
      // ignore: avoid_print
      print('Expense Deleted Successfully');
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Expense Deleted')));
    }).catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Something went wrong: $error')));
    });
  }

  /*===========================================================================================================
                                      Print Expense data
  =============================================================================================================*/

  List<String> cropData = []; // List to store data from crop
  List<String> farmData = []; // List to store data from farm
  List<String> machineData = []; // List to store data from Machinery
  List<String> animalData = []; // List to store data from Animal


  String? cropItem; // Selected item from crop dropdown
  String? farmItem; // Selected item from farm dropdown
  String? machineItem; // Selected item from Machinery dropdown
  String? animalItem; // Selected item from animal dropdown

  @override
  void initState() {
    super.initState();
    initializeData();
  }

  Future<void> initializeData() async {
    await getDataFromCollections();
    setState(() {});
  }


  Future<void> getDataFromCollections() async {
    CollectionReference crop = FirebaseFirestore.instance.collection('crop').doc('crops').collection('crop-data');
    CollectionReference machine = FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data');
    CollectionReference animal = FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');

    QuerySnapshot cropSnapshot = await crop.get();
    QuerySnapshot machinerySnapshot = await machine.get();
    QuerySnapshot animalSnapshot = await animal.get();

    // Clear the lists before populating with new data
    cropData.clear();
    farmData.clear();
    machineData.clear();
    animalData.clear();

    // Populate the lists with specific fields from document data
    for (var document in cropSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data[
      'crop']; // field name in the collection
      cropData.add(fieldName);
    }

    for (var document in machinerySnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data['type']; // field name in the collection
      machineData.add(fieldName);
    }

    for (var document in animalSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data['type']; // field name in the collection
      animalData.add(fieldName);
    }
  }

  Future<void> saveDataToNewCollection() async {
    // Get the Firestore instance
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    // Create a new collection reference for the new collection
    CollectionReference newCollection = firestore
        .collection('expenses'); // Replace with your desired collection name

    // Create a new document in the new collection with the selected values
    await newCollection
        .add({'crop': cropItem, 'farm': farmItem, 'machine': machineItem, 'animal': animalItem});

    // Reset the dropdown values after saving
    setState(() {
      cropItem = null;
      farmItem = null;
      machineItem = null;
      animalItem = null;
    });

    // Show a success message or perform any other necessary actions
    // ignore: use_build_context_synchronously
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Data saved to new collection'),
      ),
    );
  }



  /*===================================================================================================================================================
                                         Adding Income to collection
  ====================================================================================================================================================*/
  CollectionReference reference =
  FirebaseFirestore.instance.collection('income');
  // ignore: non_constant_identifier_names
  Future<void> AddIncome() async {
    return await reference
        .add({
      'crop': cropItem,
      'quantity': quantity,
      'cost': cost,
      'date': date,
    })
    // ignore: avoid_print
        .then((value) => print('Planting added Successfully'))
        .catchError((error) {
      // ignore: avoid_print
      print('Something went wrong: $error');
    });
  }

  /*==================================================================================================================================================*/
  clearIncome() {
    _quantityController.clear();
    _costController.clear();
    _dateController.clear();
  }
  /*===================================================================================================================================================
  *                                              List of Quantity units
  * ===================================================================================================================================================*/
  // ignore: prefer_typing_uninitialized_variables
  var selectedType;
  final List<String> _categoryList = <String>[
    'Bales',
    'Dozen',
    'Kilograms',
    'Tonnes',
    'Litres',
    'Sacks',
    'heads',
  ];

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.lightGreen[900],
            title: const Text(
              'Transactions',
              style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24),
            ),
            centerTitle: true,
          ),
          body: TabBarView(children: [ DefaultTabController(
            length: 3,
            child: SizedBox(
              height: 210,
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  TabBar(tabs: const [
                    Tab(
                      text: 'Crop',
                      icon: Icon(Icons.grass),
                    ),
                    Tab(
                      text: 'Machinery',
                      icon: Icon(Icons.build),
                    ),
                    Tab(
                      text: 'Animal',
                      icon: Icon(Icons.pets),
                    ),
                  ],
                    labelColor:
                    Colors.white, // Color of the selected tab's label
                    unselectedLabelColor:
                    Colors.grey, // Color of unselected tab's label
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          8), // Rounded corner indicator
                      color:
                      Colors.lightGreen[900], // Color of the indicator
                    ),
                    indicatorSize:
                    TabBarIndicatorSize.tab, // Match the tab's size
                    indicatorPadding:
                    const EdgeInsets.symmetric(horizontal: 8),
                  ),
                  Expanded(child: TabBarView(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(20),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Form(
                                  key: _cropIncomeFormKey,
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 20),
                                      DropdownButtonFormField<String>(
                                          validator: (value) {
                                            if (value!.isEmpty &&
                                                !RegExp(r"^[a-z A-Z]+$")
                                                    .hasMatch(value)) {
                                              return 'Enter valid crop name';
                                            }
                                            return null;
                                          },
                                          value: cropItem,
                                          items: cropData.map((String item) {
                                            return DropdownMenuItem(
                                                value: item,
                                                child: Text(item));
                                          }).toList(),
                                          decoration: const InputDecoration(
                                              labelText: 'Crop Name',
                                              border: OutlineInputBorder()),
                                          onChanged: (value) {
                                            setState(() {
                                              cropItem = value;
                                            });
                                          }),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        controller: _costController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r'^[0-9]+$')
                                                  .hasMatch(value)) {
                                            return 'Enter a valid ammount';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Cost',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        controller: _quantityController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r'^[0-9]+$')
                                                  .hasMatch(value)) {
                                            return 'Enter a valid quantity';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Quantity',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      Container(
                                        padding: const EdgeInsets.all(0),
                                        child: Row(
                                          children: [
                                            const Text('Quantity Unit'),
                                            const SizedBox(
                                              width: 30,
                                            ),
                                            Flexible(
                                              child: DropdownButtonFormField(
                                                items: _categoryList
                                                    .map((value) => DropdownMenuItem(
                                                  value: value,
                                                  child: Text(value),
                                                ))
                                                    .toList(),
                                                onChanged: (selectedCategoryType) {
                                                  setState(() {
                                                    selectedType = selectedCategoryType;
                                                  });
                                                },
                                                value: selectedType,
                                                isExpanded: true,
                                                hint: const Text('Select quantity unit'),
                                                validator: (value) {
                                                  if (value == null) {
                                                    return 'Please select a quantity unit';
                                                  }
                                                  return null;
                                                },
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        validator: (value) {
                                          if (value != null &&
                                              value.isEmpty) {
                                            return "Date can't be null";
                                          }
                                          return null;
                                        },
                                        readOnly: true,
                                        controller: _dateController,
                                        onTap: _handleDatePicker,
                                        decoration: const InputDecoration(
                                          labelText: 'Date Added',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        maxLines: 5,
                                        controller: _descController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r"^[A-Z a-z]+$")
                                                  .hasMatch(value)) {
                                            return 'Enter a valid description';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Description',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                              Colors.lightGreen[900],
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      7))),
                                          onPressed: () {

                                            if(_cropIncomeFormKey.currentState!.validate()){
                                              FirebaseFirestore.instance.collection('crop').doc('data').collection('income').doc().set({
                                                'crop': cropItem.toString(),
                                                'ammount': _costController.text,
                                                'quantity': _quantityController.text,
                                                'unit': selectedType.toString(),
                                                'date': _dateController.text,
                                                'desc': _descController.text
                                              });
                                            }
                                          },
                                          child: const Text('Save'))
                                    ],
                                  )),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(20),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Form(
                                  key: _machineryIncomeFormKey,
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 20),
                                      DropdownButtonFormField<String>(
                                          validator: (value) {
                                            if (value!.isEmpty &&
                                                !RegExp(r"^[A-Z a-z]+$")
                                                    .hasMatch(value)) {
                                              return 'Enter valid Machinery name';
                                            }
                                            return null;
                                          },
                                          value: machineItem,
                                          items: machineData.map((String item) {
                                            return DropdownMenuItem(
                                                value: item,
                                                child: Text(item));
                                          }).toList(),
                                          decoration: const InputDecoration(
                                              labelText: 'Machine Type',
                                              border: OutlineInputBorder()),
                                          onChanged: (value) {
                                            setState(() {
                                              machineItem = value;
                                            });
                                          }),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        controller: _costController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r'^[0-9]+$')
                                                  .hasMatch(value)) {
                                            return 'Enter a valid ammount';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Cost',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        validator: (value) {
                                          if (value != null &&
                                              value.isEmpty) {
                                            return "Date can't be null";
                                          }
                                          return null;
                                        },
                                        readOnly: true,
                                        controller: _dateController,
                                        onTap: _handleDatePicker,
                                        decoration: const InputDecoration(
                                          labelText: 'Date',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        maxLines: 5,
                                        controller: _descController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r"^[A-Z a-z]+$")
                                                  .hasMatch(value)) {
                                            return 'Enter a valid description';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Description',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                              Colors.lightGreen[900],
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      7))),
                                          onPressed: () {},
                                          child: const Text('Save'))
                                    ],
                                  )),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(20),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: Form(
                                  key: _animalIncomeFormKey,
                                  child: Column(
                                    children: [
                                      const SizedBox(height: 20),
                                      DropdownButtonFormField<String>(
                                          validator: (value) {
                                            if (value!.isEmpty &&
                                                !RegExp(r"^[A-Z a-z]+$")
                                                    .hasMatch(value)) {
                                              return 'Enter valid Animal name';
                                            }
                                            return null;
                                          },
                                          value: animalItem,
                                          items: animalData.map((String item) {
                                            return DropdownMenuItem(
                                                value: item,
                                                child: Text(item));
                                          }).toList(),
                                          decoration: const InputDecoration(
                                              labelText: 'Animal Name',
                                              border: OutlineInputBorder()),
                                          onChanged: (value) {
                                            setState(() {
                                              animalItem = value;
                                            });
                                          }),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        controller: _costController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r'^[0-9]+$')
                                                  .hasMatch(value)) {
                                            return 'Enter a valid ammount';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Cost',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      Container(
                                        padding: const EdgeInsets.all(0),
                                        child: Row(
                                          children: [
                                            const Text('Quantity Unit'),
                                            const SizedBox(
                                              width: 30,
                                            ),
                                            Flexible(
                                              child: DropdownButton(
                                                items: _categoryList
                                                    .map((value) => DropdownMenuItem(
                                                    value: value, child: Text(value)))
                                                    .toList(),
                                                onChanged: (selectedCategoryType) {
                                                  setState(() {
                                                    selectedType = selectedCategoryType;
                                                  });
                                                },
                                                value: selectedType,
                                                isExpanded: true,
                                                hint: const Text('Select quantity unit'),
                                              ),
                                            )
                                          ],
                                        ),
                                      ),
                                      const SizedBox(height: 20),

                                      TextFormField(
                                        validator: (value) {
                                          if (value != null &&
                                              value.isEmpty) {
                                            return "Date can't be null";
                                          }
                                          return null;
                                        },
                                        readOnly: true,
                                        controller: _dateController,
                                        onTap: _handleDatePicker,
                                        decoration: const InputDecoration(
                                          labelText: 'Date Added',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      TextFormField(
                                        maxLines: 5,
                                        controller: _descController,
                                        validator: (value) {
                                          if (value!.isEmpty &&
                                              !RegExp(r"^[A-Z a-z]+$")
                                                  .hasMatch(value)) {
                                            return 'Enter a valid description';
                                          }
                                          return null;
                                        },
                                        keyboardType: TextInputType.number,
                                        decoration: const InputDecoration(
                                          hintText: 'Description',
                                          border: OutlineInputBorder(),
                                        ),
                                      ),
                                      const SizedBox(height: 20),
                                      ElevatedButton(
                                          style: ElevatedButton.styleFrom(
                                              backgroundColor:
                                              Colors.lightGreen[900],
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      7))),
                                          onPressed: () {
                                            if(_animalFormKey.currentState!.validate()){
                                              FirebaseFirestore.instance.collection('income').doc().collection('').doc().set({

                                              });
                                            }
                                          },
                                          child: const Text('Save'))
                                    ],
                                  )),
                            ),
                          )),
                    ],
                  ))
                ],
              ),
            ),
          ),
          ]),
        ));
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getCollections(){
    DocumentReference<Map<String, dynamic>> parentDoc = FirebaseFirestore.instance.collection('farm').doc('farm_ownership');

    CollectionReference<Map<String, dynamic>> parentCol = parentDoc.collection('owned');

    return parentCol.snapshots();
  }
}
