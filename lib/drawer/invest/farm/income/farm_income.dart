import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/farm/income/view_income.dart';
import 'package:meek/farm_dash/home.dart';

class FarmIncome extends StatefulWidget {
  final String id;
  const FarmIncome({Key? key, required this.id}) : super(key: key);

  @override
  State<FarmIncome> createState() => _FarmIncomeState();
}

class _FarmIncomeState extends State<FarmIncome> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference<Map<String, dynamic>> update = FirebaseFirestore.instance
      .collection('farm')
      .doc('farms')
      .collection('farm-data');

  String farmName = '';
  String farmSize = '';

  final _dateController = TextEditingController();
  final _fertilizerController = TextEditingController();
  final _bagsController = TextEditingController();
  final _sizeController = TextEditingController();
  final _costController = TextEditingController();
  final _quantityController = TextEditingController();
  final _descController = TextEditingController();

  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDate() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now(),
        builder: (BuildContext context, Widget? child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: const ColorScheme.light().copyWith(
                  // Customize the background color of the date picker
                  background: Colors.lightGreen[900],
                ),
                // Customize the text style of the date picker
                textTheme: const TextTheme(
                  // Customize the text style of the header/title
                  titleLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the selected date
                  bodyMedium: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the unselected dates
                  bodyLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
              child: child!);
        });
    if (date != null) {
      _dateController.text = _dateFormatter.format(date);
    }
  }

  List<String> cropData = []; // List to store data from collection1
  String? cropItem; // Selected item from collection1 dropdown

  Future<void> initializeData() async {
    await getDataFromCollections();
    setState(() {});
  }

  Future<void> getDataFromCollections() async {
    CollectionReference crop = FirebaseFirestore.instance
        .collection('crop')
        .doc('crops')
        .collection('crop-data');

    QuerySnapshot cropSnapshot = await crop.where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).get();

    // Clear the lists before populating with new data
    cropData.clear();

    // Populate the lists with specific fields from document data
    for (var document in cropSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data[
          'crop']; // Replace 'crop' with the actual field name in the document
      cropData.add(fieldName);
    }
  }

  Future<void> saveDataToNewCollection() async {
    // Get the Firestore instance
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    // Create a new collection reference for the new collection
    CollectionReference newCollection = firestore
        .collection('harvest'); // Replace with your desired collection name

    // Create a new document in the new collection with the selected values
    await newCollection.add({
      'crop': cropItem,
    });

    // Reset the dropdown values after saving
    setState(() {
      cropItem = null;
    });

    // Show a success message or perform any other necessary actions
    // ignore: use_build_context_synchronously
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Data saved to new collection'),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initializeData();
  }

  clearForm() {
    _fertilizerController.clear();
    _bagsController.clear();
    _sizeController.clear();
    _costController.clear();
    _dateController.clear();
    _descController.clear();
    cropData.clear();
    _categoryList.clear();
    // ignore: unrelated_type_equality_checks
    cropItem == initializeData();
    // ignore: unrelated_type_equality_checks
    selectedType == '';
    selectedHarvestType == '';
    _quantityController.clear();
  }

  // ignore: prefer_typing_uninitialized_variables
  var selectedType;
  final List<String> _categoryList = <String>[
    'Harvest',
    'Planting',
    'Weeding',
    'Plough',
    'Till',
    'Top-Dress',
  ];

  // ignore: prefer_typing_uninitialized_variables
  var selectedHarvestType;
  final List<String> _categoryHarvestList = <String>[
    'Bales',
    'Dozen',
    'Kilograms',
    'Tonnes',
    'Litres',
    'Sacks',
  ];
  // ignore: prefer_typing_uninitialized_variables
  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Add Income'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('farm')
                      .doc('farms')
                      .collection('farm-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    if (!snapshot.hasData || snapshot.data == null) {
                      return const Text('No data available');
                    }
                    var data = snapshot.data!.data();
                    if (data == null) {
                      return const Text('No data available');
                    }
                    farmName = data['name'] ?? '';
                    farmSize = data['size'] ?? '';

                    return Column(
                      children: [
                        Column(
                          children: [
                            TextFormField(
                              onChanged: (val) {
                                setState(() {
                                  farmName = val;
                                });
                              },
                              readOnly: true,
                              initialValue: farmName,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[a-zA-Z ]+$').hasMatch(value)) {
                                  return 'Please enter a valid Farm name';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Name',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              onChanged: (val) {
                                setState(() {
                                  farmSize = val;
                                });
                              },
                              initialValue: farmSize,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                  return 'Please enter a valid Farm Size';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Size',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Column(
                              children: [
                                DropdownButtonFormField<String>(
                                  validator: (value) {
                                    if (value == null ||
                                        !RegExp(r"^[A-Z a-z]+$")
                                            .hasMatch(value)) {
                                      return 'Enter valid crop name';
                                    }
                                    return null;
                                  },
                                  value: cropItem,
                                  onChanged: (value) {
                                    setState(() {
                                      cropItem = value;
                                    });
                                  },
                                  decoration: const InputDecoration(
                                      labelText: 'Crop Name',
                                      border: OutlineInputBorder()),
                                  items: cropData.map((String item) {
                                    return DropdownMenuItem<String>(
                                      value: item,
                                      child: Text(item),
                                    );
                                  }).toList(),
                                ),
                                const SizedBox(height: 20),
                                TextFormField(
                                  controller: _quantityController,
                                  keyboardType: TextInputType.number,
                                  validator: (value) {
                                    if (value!.isEmpty &&
                                        !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                      return 'Enter valid Quantity';
                                    }
                                    return null;
                                  },
                                  decoration: const InputDecoration(
                                    hintText: 'Quantity',
                                    border: OutlineInputBorder(),
                                  ),
                                ),
                                const SizedBox(height: 20),
                                Container(
                                  padding: const EdgeInsets.all(0),
                                  child: Row(
                                    children: [
                                      const Text('Harvest Unit'),
                                      const SizedBox(
                                        width: 30,
                                      ),
                                      Flexible(
                                        child: DropdownButtonFormField<String>(
                                          items: _categoryHarvestList
                                              .map((value) => DropdownMenuItem(
                                                    value: value,
                                                    child: Text(value),
                                                  ))
                                              .toList(),
                                          onChanged:
                                              (selectedHarvestCategoryType) {
                                            setState(() {
                                              selectedHarvestType =
                                                  selectedHarvestCategoryType;
                                            });
                                          },
                                          value: selectedHarvestType,
                                          isExpanded: true,
                                          decoration: const InputDecoration(
                                            labelText: 'Select harvest unit',
                                            // Add any other decoration properties you need
                                          ),
                                          validator: (value) {
                                            if (value == null) {
                                              return 'Please select a harvest unit';
                                            }
                                            return null; // Return null to indicate the input is valid
                                          },
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                                const SizedBox(height: 20),
                              ],
                            ),
                            TextFormField(
                              controller: _costController,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                  return 'Enter a valid cost';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Amount',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return "Date can't be null";
                                }
                                return null;
                              },
                              readOnly: true,
                              controller: _dateController,
                              onTap: _handleDate,
                              decoration: const InputDecoration(
                                labelText: 'Date',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              maxLines: 5,
                              controller: _descController,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                  return 'Enter a valid description';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'Description',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.lightGreen[900],
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                  minimumSize: const Size(120, 40)
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  FirebaseFirestore.instance
                                      .collection('farm')
                                      .doc('activities')
                                      .collection('farm-income')
                                      .doc()
                                      .set({
                                    'rid': firebaseUser?.uid,
                                    'farm': farmName,
                                    'crop': cropItem.toString(),
                                    'size': farmSize,
                                    'quantity': _quantityController.text,
                                    'desc': _descController.text,
                                    'cost': _costController.text,
                                    'unit': selectedHarvestType.toString(),
                                    'date': _dateController.text,
                                    'activity': selectedType.toString()
                                  })
                                      // ignore: avoid_print
                                      .then((value) {
                                    clearForm();
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>const HomePage()));
                                  }).catchError((error) {
                                    // ignore: avoid_print
                                    print('Something went wrong: $error');
                                  });
                                }
                              },
                              child: const Text('Save', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.2),),
                            ),
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4),),
                                  backgroundColor: Colors.lightGreen[900],
                                  minimumSize: const Size(120, 40)
                                ),
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewIncome(id: widget.id)));

                                }, child: const Text('View', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.2),))
                          ],
                        ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
