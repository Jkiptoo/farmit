import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/farm/data/farm_own_add.dart';
import 'package:meek/drawer/invest/farm/records/view_farm.dart';
import 'package:meek/farm_dash/home.dart';

class FarmList extends StatefulWidget {
  const FarmList({Key? key}) : super(key: key);

  @override
  State<FarmList> createState() => _FarmListState();
}

class _FarmListState extends State<FarmList> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=> const HomePage()));
        },),
        backgroundColor: Colors.lightGreen[900],
        title: const Text('Farm List'),
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: getOwnedDocumentsStream(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeFarm = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map a = documentSnapshot.data() as Map<String, dynamic>;
            storeFarm.add(a);
            a['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (var i = 0; i < storeFarm.length; i++) ...[
                    SizedBox(
                      height: 70,
                      width: double.infinity,
                      child: Material(
                        child: InkWell(
                          onTap: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                          },
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8.0), // Adjust the border radius as per your requirement
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(0.0), // Adjust the spacing as per your requirement
                              child: ListTile(
                                leading:  const Image(height: 25, image: AssetImage('assets/icons/icons8-land-64.png'),),
                                title: Text(
                                  storeFarm[i]['name'],
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                subtitle: Text(
                                  storeFarm[i]['date'],
                                  style: TextStyle(
                                    color: Colors.grey[600],
                                  ),
                                ),
                                trailing: Text(
                                  storeFarm[i]['size'],
                                  style: TextStyle(
                                    color: Colors.grey[800],
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ),
                      ),
                    )
                  ]
                ],
              ),
            ),
          );
        },
      ),

      floatingActionButton: FloatingActionButton(
        onPressed: () {

          Navigator.push(context, MaterialPageRoute(builder: (context)=> const FarmOwnAdd()));
        },
        backgroundColor: Colors.lightGreen[900],
        child: const Icon(
          Icons.add,
        ),
      ),
    );
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('farms');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-data');

    Query<Map<String, dynamic>> query =
    subcollectionRef.where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the stream of documents in the subcollection
    return query.snapshots();
  }

}

