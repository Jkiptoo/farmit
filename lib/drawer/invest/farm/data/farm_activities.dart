import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/farm/records/activities_summary.dart';


class FarmActivities extends StatefulWidget {
  final String id;
  const FarmActivities({Key? key, required this.id}) : super(key: key);

  @override
  State<FarmActivities> createState() => _FarmActivitiesState();
}

class _FarmActivitiesState extends State<FarmActivities> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference<Map<String, dynamic>> update = FirebaseFirestore.instance
      .collection('farm')
      .doc('farms')
      .collection('farm-data');

  String farmName = '';
  String farmSize = '';

  final _dateController = TextEditingController();
  final _fertilizerController = TextEditingController();
  final _bagsController = TextEditingController();
  final _sizeController = TextEditingController();
  final _expenseController = TextEditingController();
  final _quantityController = TextEditingController();
  final _herbController = TextEditingController();
  final _descController = TextEditingController();

  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDate() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now(),
        builder: (BuildContext context, Widget? child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: const ColorScheme.light().copyWith(
                  // Customize the background color of the date picker
                  background: Colors.lightGreen[900],
                ),
                // Customize the text style of the date picker
                textTheme: const TextTheme(
                  // Customize the text style of the header/title
                  titleLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the selected date
                  bodyMedium: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the unselected dates
                  bodyLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
              child: child!);
        });
    if (date != null) {
      _dateController.text = _dateFormatter.format(date);
    }
  }

  List<String> cropData = []; // List to store data from collection1
  String? cropItem; // Selected item from collection1 dropdown

  Future<void> initializeData() async {
    await getDataFromCollections();
    setState(() {});
  }

  Future<void> getDataFromCollections() async {
    CollectionReference crop = FirebaseFirestore.instance
        .collection('crop')
        .doc('crops')
        .collection('crop-data');


    QuerySnapshot cropSnapshot = await crop.where('rid', isEqualTo:  FirebaseAuth.instance.currentUser?.uid).get();

    // Clear the lists before populating with new data
    cropData.clear();

    // Populate the lists with specific fields from document data
    for (var document in cropSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data[
      'crop']; // Replace 'crop' with the actual field name in the document
      cropData.add(fieldName);
    }
  }

  Future<void> saveDataToNewCollection() async {
    // Get the Firestore instance
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    // Create a new collection reference for the new collection
    CollectionReference newCollection = firestore
        .collection('harvest'); // Replace with your desired collection name

    // Create a new document in the new collection with the selected values
    await newCollection.add({
      'crop': cropItem,
    });

    // Reset the dropdown values after saving
    setState(() {
      cropItem = null;
    });

    // Show a success message or perform any other necessary actions
    // ignore: use_build_context_synchronously
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Data saved to new collection'),
      ),
    );
  }

  @override
  void initState() {
    super.initState();
    initializeData();
  }

  clearForm() {
    _fertilizerController.clear();
    _bagsController.clear();
    _sizeController.clear();
    _expenseController.clear();
    _dateController.clear();
    _descController.clear();
    cropData.clear();
    _categoryList.clear();
    // ignore: unrelated_type_equality_checks
    cropItem == initializeData();
    // ignore: unrelated_type_equality_checks
    selectedType == '';
    selectedHarvestType == '';
    selectedWeedingType == '';
    _quantityController.clear();
  }

  // ignore: prefer_typing_uninitialized_variables
  var selectedType;
  final List<String> _categoryList = <String>[
    'Harvest',
    'Planting',
    'Weeding',
    'Plough',
    'Till',
    'Top-Dress',
  ];

  // ignore: prefer_typing_uninitialized_variables
  var selectedHarvestType;
  final List<String> _categoryHarvestList = <String>[
    'Bales',
    'Dozen',
    'grams',
    'Kilograms',
    'Tonnes',
    'Litres',
    'Bags',
  ];
  // ignore: prefer_typing_uninitialized_variables
  var selectedWeedingType;
  final List<String> _categoryWeedingList = <String>[
    'Spraying',
    'Tilling',
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Add Activity'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('farm')
                      .doc('farms')
                      .collection('farm-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    farmName = data!['name'];
                    farmSize = data['size'];

                    return Column(
                      children: [
                        Column(
                          children: [
                            TextFormField(
                              onChanged: (val) {
                                setState(() {
                                  farmName = val;
                                });
                              },
                              readOnly: true,
                              initialValue: farmName,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[a-z A-z0-9]+$').hasMatch(value)) {
                                  return 'Enter valid farm name';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Name',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              onChanged: (val) {
                                  farmSize = val;
                              },
                              readOnly: false,
                              initialValue: farmSize,
                              validator: (value) {
                                if (value == null || !RegExp(r'^\d+(\.\d+)?$').hasMatch(value)) {
                                  return 'Please enter a valid Farm Size';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Size',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                        Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.all(0),
                              child: Row(
                                children: [
                                  const Text('Farm Activities'),
                                  const SizedBox(
                                    width: 30,
                                  ),
                                  Flexible(
                                    child: DropdownButtonFormField<String>(
                                        items: _categoryList
                                            .map((value) => DropdownMenuItem(
                                          value: value,
                                          child: Text(value),
                                        ))
                                            .toList(),
                                        onChanged:
                                            (selectedCategoryType) {
                                          setState(() {
                                            selectedType =
                                                selectedCategoryType;
                                          });
                                        },
                                        value: selectedType,
                                        isExpanded: true,
                                        decoration: const InputDecoration(
                                          labelText: 'Select farm activity',
                                          // Add any other decoration properties you need
                                        ),
                                        validator: (value) {
                                          if (value == null) {
                                            return 'Please Select farm activity';
                                          }
                                          return null; // Return null to indicate the input is valid
                                        },
                                      )
                                  )
                                ],
                              ),
                            ),
                            if (selectedType == 'Weeding')
                              Column(
                                children: [
                                  DropdownButtonFormField<String>(
                                    validator: (value) {
                                      if (value == null ||
                                          !RegExp(r"^[A-Z a-z0-9]+$")
                                              .hasMatch(value)) {
                                        return 'Enter valid crop name';
                                      }
                                      return null;
                                    },
                                    value: cropItem,
                                    onChanged: (value) {
                                      setState(() {
                                        cropItem = value;
                                      });
                                    },
                                    decoration: const InputDecoration(
                                        labelText: 'Crop Name',
                                        border: OutlineInputBorder()),
                                    items: cropData.map((String item) {
                                      return DropdownMenuItem<String>(
                                        value: item,
                                        child: Text(item),
                                      );
                                    }).toList(),
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        const Text('Weeding Type'),
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Flexible(
                                          child: DropdownButtonFormField<String>(
                                            items: _categoryWeedingList
                                                .map((value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(value),
                                            ))
                                                .toList(),
                                            onChanged:
                                                (selectedWeedingCategoryType) {
                                              setState(() {
                                                selectedWeedingType =
                                                    selectedWeedingCategoryType;
                                              });
                                            },
                                            value: selectedWeedingType,
                                            isExpanded: true,
                                            decoration: const InputDecoration(
                                              labelText: 'Select Weeding type',
                                              // Add any other decoration properties you need
                                            ),
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Please select Weeding type';
                                              }
                                              return null; // Return null to indicate the input is valid
                                            },
                                          ),
                                        ),

                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            if (selectedType == 'Harvest')
                              Column(
                                children: [
                                  DropdownButtonFormField<String>(
                                    validator: (value) {
                                      if (value == null ||
                                          !RegExp(r"^[A-Z a-z0-9]+$")
                                              .hasMatch(value)) {
                                        return 'Enter valid crop name';
                                      }
                                      return null;
                                    },
                                    value: cropItem,
                                    onChanged: (value) {
                                      setState(() {
                                        cropItem = value;
                                      });
                                    },
                                    decoration: const InputDecoration(
                                        labelText: 'Crop Name',
                                        border: OutlineInputBorder()),
                                    items: cropData.map((String item) {
                                      return DropdownMenuItem<String>(
                                        value: item,
                                        child: Text(item),
                                      );
                                    }).toList(),
                                  ),
                                  const SizedBox(height: 20),
                                  TextFormField(
                                    controller: _quantityController,
                                    keyboardType: TextInputType.number,
                                    validator: (value) {
                                      if (value == null ||
                                          !RegExp(r'^[0-9]+$')
                                              .hasMatch(value)) {
                                        return 'Enter valid Quantity';
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                      hintText: 'Quantity',
                                      border: OutlineInputBorder(),
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  Container(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        const Text('Harvest Unit'),
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Flexible(
                                          child: DropdownButtonFormField<String>(
                                              items: _categoryHarvestList
                                                  .map((value) => DropdownMenuItem(
                                                value: value,
                                                child: Text(value),
                                              ))
                                                  .toList(),
                                              onChanged:
                                                  (selectedHarvestCategoryType) {
                                                setState(() {
                                                  selectedHarvestType =
                                                      selectedHarvestCategoryType;
                                                });
                                              },
                                              value: selectedHarvestType,
                                              isExpanded: true,
                                              decoration: const InputDecoration(
                                                labelText: 'Select harvest unit',
                                                // Add any other decoration properties you need
                                              ),
                                              validator: (value) {
                                                if (value == null) {
                                                  return 'Please Select harvest unit';
                                                }
                                                return null; // Return null to indicate the input is valid
                                              },
                                            )
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                ],
                              ),
                            if (selectedType == 'Weeding' &&
                                selectedWeedingType == 'Spraying')
                              Column(
                                children: [
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(
                                    controller: _herbController,
                                    validator: (value) {
                                      if (value!.isEmpty &&
                                          !RegExp(r'^[a-z A-Z0-9]+$')
                                              .hasMatch(value)) {
                                        return 'Enter valid herbicide name';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.text,
                                    decoration: const InputDecoration(
                                      hintText: 'Herbicide',
                                      border: OutlineInputBorder(),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 20,
                                  ),
                                  TextFormField(
                                    controller: _quantityController,
                                    validator: (value) {
                                      if (value!.isEmpty &&
                                          !RegExp(r'^[0-9]+$')
                                              .hasMatch(value)) {
                                        return 'Enter valid herbicide quantity';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.number,
                                    decoration: const InputDecoration(
                                      hintText: 'Quantity',
                                      border: OutlineInputBorder(),
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                  Container(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        const Text('Herbicide Unit'),
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Flexible(
                                          child: DropdownButtonFormField<String>(
                                              items: _categoryHarvestList
                                                  .map((value) => DropdownMenuItem(
                                                value: value,
                                                child: Text(value),
                                              ))
                                                  .toList(),
                                              onChanged:
                                                  (selectedHarvestCategoryType) {
                                                setState(() {
                                                  selectedHarvestType =
                                                      selectedHarvestCategoryType;
                                                });
                                              },
                                              value: selectedHarvestType,
                                              isExpanded: true,
                                              decoration: const InputDecoration(
                                                labelText: 'Select herbicide unit',
                                                // Add any other decoration properties you need
                                              ),
                                              validator: (value) {
                                                if (value == null) {
                                                  return 'Please Select herbicide unit';
                                                }
                                                return null; // Return null to indicate the input is valid
                                              },
                                            )
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 10,
                                  ),
                                ],
                              ),
                            if (selectedType == 'Planting' ||
                                selectedType == 'Top-Dress')
                              Column(
                                children: [
                                  const SizedBox(height: 20),
                                  DropdownButtonFormField<String>(
                                    validator: (value) {
                                      if (value== null ||
                                          !RegExp(r"^[A-Z a-z0-9]+$")
                                              .hasMatch(value)) {
                                        return 'Enter valid crop name';
                                      }
                                      return null;
                                    },
                                    value: cropItem,
                                    onChanged: (value) {
                                      setState(() {
                                        cropItem = value;
                                      });
                                    },
                                    decoration: const InputDecoration(
                                        labelText: 'Crop Name',
                                        border: OutlineInputBorder()),
                                    items: cropData.map((String item) {
                                      return DropdownMenuItem<String>(
                                        value: item,
                                        child: Text(item),
                                      );
                                    }).toList(),
                                  ),
                                  const SizedBox(
                                    height: 16,
                                  ),
                                  TextFormField(
                                    controller: _fertilizerController,
                                    validator: (value) {
                                      if (value ==null ||
                                          !RegExp(r'^[a-z A-Z0-9]+$')
                                              .hasMatch(value)) {
                                        return 'Enter valid Fertilizer name';
                                      }
                                      return null;
                                    },
                                    decoration: const InputDecoration(
                                      hintText: 'Fertilizer Name',
                                      border: OutlineInputBorder(),
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  Container(
                                    padding: const EdgeInsets.all(0),
                                    child: Row(
                                      children: [
                                        const Text('Fertilizer Unit'),
                                        const SizedBox(
                                          width: 30,
                                        ),
                                        Flexible(
                                          child: DropdownButtonFormField<String>(
                                            items: _categoryHarvestList
                                                .map((value) => DropdownMenuItem(
                                              value: value,
                                              child: Text(value),
                                            ))
                                                .toList(),
                                            onChanged:
                                                (selectedHarvestCategoryType) {
                                              setState(() {
                                                selectedHarvestType =
                                                    selectedHarvestCategoryType;
                                              });
                                            },
                                            value: selectedHarvestType,
                                            isExpanded: true,
                                            decoration: const InputDecoration(
                                              labelText: 'Select Fertilizer Weight unit',
                                              // Add any other decoration properties you need
                                            ),
                                            validator: (value) {
                                              if (value == null) {
                                                return 'Please select a harvest unit';
                                              }
                                              return null; // Return null to indicate the input is valid
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                  TextFormField(
                                    controller: _quantityController,
                                    validator: (value) {
                                      if (value == null ||
                                          !RegExp(r'^[0-9]+$')
                                              .hasMatch(value)) {
                                        return 'Enter valid quantity';
                                      }
                                      return null;
                                    },
                                    keyboardType: TextInputType.number,
                                    decoration: const InputDecoration(
                                      hintText: 'Quantity',
                                      border: OutlineInputBorder(),
                                    ),
                                  ),
                                  const SizedBox(height: 20),
                                ],
                              ),const SizedBox(height: 20),
                            TextFormField(
                              controller: _expenseController,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                  return 'Enter a valid expense Ammount';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Ammount',
                                border: OutlineInputBorder(),
                              ),
                            ),const SizedBox(height: 20),
                            TextFormField(
                              validator: (value) {
                                if (value == null || value.isEmpty) {
                                  return "Date can't be null";
                                }
                                return null;
                              },
                              readOnly: true,
                              controller: _dateController,
                              onTap: _handleDate,
                              decoration: const InputDecoration(
                                labelText: 'Date',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              maxLines: 5,
                              controller: _descController,
                              validator: (value) {
                                if (value== null ||
                                    !RegExp(r'^[a-z A-Z0-9]+$').hasMatch(value)) {
                                  return 'Enter a valid description';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'Description',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(
                              height: 20,
                            ),
                          ],
                        ),
                        const SizedBox(
                          height: 20,
                        ),

                      ],
                    );
                  },
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.lightGreen[900],
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4),
                        ),
                        minimumSize: const Size(120, 40)
                    ),
                    onPressed: () {

                      if (_formKey.currentState!.validate()) {
                        if (selectedType == 'Harvest') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'crop': cropItem.toString(),
                            'size': farmSize,
                            'quantity': _quantityController.text,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'unit': selectedHarvestType.toString(),
                            'date': _dateController.text,
                            'activity': selectedType.toString()
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => ActivitiesSummary(id: '',)));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        } }
                      if(_formKey.currentState!.validate()){
                      if (selectedType == 'Planting') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'crop': cropItem.toString(),
                            'size': farmSize,
                            'quantity': _quantityController.text,
                            'desc': _descController.text,
                            'unit': selectedHarvestType.toString(),
                            'date': _dateController.text,
                            'fertilizer': _fertilizerController
                                .text,
                            'activity': selectedType.toString(),
                            'expense': _expenseController.text,
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => const ActivitiesSummary(id: '',)));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        }}
                      if(_formKey.currentState!.validate()){
                        if (selectedType == 'Weeding' &&
                            selectedWeedingType == 'Tilling') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'crop': cropItem.toString(),
                            'size': farmSize,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'date': _dateController.text,
                            'type': selectedWeedingType.toString(),
                            'activity': selectedType.toString()
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => const ActivitiesSummary(id: '')));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        }
                        else if (selectedType == 'Weeding' &&
                            selectedWeedingType == 'Spraying') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'crop': cropItem.toString(),
                            'size': farmSize,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'date': _dateController.text,
                            'type': selectedWeedingType
                                .toString(),
                            'herb': _herbController.text,
                            'quantity': _quantityController.text,
                            'activity': selectedType.toString(),
                            'unit': selectedHarvestType
                                .toString(),
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => const ActivitiesSummary(id : '')));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        }

                        else if (selectedType == 'Plough') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'size': farmSize,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'date': _dateController.text,
                            'activity': selectedType.toString()
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => ActivitiesSummary(id: widget.id,)));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        }

                        else if (selectedType == 'Till') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'rid': FirebaseAuth.instance.currentUser?.uid,
                            'farm': farmName,
                            'size': farmSize,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'date': _dateController.text,
                            'activity': selectedType.toString()
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => ActivitiesSummary(id: widget.id,)));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });

                        }
                        else if (selectedType == 'Top-Dress') {
                          FirebaseFirestore.instance
                              .collection('farm')
                              .doc('activities')
                              .collection('farm-activity')
                              .doc()
                              .set({
                            'farm': farmName,
                            'crop': cropItem.toString(),
                            'size': farmSize,
                            'quantity': _quantityController.text,
                            'desc': _descController.text,
                            'expense': _expenseController.text,
                            'unit': selectedHarvestType
                                .toString(),
                            'date': _dateController.text,
                            'fertilizer': _fertilizerController
                                .text,
                            'activity': selectedType.toString()
                          })
                          // ignore: avoid_print
                              .then((value) {
                            clearForm();
                            Navigator.push(context,
                                MaterialPageRoute(builder: (
                                    context) => ActivitiesSummary(id:  widget.id,)));
                          }).catchError((error) {
                            // ignore: avoid_print
                            print('Something went wrong: $error');
                          });
                        }
                      }
                    },
                    child: const Text('Save'),
                  ),
                  ElevatedButton(
                      style: ElevatedButton.styleFrom(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(4),
                          ),
                          backgroundColor: Colors.lightGreen[900],
                          minimumSize: const Size(120, 40)
                      ),
                      onPressed: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context)=>ActivitiesSummary(id: widget.id)));
                      }, child: const Text('View'))
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
