import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/crop/add_crop.dart';
import 'package:meek/drawer/invest/farm/data/add_farm.dart';

class EditFarm extends StatefulWidget {
  final String id;
  const  EditFarm({Key? key, required this.id}) : super(key: key);

  @override
  State<EditFarm> createState() => _EditFarmState();
}

class _EditFarmState extends State<EditFarm> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference<Map<String, dynamic>> update =
  FirebaseFirestore.instance.collection('farm').doc('farms').collection('farm-data');

  String leasePeriod = '';
  String leaseAmmount = '';
  String leassorId = '';
  String leassorPhone = '';

  Future<void> machineUpdate(id, farmName, farmSize, farmLoc, farmDesc) async {
    update
        .doc(id)
        .update({
      'name': farmName,
      'size': farmSize,
      'desc': farmDesc,
      'loc': farmLoc,
      'phone':leassorPhone,
      'idnum': leassorId,
      'ammount': leaseAmmount,
      'period': leasePeriod,
    })
        .then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Farm Successfully Updated'))))
        .catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Failed to update farm')));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Add Activity'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('farm')
                      .doc('farms')
                      .collection('farm-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }

                    var data = snapshot.data!.data();
                    var farmName = data?['name'] ?? '';
                    var farmSize = data?['size'] ?? '';
                    var farmLoc = data?['loc'] ?? '';
                    var farmDesc = data?['desc'] ?? '';

                    return Column(
                      children: [
                        Column(
                          children: [
                            TextFormField(
                              onChanged: (val) {
                                  farmName = val;
                              },
                              initialValue: farmName,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[a-zA-Z ]+$').hasMatch(value)) {
                                  return 'Please enter a valid Farm name';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Name',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20,),
                            TextFormField(
                              onChanged: (val) {
                                  farmSize = val;
                              },
                              initialValue: farmSize,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^\d+(\.\d+)?$').hasMatch(value)) {
                                  return 'Please enter a valid Farm Size';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Size',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20,),
                            TextFormField(
                              onChanged: (val) {
                                  farmLoc = val;
                              },
                              initialValue: farmLoc,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                  return 'Please enter a valid Farm Location';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Location',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20,),
                            TextFormField(
                              onChanged: (val) {
                                  farmDesc = val;
                              },
                              initialValue: farmDesc,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                  return 'Please enter a valid Farm Description';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Description:',
                                border: OutlineInputBorder(),
                              ),
                              maxLines: null,
                            ),
                          ],
                        ),


                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.lightGreen[900],
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  machineUpdate(widget.id, farmName, farmSize, farmLoc, farmDesc)
                                      .then((value) => Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => const FarmList(),
                                    ),
                                  ))
                                      .catchError((error) {
                                    // Handle the error
                                  });
                                }
                              },
                              child: const Text('Update'),
                            ),
                          ],
                        ),
                      ],
                    );
                  },
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

