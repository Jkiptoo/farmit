import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/farm/data/add_farm.dart';
import 'package:meek/farm_dash/home.dart';

class FarmOwnAdd extends StatefulWidget {
  const FarmOwnAdd({Key? key}) : super(key: key);

  @override
  State<FarmOwnAdd> createState() => _FarmOwnAddState();
}

class _FarmOwnAddState extends State<FarmOwnAdd> {
  var firebaseUser = FirebaseAuth.instance.currentUser;

  final _formKey = GlobalKey<FormState>();
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDate() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    _dateController.text = _dateFormatter.format(date!);

    // ignore: unnecessary_null_comparison
    if (date == null && date == _time) {
      date;
    }
  }

  var name = '';
  var size = '';
  var date = '';
  var loc = '';
  var desc = '';

  final _sizeController = TextEditingController();
  final _farmController = TextEditingController();
  final _dateController = TextEditingController();
  final _descController = TextEditingController();
  final _locController = TextEditingController();
  final _periodController = TextEditingController();
  final _idController = TextEditingController();
  final _phoneController = TextEditingController();
  final _ammountController = TextEditingController();

  clearText() {
    _sizeController.clear();
    _farmController.clear();
    _dateController.clear();
    _locController.clear();
    _descController.clear();
    _phoneController.clear();
    _periodController.clear();
    _idController.clear();
    _ammountController.clear();
    _ownershipType.clear();
  }

  var collectionReference = FirebaseFirestore.instance
      .collection('farm')
      .doc('farm-ownership')
      .collection('owned')
      .doc();
  Future<void> farmsAdd() async {
    return await collectionReference
        .set({
          'rid': FirebaseAuth.instance.currentUser?.uid,
          'name': name,
          'size': size,
          'date': date,
          'loc': loc,
          'desc': desc
        })
        // ignore: avoid_print
        .then((value) => print('Farm added Successfully'))
        .catchError((error) {
          // ignore: avoid_print
          print('Something went wrong: $error');
        });
  }

  // ignore: prefer_typing_uninitialized_variables
  var selectedFarmType;
  final List<String> _ownershipType = <String>['Own', 'Lease'];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => const FarmList()));
          },
        ),
        title: const Text('Add Farm'),
        centerTitle: true,
        backgroundColor: Colors.lightGreen[900],
      ),
      body: Container(
        padding: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextFormField(
                      validator: (value) {
                        if (value!.isEmpty &&
                            !RegExp(r'^[a-z A-z0-9]+$').hasMatch(value)) {
                          return 'Enter valid farm name';
                        }
                        return null;
                      },
                      controller: _farmController,
                      decoration: const InputDecoration(labelText: 'Farm Name'),
                    ),
                    TextFormField(
                      keyboardType: TextInputType.number,validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r'^\d+(\.\d+)?$').hasMatch(value)) {
                        return 'Enter a valid farm size';
                      }
                      return null;
                    },
                      controller: _sizeController,
                      decoration: const InputDecoration(labelText: 'Farm Size'),
                    ),
                    Container(
                      padding: const EdgeInsets.all(0),
                      child: Row(
                        children: [
                          const Text('Farm Activities'),
                          const SizedBox(
                            width: 30,
                          ),
                          Flexible(
                            child: DropdownButtonFormField<String>(
                              items: _ownershipType
                                  .map((value) => DropdownMenuItem(
                                        value: value,
                                        child: Text(value),
                                      ))
                                  .toList(),
                              onChanged: (selectedFarmCategoryType) {
                                setState(() {
                                  selectedFarmType = selectedFarmCategoryType;
                                });
                              },
                              value: selectedFarmType,
                              isExpanded: true,
                              decoration: const InputDecoration(
                                labelText: 'Select farm type',
                                // Add any other decoration properties you need
                              ),
                              validator: (value) {
                                if (value == null) {
                                  return 'Please select a farm type';
                                }
                                return null; // Return null to indicate the input is valid
                              },
                            ),
                          )
                        ],
                      ),
                    ),
                    if (selectedFarmType == 'Lease')
                      Column(
                        children: [
                          TextFormField(
                            keyboardType: TextInputType.text,
                            validator: (value) {
                              if (value!.isEmpty &&
                                  !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                return 'Enter valid lease period';
                              }
                              return null;
                            },
                            controller: _periodController,
                            decoration:
                                const InputDecoration(labelText: 'Period'),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            maxLength: 8,
                            validator: (value) {
                              if (value!.isEmpty &&
                                  !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                return 'Enter valid ID number';
                              }
                              return null;
                            },
                            controller: _idController,
                            decoration:
                                const InputDecoration(labelText: 'ID Number'),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            maxLength: 10,
                            validator: (value) {
                              if (value!.isEmpty &&
                                  !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                return 'Enter valid phone number';
                              }
                              return null;
                            },
                            controller: _phoneController,
                            decoration: const InputDecoration(
                                labelText: 'Phone Number'),
                          ),
                          TextFormField(
                            keyboardType: TextInputType.number,
                            validator: (value) {
                              if (value!.isEmpty &&
                                  !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                return 'Enter valid ammount';
                              }
                              return null;
                            },
                            controller: _ammountController,
                            decoration:
                                const InputDecoration(labelText: 'Ammount'),
                          ),
                        ],
                      ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value!.isEmpty &&
                            !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                          return 'Enter valid farm location';
                        }
                        return null;
                      },
                      controller: _locController,
                      decoration: const InputDecoration(labelText: 'Location'),
                    ),
                    TextFormField(
                      validator: (value) {
                        if (value != null && value.isEmpty) {
                          return "Date can't be null";
                        }
                        return null;
                      },
                      readOnly: true,
                      controller: _dateController,
                      onTap: _handleDate,
                      decoration:
                          const InputDecoration(labelText: 'Date Added'),
                    ),
                    const SizedBox(
                      height: 30,
                    ),
                    TextFormField(
                      keyboardType: TextInputType.text,
                      validator: (value) {
                        if (value!.isEmpty &&
                            !RegExp(r'^[a-zA-z0-9]+$').hasMatch(value)) {
                          return 'Enter valid farm description';
                        }
                        return null;
                      },
                      controller: _descController,
                      decoration: const InputDecoration(
                          border: OutlineInputBorder(),
                          labelText: 'Farm Description'),
                    ),
                    const SizedBox(
                      height: 20,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                fixedSize: const Size.fromWidth(90),
                                backgroundColor: Colors.lightGreen[900],
                                minimumSize: const Size(120, 40)),
                            onPressed: () {
                              checkFarmNameUniquenessAndPerformAction();
                            },
                            child: const Text(
                              'ADD',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                  letterSpacing: 1.2),
                            )),
                        const SizedBox(
                          width: 30,
                        ),
                        ElevatedButton(
                            style: ElevatedButton.styleFrom(
                                fixedSize: const Size.fromWidth(90),
                                backgroundColor: Colors.lightGreen[900],
                                minimumSize: const Size(120, 40)),
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => const HomePage()));
                            },
                            child: const Text(
                              'CANCEL',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  color: Colors.white,
                                  letterSpacing: 1.2),
                            )),
                      ],
                    )
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  Future<void> checkFarmNameUniquenessAndPerformAction() async {
    if (_formKey.currentState!.validate()) {
      String farmName = _farmController.text.toLowerCase();

      bool isFarmNameUnique = await checkFarmNameUniqueness(farmName);

      if (isFarmNameUnique) {
        // Proceed with adding the farm details
        if (selectedFarmType == 'Own') {
          var collectionReference = FirebaseFirestore.instance
              .collection('farm')
              .doc('farms')
              .collection('farm-data')
              .doc();
          var documentReference = collectionReference;

          documentReference.set({
            'rid': firebaseUser?.uid,
            'documentId': documentReference.id,
            'name': farmName,
            'size': _sizeController.text,
            'date': _dateController.text,
            'desc': _descController.text,
            'ownership': selectedFarmType.toString(),
            'loc': _locController.text,
          }).then((value) {
            clearText();
            ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Farm added successfully')));
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FarmList()));
          });
        } else if (selectedFarmType == 'Lease') {
          var collectionReference = FirebaseFirestore.instance
              .collection('farm')
              .doc('farms')
              .collection('farm-data')
              .doc();
          var documentReference = collectionReference;

          documentReference.set({
            'rid': firebaseUser?.uid,
            'documentId': documentReference.id,
            'name': farmName,
            'size': _sizeController.text,
            'date': _dateController.text,
            'desc': _descController.text,
            'ownership': selectedFarmType.toString(),
            'loc': _locController.text,
            'period': _periodController.text,
            'idnum': _idController.text,
            'phone': _phoneController.text,
            'ammount': _ammountController.text,
          }).then((value) {
            clearText();
            ScaffoldMessenger.of(context).showSnackBar(
                const SnackBar(content: Text('Leased Farm added successfully')));
            Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const FarmList()));
          });
        }
      } else {
        // Display an error message when the farm name is not unique
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Error'),
              content: const Text('The farm name already exists. Please enter a unique farm name.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      }
    }
  }
  Future<bool> checkFarmNameUniqueness(String farmName) async {
    // Query the collection to check if a document with the same farm name already exists
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore.instance
        .collection('farm')
        .doc('farms')
        .collection('farm-data')
        .where('name', isEqualTo: farmName)
        .limit(1)
        .get();

    // Return true if no matching document is found, indicating the farm name is unique
    return querySnapshot.docs.isEmpty;
  }

}
