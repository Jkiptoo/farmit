import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/farm/records/view_farm.dart';

class ActivitiesSummary extends StatefulWidget {
  const ActivitiesSummary({Key? key}) : super(key: key);

  @override
  State<ActivitiesSummary> createState() => _ActivitiesSummaryState();
}

class _ActivitiesSummaryState extends State<ActivitiesSummary> {

  var comp = FirebaseFirestore.instance
      .collection('farm').
  doc('activities').collection('farm-activity')
      .where('activity', isEqualTo: 'Spraying')
      .get().then((value){

  });

  /* ======================================================================================================
                             Checking the current user in the app
     ======================================================================================================*/
  var firebaseUser = FirebaseAuth.instance.currentUser;

/* =========================================================================================================
                                 Adding date picker
   =========================================================================================================*/


  /*=======================================================================================================
                                      Deleting machinery data
    =======================================================================================================*/
  CollectionReference collectionReference =
  FirebaseFirestore.instance.collection('expense');

  Future<void> deleteMachine(id) async {
    return await collectionReference.doc(id).delete().then((value) {
      // ignore: avoid_print
      print('Expense Deleted Successfully');
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Expense Deleted')));
    }).catchError((error) {
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text('Something went wrong: $error')));
    });
  }

  /*===========================================================================================================
                                      Print Expense data
  =============================================================================================================*/

  List<String> cropData = []; // List to store data from crop
  List<String> farmData = []; // List to store data from farm
  List<String> machineData = []; // List to store data from Machinery
  List<String> animalData = []; // List to store data from Animal


  String? cropItem; // Selected item from crop dropdown
  String? farmItem; // Selected item from farm dropdown
  String? machineItem; // Selected item from Machinery dropdown
  String? animalItem; // Selected item from animal dropdown

  @override
  void initState() {
    super.initState();
    initializeData();
  }

  Future<void> initializeData() async {
    await getDataFromCollections();
    setState(() {});
  }


  Future<void> getDataFromCollections() async {
    CollectionReference crop = FirebaseFirestore.instance.collection('crop').doc('crops').collection('crop-data');
    CollectionReference machine = FirebaseFirestore.instance.collection('machine').doc('data').collection('machine-data');
    CollectionReference animal = FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');

    QuerySnapshot cropSnapshot = await crop.get();
    QuerySnapshot machinerySnapshot = await machine.get();
    QuerySnapshot animalSnapshot = await animal.get();

    // Clear the lists before populating with new data
    cropData.clear();
    farmData.clear();
    machineData.clear();
    animalData.clear();

    // Populate the lists with specific fields from document data
    for (var document in cropSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data[
      'crop']; // field name in the collection
      cropData.add(fieldName);
    }

    for (var document in machinerySnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data['type']; // field name in the collection
      machineData.add(fieldName);
    }

    for (var document in animalSnapshot.docs) {
      Map<String, dynamic> data = document.data() as Map<String, dynamic>;
      String fieldName = data['type']; // field name in the collection
      animalData.add(fieldName);
    }
  }

  Future<void> saveDataToNewCollection() async {
    // Get the Firestore instance
    FirebaseFirestore firestore = FirebaseFirestore.instance;

    // Create a new collection reference for the new collection
    CollectionReference newCollection = firestore
        .collection('expenses'); // Replace with your desired collection name

    // Create a new document in the new collection with the selected values
    await newCollection
        .add({'crop': cropItem, 'farm': farmItem, 'machine': machineItem, 'animal': animalItem});

    // Reset the dropdown values after saving
    setState(() {
      cropItem = null;
      farmItem = null;
      machineItem = null;
      animalItem = null;
    });

    // Show a success message or perform any other necessary actions
    // ignore: use_build_context_synchronously
    ScaffoldMessenger.of(context).showSnackBar(
      const SnackBar(
        content: Text('Data saved to new collection'),
      ),
    );
  }



  /*===================================================================================================================================================
                                         Adding Income to collection
  ====================================================================================================================================================*/
  CollectionReference reference =
  FirebaseFirestore.instance.collection('income');
  // ignore: non_constant_identifier_names


  /*==================================================================================================================================================*/

  /*===================================================================================================================================================
  *                                              List of Quantity units
  * ===================================================================================================================================================*/


  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.lightGreen[900],
            title: const Text(
              'Transactions',
              style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24),
            ),
            centerTitle: true,
          ),
          body: TabBarView(children: [ DefaultTabController(
            length: 6,
            child: SizedBox(
              height: 210,
              child: Column(
                children: [
                  const SizedBox(
                    height: 20,
                  ),
                  TabBar(tabs: const [
                    Tab(
                      text: 'Plough',
                      icon: Icon(Icons.grass),
                    ),
                    Tab(
                      text: 'Till',
                      icon: Icon(Icons.build),
                    ),
                    Tab(
                      text: 'Planting',
                      icon: Icon(Icons.pets),
                    ),
                    Tab(
                      text: 'Weeding',
                      icon: Icon(Icons.build),
                    ),
                    Tab(
                      text: 'Top-Dress',
                      icon: Icon(Icons.grass),
                    ),

                    Tab(
                      text: 'Harvest',
                      icon: Icon(Icons.pets),
                    ),
                  ],
                    labelColor:
                    Colors.white, // Color of the selected tab's label
                    unselectedLabelColor:
                    Colors.grey, // Color of unselected tab's label
                    indicator: BoxDecoration(
                      borderRadius: BorderRadius.circular(
                          8), // Rounded corner indicator
                      color:
                      Colors.lightGreen[900], // Color of the indicator
                    ),
                    indicatorSize:
                    TabBarIndicatorSize.tab, // Match the tab's size
                    indicatorPadding:
                    const EdgeInsets.symmetric(horizontal: 8),
                  ),
                  Expanded(child: TabBarView(
                    children: [
                      Padding(
                          padding: const EdgeInsets.all(5),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                              padding: const EdgeInsets.all(10),
                              child: StreamBuilder<QuerySnapshot>(
                                stream: getCollections(),
                                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                    const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  final List storeFarm = [];
                                  snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                    Map a = documentSnapshot.data() as Map<String, dynamic>;
                                    storeFarm.add(a);
                                    a['id'] = documentSnapshot.id;
                                  }).toList();
                                  return SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      children: [
                                        for (var i = 0; i < storeFarm.length; i++) ...[
                                          SizedBox(
                                            height: 110,
                                            width: double.infinity,
                                            child: Material(
                                              child: InkWell(
                                                onTap: (){
                                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                },
                                                child: Card(
                                                    child: SingleChildScrollView(
                                                      scrollDirection: Axis.horizontal,
                                                      child: Row(
                                                        children: [
                                                          const SizedBox(
                                                            width: 20,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                            children: [
                                                              Text(
                                                                "Farm:",
                                                                style: TextStyle(color: Colors.grey[800]),
                                                              ),
                                                              Text(
                                                                "Size:",
                                                                style: TextStyle(color: Colors.grey[800]),
                                                              ),
                                                              Text(
                                                                "Expense:",
                                                                style: TextStyle(color: Colors.grey[800]),
                                                              ),
                                                              Text(
                                                                "Date:",
                                                                style: TextStyle(color: Colors.grey[800]),
                                                              ),Text(
                                                                "Description:",
                                                                style: TextStyle(color: Colors.grey[800]),
                                                              ),


                                                            ],
                                                          ),
                                                          const SizedBox(
                                                            width: 35,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                            children: [
                                                              Text(
                                                                storeFarm[i]['farm'],
                                                                style: TextStyle(
                                                                  color: Colors.green[800],
                                                                ),
                                                              ),
                                                              Text(
                                                                  storeFarm[i]['size'],style: TextStyle(
                                                                  color: Colors.green[800]
                                                              )),
                                                              Text(
                                                                storeFarm[i]['expense'],
                                                                style: TextStyle(
                                                                  color: Colors.green[800],
                                                                ),
                                                              ),
                                                              Text(
                                                                  storeFarm[i]['date'],style: TextStyle(
                                                                  color: Colors.green[800]
                                                              )),Text(
                                                                storeFarm[i]['desc'],
                                                                style: TextStyle(
                                                                  color: Colors.green[800],
                                                                ),
                                                              ),

                                                            ],
                                                          ),
                                                          const SizedBox(
                                                            width: 100,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment: CrossAxisAlignment.start,
                                                            mainAxisAlignment: MainAxisAlignment.end,
                                                            children: [
                                                              IconButton(
                                                                  onPressed: () {
                                                                    showDialog(context: context, builder: (BuildContext context){
                                                                      return Container(
                                                                        height: 100,
                                                                      );
                                                                    });
                                                                  },
                                                                  icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                            ],
                                                          )
                                                        ],
                                                      ),
                                                    )
                                                ),



                                              ),
                                            ),
                                          )
                                        ]
                                      ],
                                    ),
                                  );
                                },
                              ),
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(20),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: StreamBuilder<QuerySnapshot>(
                                  stream: getOwnedDocumentsStream(),
                                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.connectionState == ConnectionState.waiting) {
                                      const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    final List storeFarm = [];
                                    snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                      Map a = documentSnapshot.data() as Map<String, dynamic>;
                                      storeFarm.add(a);
                                      a['id'] = documentSnapshot.id;
                                    }).toList();
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          for (var i = 0; i < storeFarm.length; i++) ...[
                                            SizedBox(
                                              height: 110,
                                              width: double.infinity,
                                              child: Material(
                                                child: InkWell(
                                                  onTap: (){
                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                  },
                                                  child: Card(
                                                      child: SingleChildScrollView(
                                                        scrollDirection: Axis.horizontal,
                                                        child: Row(
                                                          children: [
                                                            const SizedBox(
                                                              width: 20,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                              children: [
                                                                Text(
                                                                  "Farm:",
                                                                  style: TextStyle(color: Colors.grey[800]),
                                                                ),
                                                                Text(
                                                                  "Size:",
                                                                  style: TextStyle(color: Colors.grey[800]),
                                                                ),
                                                                Text(
                                                                  "Expense:",
                                                                  style: TextStyle(color: Colors.grey[800]),
                                                                ),
                                                                Text(
                                                                  "Date:",
                                                                  style: TextStyle(color: Colors.grey[800]),
                                                                ),Text(
                                                                  "Description:",
                                                                  style: TextStyle(color: Colors.grey[800]),
                                                                ),


                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              width: 35,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                              children: [
                                                                Text(
                                                                  storeFarm[i]['farm'],
                                                                  style: TextStyle(
                                                                    color: Colors.green[800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                    storeFarm[i]['size'],style: TextStyle(
                                                                    color: Colors.green[800]
                                                                )),
                                                                Text(
                                                                  storeFarm[i]['expense'],
                                                                  style: TextStyle(
                                                                    color: Colors.green[800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                    storeFarm[i]['date'],style: TextStyle(
                                                                    color: Colors.green[800]
                                                                )),Text(
                                                                  storeFarm[i]['desc'],
                                                                  style: TextStyle(
                                                                    color: Colors.green[800],
                                                                  ),
                                                                ),

                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              width: 100,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment: CrossAxisAlignment.start,
                                                              mainAxisAlignment: MainAxisAlignment.end,
                                                              children: [
                                                                IconButton(
                                                                    onPressed: () {
                                                                      showDialog(context: context, builder: (BuildContext context){
                                                                        return Container(
                                                                          height: 100,
                                                                        );
                                                                      });
                                                                    },
                                                                    icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                              ],
                                                            )
                                                          ],
                                                        ),
                                                      )
                                                  ),



                                                ),
                                              ),
                                            )
                                          ]
                                        ],
                                      ),
                                    );
                                  },
                                )
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(7),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: StreamBuilder<QuerySnapshot>(
                                  stream: getPlantingStream(),
                                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.connectionState == ConnectionState.waiting) {
                                      const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    final List storeFarm = [];
                                    snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                      Map a = documentSnapshot.data() as Map<String, dynamic>;
                                      storeFarm.add(a);
                                      a['id'] = documentSnapshot.id;
                                    }).toList();
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          for (var i = 0; i < storeFarm.length; i++) ...[
                                            SizedBox(
                                              width: double.infinity,
                                              child: Material(
                                                child: InkWell(
                                                    onTap: (){
                                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                    },
                                                    child: IntrinsicHeight(
                                                      child: Card(
                                                          child: SingleChildScrollView(
                                                            scrollDirection: Axis.horizontal,
                                                            child: Row(
                                                              children: [
                                                                const SizedBox(
                                                                  width: 20,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                                  children: [
                                                                    Text(
                                                                      "Farm:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Size:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Expense:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Date:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),Text(
                                                                      "Fertilizer:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Quantity:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Description:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),

                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 35,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                  children: [
                                                                    Text(
                                                                      storeFarm[i]['farm'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        storeFarm[i]['size'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),
                                                                    Text(
                                                                        storeFarm[i]['expense'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),
                                                                    Text(
                                                                        storeFarm[i]['date'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),Text(
                                                                      storeFarm[i]['fertilizer'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),

                                                                    Row(
                                                                      children: [
                                                                        Text(
                                                                          storeFarm[i]['quantity'],
                                                                          style: TextStyle(
                                                                            color: Colors.green[800],
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          storeFarm[i]['unit'],
                                                                          style: TextStyle(
                                                                            color: Colors.green[800],
                                                                          ),)
                                                                      ],
                                                                    ),
                                                                    Text(
                                                                      storeFarm[i]['desc'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),

                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 100,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    IconButton(
                                                                        onPressed: () {
                                                                          showDialog(context: context, builder: (BuildContext context){
                                                                            return Container(
                                                                              height: 100,
                                                                            );
                                                                          });
                                                                        },
                                                                        icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          )
                                                      ),
                                                    )



                                                ),
                                              ),
                                            )
                                          ]
                                        ],
                                      ),
                                    );
                                  },
                                )
                            ),
                          )),
                      Padding(
                          padding: const EdgeInsets.all(5),
                          child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: StreamBuilder<QuerySnapshot>(
                                stream: getWeedingStream(),
                                builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                  if (snapshot.connectionState == ConnectionState.waiting) {
                                    const Center(
                                      child: CircularProgressIndicator(),
                                    );
                                  }
                                  final List storeFarm = [];
                                  snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                    Map a = documentSnapshot.data() as Map<String, dynamic>;
                                    storeFarm.add(a);
                                    a['id'] = documentSnapshot.id;

                                  }).toList();
                                  return SingleChildScrollView(
                                    scrollDirection: Axis.vertical,
                                    child: Column(
                                      children: [
                                        for (var i = 0; i < storeFarm.length; i++) ...[
                                          SizedBox(
                                            width: double.infinity,
                                            child: Material(
                                              child: InkWell(
                                                  onTap: (){
                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                  },
                                                  child: IntrinsicHeight(
                                                    child: Card(
                                                        child: SingleChildScrollView(
                                                          scrollDirection: Axis.horizontal,
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 20,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                                children: [
                                                                  Text(
                                                                    "Farm:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Size:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Crop:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Expense:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Date:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                  if(storeFarm[i]['Spraying'] == true)
                                                                    Column(
                                                                      children: [
                                                                        Text(
                                                                          "Herbicide:",
                                                                          style: TextStyle(color: Colors.grey[800]),
                                                                        ),
                                                                        Text(
                                                                          "Quantity:",
                                                                          style: TextStyle(color: Colors.grey[800]),
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  Text(
                                                                    "Description:",
                                                                    style: TextStyle(color: Colors.grey[800]),
                                                                  ),
                                                                ],
                                                              ),
                                                              const SizedBox(
                                                                width: 35,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                children: [
                                                                  Text(
                                                                    storeFarm[i]['farm'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]['size'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]['crop'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]['expense'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]['date'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                  if (storeFarm[i]['Spraying'] == true)
                                                                    Column(
                                                                      children: [
                                                                        Text(
                                                                          storeFarm[i]['herb'],
                                                                          style: TextStyle(color: Colors.grey[800]),
                                                                        ),
                                                                        Row(
                                                                          children: [
                                                                            Text(
                                                                              storeFarm[i]['quantity'],
                                                                              style: TextStyle(
                                                                                color: Colors.green[800],
                                                                              ),
                                                                            ),
                                                                            SizedBox(
                                                                              width: 5,
                                                                            ),
                                                                            Text(
                                                                              storeFarm[i]['unit'],
                                                                              style: TextStyle(
                                                                                color: Colors.green[800],
                                                                              ),
                                                                            ),
                                                                          ],
                                                                        ),
                                                                      ],
                                                                    ),
                                                                  Text(
                                                                    storeFarm[i]['desc'],
                                                                    style: TextStyle(
                                                                      color: Colors.green[800],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                              const SizedBox(
                                                                width: 100,
                                                              ),

                                                              const SizedBox(
                                                                width: 100,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                children: [
                                                                  IconButton(
                                                                      onPressed: () {
                                                                        showDialog(context: context, builder: (BuildContext context){
                                                                          return Container(
                                                                            height: 100,
                                                                          );
                                                                        });
                                                                      },
                                                                      icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                                ],
                                                              )
                                                            ],
                                                          ),
                                                        )
                                                    ),
                                                  )



                                              ),
                                            ),
                                          )
                                        ]
                                      ],
                                    ),
                                  );
                                },
                              )
                          )),
                      Padding(
                          padding: EdgeInsets.all(20),
                          child: SingleChildScrollView(
                            scrollDirection: Axis.vertical,
                            child: Padding(
                                padding: EdgeInsets.all(10),
                                child: StreamBuilder<QuerySnapshot>(
                                  stream: getTopDressStream(),
                                  builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.connectionState == ConnectionState.waiting) {
                                      const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    final List storeFarm = [];
                                    snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                      Map a = documentSnapshot.data() as Map<String, dynamic>;
                                      storeFarm.add(a);
                                      a['id'] = documentSnapshot.id;
                                    }).toList();
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          for (var i = 0; i < storeFarm.length; i++) ...[
                                            SizedBox(
                                              width: double.infinity,
                                              child: Material(
                                                child: InkWell(
                                                    onTap: (){
                                                      Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                    },
                                                    child: IntrinsicHeight(
                                                      child: Card(
                                                          child: SingleChildScrollView(
                                                            scrollDirection: Axis.horizontal,
                                                            child: Row(
                                                              children: [
                                                                const SizedBox(
                                                                  width: 20,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                                  children: [
                                                                    Text(
                                                                      "Farm:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Size:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Expense:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Date:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),Text(
                                                                      "Fertilizer:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Quantity:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Description:",
                                                                      style: TextStyle(color: Colors.grey[800]),
                                                                    ),

                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 35,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                  children: [
                                                                    Text(
                                                                      storeFarm[i]['farm'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        storeFarm[i]['size'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),
                                                                    Text(
                                                                        storeFarm[i]['expense'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),
                                                                    Text(
                                                                        storeFarm[i]['date'],style: TextStyle(
                                                                        color: Colors.green[800]
                                                                    )),Text(
                                                                      storeFarm[i]['fertilizer'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),

                                                                    Row(
                                                                      children: [
                                                                        Text(
                                                                          storeFarm[i]['quantity'],
                                                                          style: TextStyle(
                                                                            color: Colors.green[800],
                                                                          ),
                                                                        ),
                                                                        SizedBox(width: 5,),
                                                                        Text(
                                                                          storeFarm[i]['unit'],
                                                                          style: TextStyle(
                                                                            color: Colors.green[800],
                                                                          ),)
                                                                      ],
                                                                    ),
                                                                    Text(
                                                                      storeFarm[i]['desc'],
                                                                      style: TextStyle(
                                                                        color: Colors.green[800],
                                                                      ),
                                                                    ),

                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 100,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                                  children: [
                                                                    IconButton(
                                                                        onPressed: () {
                                                                          showDialog(context: context, builder: (BuildContext context){
                                                                            return Container(
                                                                              height: 100,
                                                                            );
                                                                          });
                                                                        },
                                                                        icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                                  ],
                                                                )
                                                              ],
                                                            ),
                                                          )
                                                      ),
                                                    )



                                                ),
                                              ),
                                            )
                                          ]
                                        ],
                                      ),
                                    );
                                  },
                                )
                            ),
                          )),
                      Padding(
                          padding: EdgeInsets.all(20),
                          child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                  padding: EdgeInsets.all(10),
                                  child: StreamBuilder<QuerySnapshot>(
                                    stream: getHarvestStream(),
                                    builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.connectionState == ConnectionState.waiting) {
                                        const Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      final List storeFarm = [];
                                      snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
                                        Map a = documentSnapshot.data() as Map<String, dynamic>;
                                        storeFarm.add(a);
                                        a['id'] = documentSnapshot.id;
                                      }).toList();
                                      return SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          children: [
                                            for (var i = 0; i < storeFarm.length; i++) ...[
                                              SizedBox(
                                                width: double.infinity,
                                                child: Material(
                                                  child: InkWell(
                                                      onTap: (){
                                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewFarm(id: storeFarm[i]['id'],)));
                                                      },
                                                      child: IntrinsicHeight(
                                                        child: Card(
                                                            child: SingleChildScrollView(
                                                              scrollDirection: Axis.horizontal,
                                                              child: Row(
                                                                children: [
                                                                  const SizedBox(
                                                                    width: 20,
                                                                  ),
                                                                  Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                                                    children: [
                                                                      Text(
                                                                        "Farm:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Size:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Expense:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Date:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Quantity:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Description:",
                                                                        style: TextStyle(color: Colors.grey[800]),
                                                                      ),

                                                                    ],
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 35,
                                                                  ),
                                                                  Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                                    children: [
                                                                      Text(
                                                                        storeFarm[i]['farm'],
                                                                        style: TextStyle(
                                                                          color: Colors.green[800],
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                          storeFarm[i]['size'],style: TextStyle(
                                                                          color: Colors.green[800]
                                                                      )),
                                                                      Text(
                                                                          storeFarm[i]['expense'],style: TextStyle(
                                                                          color: Colors.green[800]
                                                                      )),
                                                                      Text(
                                                                          storeFarm[i]['date'],style: TextStyle(
                                                                          color: Colors.green[800]
                                                                      )),

                                                                      Row(
                                                                        children: [
                                                                          Text(
                                                                            storeFarm[i]['quantity'],
                                                                            style: TextStyle(
                                                                              color: Colors.green[800],
                                                                            ),
                                                                          ),
                                                                          Text(
                                                                            storeFarm[i]['unit'],
                                                                            style: TextStyle(
                                                                              color: Colors.green[800],
                                                                            ),)
                                                                        ],
                                                                      ),
                                                                      Text(
                                                                        storeFarm[i]['desc'],
                                                                        style: TextStyle(
                                                                          color: Colors.green[800],
                                                                        ),
                                                                      ),

                                                                    ],
                                                                  ),
                                                                  const SizedBox(
                                                                    width: 100,
                                                                  ),
                                                                  Column(
                                                                    crossAxisAlignment: CrossAxisAlignment.start,
                                                                    mainAxisAlignment: MainAxisAlignment.end,
                                                                    children: [
                                                                      IconButton(
                                                                          onPressed: () {
                                                                            showDialog(context: context, builder: (BuildContext context){
                                                                              return Container(
                                                                                height: 100,
                                                                              );
                                                                            });
                                                                          },
                                                                          icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                                                    ],
                                                                  )
                                                                ],
                                                              ),
                                                            )
                                                        ),
                                                      )



                                                  ),
                                                ),
                                              )
                                            ]
                                          ],
                                        ),
                                      );
                                    },
                                  ))
                          )),
                    ],
                  ))
                ],
              ),
            ),
          ),
          ]),
        ));
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getCollections(){
    DocumentReference<Map<String, dynamic>> parentDoc = FirebaseFirestore.instance.collection('farm').doc('activities');

    CollectionReference<Map<String, dynamic>> parentCol = parentDoc.collection('farm-activity');

    // Add where clause to filter documents where 'activity' field is 'Weeding'
    Query<Map<String, dynamic>> query = parentCol.where('activity', isEqualTo: 'Plough');

    // Return the query snapshots
    return query.snapshots();
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef.where('activity', isEqualTo: 'Till');

    // Return the query snapshots
    return query.snapshots();
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getPlantingStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef.where('activity', isEqualTo: 'Planting');

    // Return the query snapshots
    return query.snapshots();
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getWeedingStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef.where('activity', isEqualTo: 'Weeding');

    // Return the query snapshots
    return query.snapshots();
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getTopDressStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef.where('activity', isEqualTo: 'Top-Dress');

    // Return the query snapshots
    return query.snapshots();
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getHarvestStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef.where('activity', isEqualTo: 'Harvest');

    // Return the query snapshots
    return query.snapshots();
  }
}
