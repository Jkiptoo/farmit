import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/farm/data/add_farm.dart';
import 'package:meek/record/records.dart';
import '../data/edit_farm.dart';
import '../data/farm_activities.dart';
import '../income/farm_income.dart';

class ViewFarm extends StatefulWidget {
  final String id;
  const ViewFarm({Key? key, required this.id}) : super(key: key);

  @override
  State<ViewFarm> createState() => _ViewFarmState();
}

class _ViewFarmState extends State<ViewFarm> {
  bool isFarmLeased = false;
  final List storeFarm = [];

  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('View Farm'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('farm').
                    doc('farms').collection('farm-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Center(
                        child: Text('Something Went Wrong'),
                      );
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }
                    var data = snapshot.data!.data();
                    var name = data?['name'] ?? '';
                    var loc = data?['loc'] ?? '';
                    var date = data?['date'] ?? '';
                    var desc = data?['desc'] ?? '';
                    var size = data?['size'] ?? '';
                    var period = data?['period'] ?? '';
                    var idnum = data?['idnum'] ?? '';
                    var phone = data?['phone'] ?? '';
                    var ammount = data?['ammount'] ?? '';
                    var ownership = data?['ownership'] ?? '';


                    return SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Container(
                        padding: const EdgeInsets.all(0),
                        child: Column(
                          children: [
                              SizedBox(
                                height: 200,
                                width: double.infinity,
                                child: Card(
                                        child: InkWell(
                                          onTap: (){
                                            Navigator.push(context, MaterialPageRoute(builder: (context)=>  FarmActivities(id: widget.id,)));
                                          },
                                          child: SingleChildScrollView(
                                            scrollDirection: Axis.horizontal,
                                            child: Row(
                                              children: [
                                                const SizedBox(
                                                  width: 20,
                                                ),
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween                                                                      ,
                                                  children: [
                                                    Text(
                                                      "Farm:",
                                                      style: TextStyle(color: Colors.grey[800]),
                                                    ),
                                                    Text(
                                                      "Size:",
                                                      style: TextStyle(color: Colors.grey[800]),
                                                    ),
                                                    Text(
                                                      "Location:",
                                                      style: TextStyle(color: Colors.grey[800]),
                                                    ),
                                                    Text(
                                                      "Date:",
                                                      style: TextStyle(color: Colors.grey[800]),
                                                    ),
                                                    Text(
                                                      "Description:",
                                                      style: TextStyle(color: Colors.grey[800]),
                                                    ),
                                                    if(ownership == 'Lease')
                                                      Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [
                                                          Text(
                                                            "ID No:",
                                                            style: TextStyle(color: Colors.grey[800]),
                                                          ),
                                                          Text(
                                                            "Phone:",
                                                            style: TextStyle(color: Colors.grey[800]),
                                                          ),
                                                          Text(
                                                            "Ammount:",
                                                            style: TextStyle(color: Colors.grey[800]),
                                                          ),
                                                          Text(
                                                            "Period:",
                                                            style: TextStyle(color: Colors.grey[800]),
                                                          ),
                                                        ],
                                                      )
                                                  ],
                                                ),
                                                const SizedBox(
                                                  width: 35,
                                                ),
                                                Column(
                                                  crossAxisAlignment: CrossAxisAlignment.start,
                                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                  children: [
                                                    Text(
                                                      name,
                                                      style: TextStyle(
                                                        color: Colors.green[800],
                                                      ),
                                                    ),
                                                    Text(size,style: TextStyle(
                                                        color: Colors.green[800]
                                                    )),
                                                    Text(
                                                      loc,
                                                      style: TextStyle(
                                                        color: Colors.green[800],
                                                      ),
                                                    ),
                                                    Text(date,style: TextStyle(
                                                        color: Colors.green[800]
                                                    )),
                                                    Text(
                                                      desc,
                                                      style: TextStyle(
                                                        color: Colors.green[800],
                                                      ),
                                                    ),
                                                    if(ownership == 'Lease')
                                                       Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                        children: [

                                                          Text(idnum,style: TextStyle(
                                                              color: Colors.green[800]
                                                          )),
                                                          Text(
                                                            phone,
                                                            style: TextStyle(
                                                              color: Colors.green[800],
                                                            ),
                                                          ),
                                                          Text(ammount,style: TextStyle(
                                                              color: Colors.green[800]
                                                          )),
                                                          Text(
                                                            period,
                                                            style: TextStyle(
                                                              color: Colors.green[800],
                                                            ),
                                                          ),
                                                        ],
                                                      ) ,

                                                  ],
                                                ),
                                                const SizedBox(
                                                  width: 100,
                                                ),
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment.end,
                                                  children: [
                                                    IconButton(onPressed: (){
                                                      showDialog(
                                                          context: context,
                                                          builder: (context) {
                                                            return AlertDialog(
                                                              title:
                                                              const Text('More Options',textAlign: TextAlign.center,),
                                                              content: SizedBox(
                                                                height: 180,
                                                                width: 100,
                                                                child: Column(
                                                                  crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                                  mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceBetween,
                                                                  children: [
                                                                    InkWell(
                                                                      onTap: () {
                                                                        Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmIncome(id: widget.id,)));
                                                                      },
                                                                      child: const Text(
                                                                        'Add Income',
                                                                        style: TextStyle(
                                                                          fontSize: 16,
                                                                          fontWeight: FontWeight.bold,
                                                                          color: Colors.grey, // Apply your desired color
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    const Divider(
                                                                      color: Colors.grey, // Apply your desired color
                                                                    ),
                                                                    Material(
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmActivities(id: widget.id,)));
                                                                        },
                                                                        child: const Text(
                                                                            'Add Activity',
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            fontWeight: FontWeight.bold,
                                                                            color: Colors.grey, // Apply your desired color
                                                                          ),),
                                                                      ),
                                                                    ),
                                                                    const Divider(
                                                                      color: Colors.grey,
                                                                      thickness: 0.5,
                                                                    ),
                                                                    Material(
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>EditFarm(id: widget.id)));
                                                                        },
                                                                        child: const Text(
                                                                            'Edit Farm',
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            fontWeight: FontWeight.bold,
                                                                            color: Colors.grey, // Apply your desired color
                                                                          ),),
                                                                      ),
                                                                    ),
                                                                    const Divider(
                                                                      color: Colors.grey,
                                                                    ),
                                                                    Material(
                                                                      child: InkWell(
                                                                        onTap: () {
                                                                          Navigator.push(context, MaterialPageRoute(builder: (context)=>FarmNote(id: widget.id)));
                                                                        },
                                                                        child: const Text(
                                                                            'Add Note',
                                                                          style: TextStyle(
                                                                            fontSize: 16,
                                                                            fontWeight: FontWeight.bold,
                                                                            color: Colors.grey, // Apply your desired color
                                                                          ),),
                                                                      ),
                                                                    ),
                                                                    const Divider(
                                                                      color: Colors.grey,
                                                                    ),
                                                                    // Material(
                                                                    //   child: InkWell(
                                                                    //     onTap: () {
                                                                    //       showDialog(
                                                                    //           context:
                                                                    //           context,
                                                                    //           builder:
                                                                    //               (context) {
                                                                    //             return AlertDialog(
                                                                    //               title: const Text(
                                                                    //                   'Delete Farm'),
                                                                    //               content:
                                                                    //               SizedBox(
                                                                    //                 height:
                                                                    //                 125,
                                                                    //                 child:
                                                                    //                 Column(
                                                                    //                   children: [
                                                                    //                     const Text(
                                                                    //                         'All records associated with this farm will be deleted such as plantings, harvests and treatments. Are you sure?'),
                                                                    //                     Row(
                                                                    //                       mainAxisAlignment:
                                                                    //                       MainAxisAlignment.end,
                                                                    //                       children: [
                                                                    //                         ElevatedButton(
                                                                    //                             style: ElevatedButton.styleFrom(backgroundColor: Colors.orange),
                                                                    //                             onPressed: () {
                                                                    //                               Navigator.of(context).pop();
                                                                    //                             },
                                                                    //                             child: const Text('CANCEL')),
                                                                    //                         const SizedBox(
                                                                    //                           width: 10,
                                                                    //                         ),
                                                                    //                         ElevatedButton(
                                                                    //                           style: ElevatedButton.styleFrom(backgroundColor: Colors.red[800]),
                                                                    //                           onPressed: () async {
                                                                    //                             await FirebaseFirestore.instance
                                                                    //                                 .collection('farm')
                                                                    //                                 .doc('farms')
                                                                    //                                 .collection('farm-data')
                                                                    //                                 .doc(widget.id)
                                                                    //                                 .delete();
                                                                    //
                                                                    //                             await FirebaseFirestore.instance
                                                                    //                                 .collection('farm')
                                                                    //                                 .doc('activities')
                                                                    //                                 .collection('farm-income')
                                                                    //                                 .doc()
                                                                    //                                 .delete();
                                                                    //
                                                                    //                             // ignore: use_build_context_synchronously
                                                                    //                             Navigator.push(context, MaterialPageRoute(builder: (context) => const FarmList()));
                                                                    //                           },
                                                                    //                           child: const Text('DELETE'),
                                                                    //                         )
                                                                    //
                                                                    //                       ],
                                                                    //                     )
                                                                    //                   ],
                                                                    //                 ),
                                                                    //               ),
                                                                    //             );
                                                                    //           });
                                                                    //     },
                                                                    //     child: const Text(
                                                                    //         'Delete',
                                                                    //       style: TextStyle(
                                                                    //         fontSize: 16,
                                                                    //         fontWeight: FontWeight.bold,
                                                                    //         color: Colors.grey, // Apply your desired color
                                                                    //       ),),
                                                                    //   ),
                                                                    // ),
                                                                  ],
                                                                ),
                                                              ),
                                                            );
                                                          });
                                                    }, icon: const Icon(Icons.more_vert))
                                                  ],
                                                )
                                              ],
                                            ),
                                          ),
                                        )
                                    ),
                                  ),

                          ],
                        ),
                      ),
                    );

                  },
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        selectedLabelStyle: const TextStyle(
          fontSize: 20,
        ),
        selectedItemColor: Colors.lightGreen[900],
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Material(
              child: InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmIncome(id: widget.id,)));
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children:const [
                      Image(height: 45, image: AssetImage('assets/icons/homes.png'),),
                      Text('Income')
                    ],
                  )
              ),
            ),
            label: '',
          ),
          BottomNavigationBarItem(
            icon: Material(
              child: InkWell(
                  onTap: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context)=> FarmActivities(id: widget.id,)));
                  },
                  child: Column(
                    children: const[
                      Image(height: 45, image: AssetImage('assets/icons/sprout.png'),),
                      Text('Expense')
                    ],
                  )
              ),
            ),
            label: '',
          ),
        ],
      ),

    );
  }
  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('farm').doc('farms');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('farm-data');

    Query<Map<String, dynamic>> query =
    subcollectionRef.where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the stream of documents in the subcollection
    return query.snapshots();
  }

  // Initialize Firebase
  void initializeFirebase() {
    FirebaseFirestore.instance;
  }

// Delete data from multiple collections
  void deleteDataFromCollections(String documentId) async {
    // Get a reference to the first collection
    CollectionReference collection1Ref = FirebaseFirestore.instance.collection('farm')
        .doc('farms')
        .collection('farm-data');

    // Get a reference to the second collection
    CollectionReference collection2Ref = FirebaseFirestore.instance.collection('collection2');

    // Delete documents in the first collection
    await collection1Ref.doc('document1').delete();
    await collection1Ref.doc('document2').delete();

    // Delete documents in the second collection
    await collection2Ref.doc('document3').delete();
    await collection2Ref.doc('document4').delete();
  }


}
