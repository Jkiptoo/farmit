import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ActivitiesSummary extends StatefulWidget {
  final String id;
  const ActivitiesSummary({Key? key, required this.id}) : super(key: key);

  @override
  State<ActivitiesSummary> createState() => _ActivitiesSummaryState();
}

class _ActivitiesSummaryState extends State<ActivitiesSummary> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 1,
        child: Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.lightGreen[900],
            title: const Text(
              'Transactions',
              style: TextStyle(fontStyle: FontStyle.normal, fontSize: 24),
            ),
            centerTitle: true,
          ),
          body: TabBarView(children: [
            DefaultTabController(
              length: 6,
              child: SizedBox(
                height: 210,
                child: Column(
                  children: [
                    const SizedBox(
                      height: 20,
                    ),
                    TabBar(
                      tabs: const [
                        Tab(
                          text: 'Plough',
                          icon: Icon(Icons.grass),
                        ),
                        Tab(
                          text: 'Till',
                          icon: Icon(Icons.build),
                        ),
                        Tab(
                          text: 'Planting',
                          icon: Icon(Icons.pets),
                        ),
                        Tab(
                          text: 'Weeding',
                          icon: Icon(Icons.build),
                        ),
                        Tab(
                          text: 'Top-Dress',
                          icon: Icon(Icons.grass),
                        ),
                        Tab(
                          text: 'Harvest',
                          icon: Icon(Icons.pets),
                        ),
                      ],
                      labelColor: Colors.white, // Color of the selected tab's label
                      unselectedLabelColor: Colors.grey, // Color of unselected tab's label
                      indicator: BoxDecoration(
                        borderRadius: BorderRadius.circular(8), // Rounded corner indicator
                        color: Colors.lightGreen[900], // Color of the indicator
                      ),
                      indicatorSize: TabBarIndicatorSize.tab, // Match the tab's size
                      indicatorPadding: const EdgeInsets.symmetric(horizontal: 2),
                      labelPadding: const EdgeInsets.symmetric(horizontal: 4), // Adjust horizontal padding for the tab labels
                      labelStyle: const TextStyle(fontSize: 12), // Adjust the font size of the tab labels
                    ),
                    Expanded(
                        child: TabBarView(
                      children: [
                        Padding(
                            padding: const EdgeInsets.all(5),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                padding: const EdgeInsets.all(10),
                                child: StreamBuilder<QuerySnapshot>(
                                  stream: getCollections(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    final List storeFarm = [];
                                    snapshot.data?.docs.map(
                                        (DocumentSnapshot documentSnapshot) {
                                      Map a = documentSnapshot.data()
                                          as Map<String, dynamic>;
                                      storeFarm.add(a);
                                      a['id'] = documentSnapshot.id;
                                    }).toList();
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          for (var i = 0;
                                              i < storeFarm.length;
                                              i++) ...[
                                            SizedBox(
                                              height: 110,
                                              width: double.infinity,
                                              child: Material(
                                                child: Card(
                                                      child:
                                                          SingleChildScrollView(
                                                    scrollDirection:
                                                        Axis.horizontal,
                                                    child: Row(
                                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                                      children: [
                                                        const SizedBox(
                                                          width: 20,
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text(
                                                              "Farm:",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .grey[
                                                                      800]),
                                                            ),
                                                            Text(
                                                              "Size:",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .grey[
                                                                      800]),
                                                            ),
                                                            Text(
                                                              "Expense:",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .grey[
                                                                      800]),
                                                            ),
                                                            Text(
                                                              "Date:",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .grey[
                                                                      800]),
                                                            ),
                                                            Text(
                                                              "Description:",
                                                              style: TextStyle(
                                                                  color: Colors
                                                                          .grey[
                                                                      800]),
                                                            ),
                                                          ],
                                                        ),
                                                        const SizedBox(
                                                          width: 35,
                                                        ),
                                                        Column(
                                                          crossAxisAlignment:
                                                              CrossAxisAlignment
                                                                  .start,
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceEvenly,
                                                          children: [
                                                            Text(
                                                              storeFarm[i]
                                                                  ['farm'],
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .green[800],
                                                              ),
                                                            ),
                                                            Text(
                                                                storeFarm[i]
                                                                    ['size'],
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800])),
                                                            Text(
                                                              storeFarm[i]
                                                                  ['expense'],
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .green[800],
                                                              ),
                                                            ),
                                                            Text(
                                                                storeFarm[i]
                                                                    ['date'],
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800])),
                                                            Text(
                                                              storeFarm[i]
                                                                  ['desc'],
                                                              style: TextStyle(
                                                                color: Colors
                                                                    .green[800],
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        ],
                                                    ),
                                                  )),
                                                ),
                                              ),
                                          ]
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(20),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: StreamBuilder<QuerySnapshot>(
                                    stream: getOwnedDocumentsStream(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        const Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      final List storeFarm = [];
                                      snapshot.data?.docs.map(
                                          (DocumentSnapshot documentSnapshot) {
                                        Map a = documentSnapshot.data()
                                            as Map<String, dynamic>;
                                        storeFarm.add(a);
                                        a['id'] = documentSnapshot.id;
                                      }).toList();
                                      return SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          children: [
                                            for (var i = 0;
                                                i < storeFarm.length;
                                                i++) ...[
                                              SizedBox(
                                                height: 110,
                                                width: double.infinity,
                                                child: Material(
                                                  child: Card(
                                                        child:
                                                            SingleChildScrollView(
                                                      scrollDirection:
                                                          Axis.horizontal,
                                                      child: Row(
                                                        children: [
                                                          const SizedBox(
                                                            width: 20,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              Text(
                                                                "Farm:",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                              Text(
                                                                "Size:",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                              Text(
                                                                "Expense:",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                              Text(
                                                                "Date:",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                              Text(
                                                                "Description:",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                            .grey[
                                                                        800]),
                                                              ),
                                                            ],
                                                          ),
                                                          const SizedBox(
                                                            width: 35,
                                                          ),
                                                          Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceEvenly,
                                                            children: [
                                                              Text(
                                                                storeFarm[i]
                                                                    ['farm'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .green[
                                                                      800],
                                                                ),
                                                              ),
                                                              Text(
                                                                  storeFarm[i]
                                                                      ['size'],
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800])),
                                                              Text(
                                                                storeFarm[i]
                                                                    ['expense'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .green[
                                                                      800],
                                                                ),
                                                              ),
                                                              Text(
                                                                  storeFarm[i]
                                                                      ['date'],
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800])),
                                                              Text(
                                                                storeFarm[i]
                                                                    ['desc'],
                                                                style:
                                                                    TextStyle(
                                                                  color: Colors
                                                                          .green[
                                                                      800],
                                                                ),
                                                              ),
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    )),
                                                  ),
                                                ),

                                            ]
                                          ],
                                        ),
                                      );
                                    },
                                  )),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(7),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: StreamBuilder<QuerySnapshot>(
                                    stream: getPlantingStream(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        const Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      final List storeFarm = [];
                                      snapshot.data?.docs.map(
                                          (DocumentSnapshot documentSnapshot) {
                                        Map a = documentSnapshot.data()
                                            as Map<String, dynamic>;
                                        storeFarm.add(a);
                                        a['id'] = documentSnapshot.id;
                                      }).toList();
                                      return SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          children: [
                                            for (var i = 0;
                                                i < storeFarm.length;
                                                i++) ...[
                                              SizedBox(
                                                width: double.infinity,
                                                child: Material(
                                                  child: IntrinsicHeight(
                                                        child: Card(
                                                            child:
                                                                SingleChildScrollView(
                                                          scrollDirection:
                                                              Axis.horizontal,
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 20,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Text(
                                                                    "Farm:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Size:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Expense:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Date:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Fertilizer:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Quantity:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Description:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                ],
                                                              ),
                                                              const SizedBox(
                                                                width: 35,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'farm'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'size'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'expense'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'date'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'fertilizer'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'quantity'],
                                                                        style:
                                                                            TextStyle(
                                                                          color:
                                                                              Colors.green[800],
                                                                        ),
                                                                      ),
                                                                      Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'unit'],
                                                                        style:
                                                                            TextStyle(
                                                                          color:
                                                                              Colors.green[800],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'desc'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                            ],
                                                          ),
                                                        )),
                                                      )),
                                                ),
                                            ]
                                          ],
                                        ),
                                      );
                                    },
                                  )),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(5),
                            child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: StreamBuilder<QuerySnapshot>(
                                  stream: getWeedingStream(),
                                  builder: (BuildContext context,
                                      AsyncSnapshot<QuerySnapshot> snapshot) {
                                    if (snapshot.connectionState ==
                                        ConnectionState.waiting) {
                                      const Center(
                                        child: CircularProgressIndicator(),
                                      );
                                    }
                                    final List storeFarm = [];
                                    snapshot.data?.docs.map(
                                        (DocumentSnapshot documentSnapshot) {
                                      Map a = documentSnapshot.data()
                                          as Map<String, dynamic>;
                                      storeFarm.add(a);
                                      a['id'] = documentSnapshot.id;
                                    }).toList();
                                    return SingleChildScrollView(
                                      scrollDirection: Axis.vertical,
                                      child: Column(
                                        children: [
                                          for (var i = 0;
                                              i < storeFarm.length;
                                              i++) ...[
                                            SizedBox(
                                              width: double.infinity,
                                              child: Material(
                                                child: IntrinsicHeight(
                                                      child: Card(
                                                          child:
                                                              SingleChildScrollView(
                                                        scrollDirection:
                                                            Axis.horizontal,
                                                        child: Row(
                                                          children: [
                                                            const SizedBox(
                                                              width: 20,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceEvenly,
                                                              children: [
                                                                Text(
                                                                  "Farm:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                                Text(
                                                                  "Size:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                                Text(
                                                                  "Crop:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                                Text(
                                                                  "Expense:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                                Text(
                                                                  "Date:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                                if (storeFarm[i]
                                                                        [
                                                                        'Spraying'] ==
                                                                    true)
                                                                  Column(
                                                                    children: [
                                                                      Text(
                                                                        "Herbicide:",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.grey[800]),
                                                                      ),
                                                                      Text(
                                                                        "Quantity:",
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.grey[800]),
                                                                      ),
                                                                    ],
                                                                  ),
                                                                Text(
                                                                  "Description:",
                                                                  style: TextStyle(
                                                                      color: Colors
                                                                              .grey[
                                                                          800]),
                                                                ),
                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              width: 35,
                                                            ),
                                                            Column(
                                                              crossAxisAlignment:
                                                                  CrossAxisAlignment
                                                                      .start,
                                                              mainAxisAlignment:
                                                                  MainAxisAlignment
                                                                      .spaceEvenly,
                                                              children: [
                                                                Text(
                                                                  storeFarm[i]
                                                                      ['farm'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                  storeFarm[i]
                                                                      ['size'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                  storeFarm[i]
                                                                      ['crop'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                  storeFarm[i][
                                                                      'expense'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                                Text(
                                                                  storeFarm[i]
                                                                      ['date'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                                if (storeFarm[i]
                                                                        [
                                                                        'Spraying'] ==
                                                                    true)
                                                                  Column(
                                                                    children: [
                                                                      Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'herb'],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.grey[800]),
                                                                      ),
                                                                      Row(
                                                                        children: [
                                                                          Text(
                                                                            storeFarm[i]['quantity'],
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.green[800],
                                                                            ),
                                                                          ),
                                                                          const SizedBox(
                                                                            width:
                                                                                5,
                                                                          ),
                                                                          Text(
                                                                            storeFarm[i]['unit'],
                                                                            style:
                                                                                TextStyle(
                                                                              color: Colors.green[800],
                                                                            ),
                                                                          ),
                                                                        ],
                                                                      ),
                                                                    ],
                                                                  ),
                                                                Text(
                                                                  storeFarm[i]
                                                                      ['desc'],
                                                                  style:
                                                                      TextStyle(
                                                                    color: Colors
                                                                            .green[
                                                                        800],
                                                                  ),
                                                                ),
                                                              ],
                                                            ),
                                                            const SizedBox(
                                                              width: 100,
                                                            ),
                                                                                                                      ],
                                                        ),
                                                      )),
                                                    )),
                                              ),

                                          ]
                                        ],
                                      ),
                                    );
                                  },
                                ))),
                        Padding(
                            padding: const EdgeInsets.all(20),
                            child: SingleChildScrollView(
                              scrollDirection: Axis.vertical,
                              child: Padding(
                                  padding: const EdgeInsets.all(10),
                                  child: StreamBuilder<QuerySnapshot>(
                                    stream: getTopDressStream(),
                                    builder: (BuildContext context,
                                        AsyncSnapshot<QuerySnapshot> snapshot) {
                                      if (snapshot.connectionState ==
                                          ConnectionState.waiting) {
                                        const Center(
                                          child: CircularProgressIndicator(),
                                        );
                                      }
                                      final List storeFarm = [];
                                      snapshot.data?.docs.map(
                                          (DocumentSnapshot documentSnapshot) {
                                        Map a = documentSnapshot.data()
                                            as Map<String, dynamic>;
                                        storeFarm.add(a);
                                        a['id'] = documentSnapshot.id;
                                      }).toList();
                                      return SingleChildScrollView(
                                        scrollDirection: Axis.vertical,
                                        child: Column(
                                          children: [
                                            for (var i = 0;
                                                i < storeFarm.length;
                                                i++) ...[
                                              SizedBox(
                                                width: double.infinity,
                                                child: Material(
                                                  child: IntrinsicHeight(
                                                        child: Card(
                                                            child:
                                                                SingleChildScrollView(
                                                          scrollDirection:
                                                              Axis.horizontal,
                                                          child: Row(
                                                            children: [
                                                              const SizedBox(
                                                                width: 20,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Text(
                                                                    "Farm:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Size:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Expense:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Date:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Fertilizer:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Quantity:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                  Text(
                                                                    "Description:",
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .grey[800]),
                                                                  ),
                                                                ],
                                                              ),
                                                              const SizedBox(
                                                                width: 35,
                                                              ),
                                                              Column(
                                                                crossAxisAlignment:
                                                                    CrossAxisAlignment
                                                                        .start,
                                                                mainAxisAlignment:
                                                                    MainAxisAlignment
                                                                        .spaceEvenly,
                                                                children: [
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'farm'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'size'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'expense'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'date'],
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.green[800])),
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'fertilizer'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                  Row(
                                                                    children: [
                                                                      Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'quantity'],
                                                                        style:
                                                                            TextStyle(
                                                                          color:
                                                                              Colors.green[800],
                                                                        ),
                                                                      ),
                                                                      const SizedBox(
                                                                        width:
                                                                            5,
                                                                      ),
                                                                      Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'unit'],
                                                                        style:
                                                                            TextStyle(
                                                                          color:
                                                                              Colors.green[800],
                                                                        ),
                                                                      )
                                                                    ],
                                                                  ),
                                                                  Text(
                                                                    storeFarm[i]
                                                                        [
                                                                        'desc'],
                                                                    style:
                                                                        TextStyle(
                                                                      color: Colors
                                                                              .green[
                                                                          800],
                                                                    ),
                                                                  ),
                                                                ],
                                                              ),
                                                                                                                          ],
                                                          ),
                                                        )),
                                                      )),
                                                ),

                                            ]
                                          ],
                                        ),
                                      );
                                    },
                                  )),
                            )),
                        Padding(
                            padding: const EdgeInsets.all(20),
                            child: SingleChildScrollView(
                                scrollDirection: Axis.vertical,
                                child: Padding(
                                    padding: const EdgeInsets.all(10),
                                    child: StreamBuilder<QuerySnapshot>(
                                      stream: getHarvestStream(),
                                      builder: (BuildContext context,
                                          AsyncSnapshot<QuerySnapshot>
                                              snapshot) {
                                        if (snapshot.connectionState ==
                                            ConnectionState.waiting) {
                                          const Center(
                                            child: CircularProgressIndicator(),
                                          );
                                        }
                                        final List storeFarm = [];
                                        snapshot.data?.docs.map(
                                            (DocumentSnapshot
                                                documentSnapshot) {
                                          Map a = documentSnapshot.data()
                                              as Map<String, dynamic>;
                                          storeFarm.add(a);
                                          a['id'] = documentSnapshot.id;
                                        }).toList();
                                        return SingleChildScrollView(
                                          scrollDirection: Axis.vertical,
                                          child: Column(
                                            children: [
                                              for (var i = 0;
                                                  i < storeFarm.length;
                                                  i++) ...[
                                                SizedBox(
                                                  width: double.infinity,
                                                  child: Material(
                                                    child: IntrinsicHeight(
                                                          child: Card(
                                                              child:
                                                                  SingleChildScrollView(
                                                            scrollDirection:
                                                                Axis.horizontal,
                                                            child: Row(
                                                              children: [
                                                                const SizedBox(
                                                                  width: 20,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: [
                                                                    Text(
                                                                      "Farm:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Size:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Expense:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Date:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Quantity:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                    Text(
                                                                      "Description:",
                                                                      style: TextStyle(
                                                                          color:
                                                                              Colors.grey[800]),
                                                                    ),
                                                                  ],
                                                                ),
                                                                const SizedBox(
                                                                  width: 35,
                                                                ),
                                                                Column(
                                                                  crossAxisAlignment:
                                                                      CrossAxisAlignment
                                                                          .start,
                                                                  mainAxisAlignment:
                                                                      MainAxisAlignment
                                                                          .spaceEvenly,
                                                                  children: [
                                                                    Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'farm'],
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .green[800],
                                                                      ),
                                                                    ),
                                                                    Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'size'],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.green[800])),
                                                                    Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'expense'],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.green[800])),
                                                                    Text(
                                                                        storeFarm[i]
                                                                            [
                                                                            'date'],
                                                                        style: TextStyle(
                                                                            color:
                                                                                Colors.green[800])),
                                                                    Row(
                                                                      children: [
                                                                        Text(
                                                                          storeFarm[i]
                                                                              [
                                                                              'quantity'],
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Colors.green[800],
                                                                          ),
                                                                        ),
                                                                        Text(
                                                                          storeFarm[i]
                                                                              [
                                                                              'unit'],
                                                                          style:
                                                                              TextStyle(
                                                                            color:
                                                                                Colors.green[800],
                                                                          ),
                                                                        )
                                                                      ],
                                                                    ),
                                                                    Text(
                                                                      storeFarm[
                                                                              i]
                                                                          [
                                                                          'desc'],
                                                                      style:
                                                                          TextStyle(
                                                                        color: Colors
                                                                            .green[800],
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),

                                                              ],
                                                            ),
                                                          )),
                                                        )),
                                                  ),
                                              ]
                                            ],
                                          ),
                                        );
                                      },
                                    )))),
                      ],
                    ))
                  ],
                ),
              ),
            ),
          ]),
        ));
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getCollections() {
    DocumentReference<Map<String, dynamic>> parentDoc =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    CollectionReference<Map<String, dynamic>> parentCol =
        parentDoc.collection('farm-activity');

    // Add where clause to filter documents where 'activity' field is 'Weeding'
    Query<Map<String, dynamic>> query =
        parentCol.where('activity', isEqualTo: 'Plough')
            .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
        parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query =
        subcollectionRef.where('activity', isEqualTo: 'Till')
            .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getPlantingStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
        parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query =
        subcollectionRef.where('activity', isEqualTo: 'Planting')
            .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getWeedingStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
        parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query =
        subcollectionRef.where('activity', isEqualTo: 'Weeding')
            .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getTopDressStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
        parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query =
        subcollectionRef.where('activity', isEqualTo: 'Top-Dress')
            .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getHarvestStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
        FirebaseFirestore.instance.collection('farm').doc('activities');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
        parentDocRef.collection('farm-activity');

    // Return the stream of documents in the subcollection
    Query<Map<String, dynamic>> query = subcollectionRef
        .where('activity', isEqualTo: 'Harvest')
        .where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    // Return the query snapshots
    return query.snapshots();
  }
  // Delete data from multiple collections
  void deleteDataFromCollections() async {
    // Get a reference to the first collection
    CollectionReference collection1Ref = FirebaseFirestore.instance.collection('farm')
        .doc('activities')
        .collection('farm-activity');

    // Get a reference to the second collection
    CollectionReference collection2Ref = FirebaseFirestore.instance.collection('collection2');

    // Delete documents in the first collection
    await collection1Ref.doc(widget.id).delete();

    // Delete documents in the second collection
    await collection2Ref.doc('document3').delete();
    await collection2Ref.doc('document4').delete();
  }
}
