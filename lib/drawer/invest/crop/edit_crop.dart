import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/crop/add_crop.dart';

class EditCrop extends StatefulWidget {
  final String id;
  const  EditCrop({Key? key, required this.id}) : super(key: key);

  @override
  State<EditCrop> createState() => _EditCropState();
}

class _EditCropState extends State<EditCrop> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference<Map<String, dynamic>> update = FirebaseFirestore.instance.collection('crop').doc('crops').collection('crop-data');

  Future<void> machineUpdate(id, crop) async {
    update.doc(id).update({
      'crop': crop,

    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Crop Successfully Updated'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Update Crop'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('crop')
                      .doc('crops').collection('crop-data').doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    var crop = data!['crop'];

                    return Column(
                      children: [
                        TextFormField(
                          onChanged: (val) {
                            crop = val;
                          },
                          initialValue: crop,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Crop';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Crop', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    backgroundColor: Colors.lightGreen[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(10))),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    machineUpdate(widget.id, crop,
                                    )
                                        .then((value) => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const AddCrop())));
                                  }
                                },
                                child: const Text('Update'))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
  //Creating a subcollection for storing crop data
  Stream<QuerySnapshot<Map<String, dynamic>>> getCropSubCollection(){
    // setting up the crops documents in the crop collection
    DocumentReference<Map<String, dynamic>> parentDoc = FirebaseFirestore.instance.collection('crop').doc('crops');

    CollectionReference<Map<String, dynamic>> colReference = parentDoc.collection('crop-data');

    return colReference.snapshots();


  }
}
