import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/crop/edit_crop.dart';

class AddCrop extends StatefulWidget {
  const AddCrop({Key? key}) : super(key: key);

  @override
  State<AddCrop> createState() => _AddCropState();
}

class _AddCropState extends State<AddCrop> {
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDatePicker() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    }
    _dateController.text = _dateFormatter.format(date!);
  }

  final _formKey = GlobalKey<FormState>();

  var crop = '';
  var dates = '';
  var rid = '';
  final _cropController = TextEditingController();
  final _dateController = TextEditingController();
  clearText() {
    _cropController.clear();
    _dateController.clear();
  }

  CollectionReference<Map<String, dynamic>> collectionReference =
      FirebaseFirestore.instance
          .collection('crop')
          .doc('crops')
          .collection('crop-data');
  Future<void> cropsAdded() async {
    return await collectionReference
        .doc()
        .set({
          'rid': firebaseUser!.uid,
          'crop': crop,
          'dates': dates, // ignore: avoid_print
        })
        // ignore: avoid_print
        .then((value) => print("Crop Added Successfully"))
        .catchError((error) {
          // ignore: avoid_print
          print('Something went wrong: $error');
        });
  }

  Future<void> deleteCrop(id) async {
    return collectionReference.doc(id).delete().then((value) {
      // ignore: avoid_print
      print('Crop Deleted');
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Crop Successfully Deleted')));
    }).catchError((error) {
      // ignore: avoid_print
      print('Something went wrong: $error');
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('Something went Wrong')));
    });
  }

  Future<void> updateCrop(id, crop, dates) async {
    return collectionReference
        .doc(id)
        .update({
          'crop': crop,
          'dates': dates,
        }
            // ignore: avoid_print
            )
        // ignore: avoid_print
        .then((value) => print('Crop Updated Successfully'))
        .catchError((error) {
          // ignore: avoid_print
          print('Something went Wrong: $error');
        });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('ADD A CROP'),
        centerTitle: true,
        backgroundColor: Colors.lightGreen[900],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: getCropSubCollection(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeAddedCrop = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map b = documentSnapshot.data() as Map<String, dynamic>;
            storeAddedCrop.add(b);
            b['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (var i = 0; i < storeAddedCrop.length; i++) ...[
                    SizedBox(
                        height: 70,
                        child: Material(
                          child: InkWell(
                              child: Card(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        "Crop Name:",
                                        style:
                                            TextStyle(color: Colors.grey[800]),
                                      ),
                                      Text(
                                        "Date:",
                                        style:
                                            TextStyle(color: Colors.grey[800]),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 35,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        storeAddedCrop[i]['crop'],
                                        style: TextStyle(
                                          color: Colors.green[800],
                                        ),
                                      ),
                                      Text(storeAddedCrop[i]['dates'],
                                          style: TextStyle(
                                              color: Colors.green[800]))
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 100,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return AlertDialog(
                                                    title: const Text(
                                                      'More Options',
                                                      textAlign:
                                                          TextAlign.center,
                                                    ),
                                                    content: SizedBox(
                                                      height: 144,
                                                      width: 100,
                                                      child: Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceBetween,
                                                        children: [
                                                          const Divider(),
                                                          Material(
                                                            child: InkWell(
                                                              onTap: () {
                                                                Navigator.push(
                                                                    context,
                                                                    MaterialPageRoute(
                                                                        builder:
                                                                            (context) =>
                                                                                EditCrop(id: storeAddedCrop[i]['id'])));
                                                              },
                                                              child: const Text(
                                                                  'Edit Crop'),
                                                            ),
                                                          ),
                                                          const Divider(),
                                                          Material(
                                                            child: InkWell(
                                                              onTap: () {},
                                                              child: const Text(
                                                                  'Add Variety'),
                                                            ),
                                                          ),
                                                          const Divider(),
                                                          Material(
                                                            child: InkWell(
                                                              onTap: () {},
                                                              child: const Text(
                                                                  'Print Pdf'),
                                                            ),
                                                          ),
                                                          const Divider(),
                                                          Material(
                                                            child: InkWell(
                                                              onTap: () {
                                                                showDialog(
                                                                    context:
                                                                        context,
                                                                    builder:
                                                                        (context) {
                                                                      return AlertDialog(
                                                                        title: const Text(
                                                                            'Deleting crop!'),
                                                                        content:
                                                                            SizedBox(
                                                                          height:
                                                                              125,
                                                                          child:
                                                                              Column(
                                                                            children: [
                                                                              const Text('All records associated with this crop will be deleted such as plantings, harvests and treatments. Are you sure?'),
                                                                              Row(
                                                                                mainAxisAlignment: MainAxisAlignment.end,
                                                                                children: [
                                                                                  ElevatedButton(
                                                                                      style: ElevatedButton.styleFrom(backgroundColor: Colors.orange),
                                                                                      onPressed: () {
                                                                                        Navigator.of(context).pop();
                                                                                      },
                                                                                      child: const Text('CANCEL')),
                                                                                  const SizedBox(
                                                                                    width: 10,
                                                                                  ),
                                                                                  ElevatedButton(
                                                                                      style: ElevatedButton.styleFrom(backgroundColor: Colors.red[800]),
                                                                                      onPressed: () {
                                                                                        deleteCrop(storeAddedCrop[i]['id']).then((value) => Navigator.pop(context));
                                                                                      },
                                                                                      child: const Text('DELETE'))
                                                                                ],
                                                                              )
                                                                            ],
                                                                          ),
                                                                        ),
                                                                      );
                                                                    });
                                                              },
                                                              child: const Text(
                                                                  'Delete'),
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  );
                                                });
                                          },
                                          icon: const Icon(Icons.more_vert))
                                    ],
                                  )
                                ],
                              ),
                            ),
                          )),
                        )),
                  ]
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen[900],
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) => AlertDialog(
                    title: Text(
                      'ADD A CROP',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontStyle: FontStyle.italic,
                          fontSize: 22,
                          color: Colors.grey[800]),
                    ),
                    content: SizedBox(
                        width: 400,
                        height: 250,
                        child: SingleChildScrollView(
                          child: Form(
                            key: _formKey,
                            child: Column(
                              children: [
                                TextFormField(
                                  validator: (value) {
                                    if (value!.isEmpty ||
                                        !RegExp(r'^[A-Z a-z]+$')
                                            .hasMatch(value)) {
                                      return 'Enter a valid Crop Name.';
                                    }
                                    return null;
                                  },
                                  controller: _cropController,
                                  decoration: const InputDecoration(
                                      labelText: 'Crop Name',
                                      labelStyle: TextStyle(
                                          fontStyle: FontStyle.italic)),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                TextFormField(
                                  autofocus: false,
                                  validator: (value) {
                                    if (value == null || value.isEmpty) {
                                      return 'Enter correct Date';
                                    }
                                    return null;
                                  },
                                  controller: _dateController,
                                  readOnly: true,
                                  onTap: _handleDatePicker,
                                  decoration: const InputDecoration(
                                      labelText: 'Date Added'),
                                ),
                                const SizedBox(
                                  height: 20,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            fixedSize: const Size.fromWidth(90),
                                            backgroundColor:
                                                Colors.lightGreen[900]),
                                        onPressed: () {
                                          checkCropUniquenessAndPerformAction();
                                        },
                                        child: const Text('ADD')),
                                    const SizedBox(
                                      width: 30,
                                    ),
                                    ElevatedButton(
                                        style: ElevatedButton.styleFrom(
                                            fixedSize: const Size.fromWidth(90),
                                            backgroundColor:
                                                Colors.lightGreen[900]),
                                        onPressed: () {
                                          Navigator.pop(context);
                                        },
                                        child: const Text('CANCEL')),
                                  ],
                                )
                              ],
                            ),
                          ),
                        )),
                  ));
        },
        child: const Icon(
          Icons.add,
          size: 30,
        ),
      ),
    );
  }

  //Creating a subcollection for storing crop data
  Stream<QuerySnapshot<Map<String, dynamic>>> getCropSubCollection() {
    // setting up the crops documents in the crop collection
    DocumentReference<Map<String, dynamic>> parentDoc =
        FirebaseFirestore.instance.collection('crop').doc('crops');

    CollectionReference<Map<String, dynamic>> colReference =
        parentDoc.collection('crop-data');

    Query<Map<String, dynamic>> query = colReference.where('rid',
        isEqualTo: FirebaseAuth.instance.currentUser?.uid);

    return query.snapshots();
  }

  Future<void> checkCropUniquenessAndPerformAction() async {
    if (_formKey.currentState!.validate()) {
      String newCrop = _cropController.text.toLowerCase();

      bool isCropUnique = await checkCropUniqueness(newCrop);

      if (isCropUnique) {
        // Perform the action when the crop is unique
        setState(() {
          dates = _dateController.text;
          crop = newCrop;
          cropsAdded().then((value) => Navigator.pop(context));
          clearText();
        });
      } else {
        // Display an error message when the crop already exists
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: const Text('Error'),
              content: const Text(
                  'The crop already exists. Please enter a unique crop.'),
              actions: [
                TextButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: const Text('OK'),
                ),
              ],
            );
          },
        );
      }
    }
  }

  Future<bool> checkCropUniqueness(String crop) async {
    // Query the collection to check if a document with the same crop already exists
    QuerySnapshot<Map<String, dynamic>> querySnapshot = await FirebaseFirestore
        .instance
        .collection('crop').doc('crops').collection('crop-data')
        .where('crop', isEqualTo: crop)
        .limit(1)
        .get();

    // Return true if no matching document is found, indicating the crop is unique
    return querySnapshot.docs.isEmpty;
  }
}
