import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/Animal/animal.dart';

class ViewTreatment extends StatefulWidget {
  const ViewTreatment({Key? key}) : super(key: key);

  @override
  State<ViewTreatment> createState() => _ViewTreatmentState();
}

class _ViewTreatmentState extends State<ViewTreatment> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => const ManageAnimal()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Treatment Data'),
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('animal').doc('data').collection('treatment').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeCategory = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map c = documentSnapshot.data() as Map<String, dynamic>;
            storeCategory.add(c);
            c['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (var i = 0; i < storeCategory.length; i++) ...[
                    SizedBox(
                      height: 150,
                      width: double.infinity,
                      child: Material(
                        child: InkWell(
                          onTap: () {
                            //Navigator.push(context, MaterialPageRoute(builder: (context)=> const ManageAnimal()));
                          },
                          child: Card(
                              child: SingleChildScrollView(
                                scrollDirection: Axis.horizontal,
                                child: Row(
                                  children: [
                                    const SizedBox(
                                      width: 20,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                      children: [
                                        Text(
                                          "Type:",
                                          style: TextStyle(color: Colors.grey[800]),
                                        ),
                                        Text(
                                          "Date:",
                                          style: TextStyle(color: Colors.grey[800]),
                                        ),
                                        Text(
                                          "Cost:",
                                          style: TextStyle(color: Colors.grey[800]),
                                        ),
                                        Text(
                                          "Description:",
                                          style: TextStyle(color: Colors.grey[800]),
                                        ),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 35,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                      children: [
                                        Text(
                                          storeCategory[i]['type'],
                                          style: TextStyle(
                                            color: Colors.green[800],
                                          ),
                                        ),
                                        Text(
                                          storeCategory[i]['date'],
                                          style: TextStyle(
                                            color: Colors.green[800],
                                          ),
                                        ),
                                        Text(storeCategory[i]['cost'],style: TextStyle(
                                            color: Colors.green[800]
                                        )),
                                        Text(storeCategory[i]['desc'],style: TextStyle(
                                            color: Colors.green[800]
                                        )),
                                      ],
                                    ),
                                    const SizedBox(
                                      width: 100,
                                    ),
                                    Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment: MainAxisAlignment.end,
                                      children: [
                                        IconButton(
                                            onPressed: () {
                                              showDialog(context: context, builder: (BuildContext context){
                                                return Container(
                                                  height: 100,
                                                );
                                              });
                                            },
                                            icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                      ],
                                    )
                                  ],
                                ),
                              )
                          ),
                        ),
                      ),
                    )
                  ]
                ],
              ),
            ),
          );
        },
      ),
          );
  }
}
