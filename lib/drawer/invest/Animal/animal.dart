import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/Animal/animal_expense.dart';
import 'package:meek/drawer/invest/Animal/animal_income.dart';
import 'package:meek/drawer/invest/Animal/treatment.dart';

import 'edit_animal.dart';

class ManageAnimal extends StatefulWidget {
  const ManageAnimal({Key? key}) : super(key: key);

  @override
  State<ManageAnimal> createState() => _ManageAnimalState();
}

class _ManageAnimalState extends State<ManageAnimal> {
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final _formKey = GlobalKey<FormState>();
  final _datesController = TextEditingController();
  final _dateController = TextEditingController();
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDatePicker() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    }
    _dateController.text = _dateFormatter.format(date!);
    _datesController.text = _dateFormatter.format(date);
  }

  var type = '';
  var num = '';
  var date = '';
  final _animalType = TextEditingController();
  final _noOfAnimal = TextEditingController();
  final CollectionReference animal =
      FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');
  Future<void> addAnimal() async {
    return await animal.doc().set({
      'rid': firebaseUser?.uid,
      'type': type,
      'num': num,
      'date': date,
    }).then((value) => null);
  }

  CollectionReference delRef = FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');

  Future<void> deleteAnimal(id) async {
    return await delRef.doc(id).delete();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(onPressed: (){
          Navigator.of(context).pushNamedAndRemoveUntil('/home', (route) => false);
        }, icon: const Icon(Icons.arrow_back),),
        shadowColor: Colors.orange,
        backgroundColor: Colors.lightGreen[900],
        title: const Text(
          'Animal Data',
          style: TextStyle(fontSize: 17, fontWeight: FontWeight.w700),
        ),
        centerTitle: true,
        elevation: 0.0,
        actions: [
          Row(
            children: [
              IconButton(onPressed: () {}, icon: const Icon(Icons.search)),
              IconButton(onPressed: () {}, icon: const Icon(Icons.print)),
            ],
          )
        ],
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeAnimal = [];
          snapshot.data!.docs.map((DocumentSnapshot documentSnapshot) {
            Map d = documentSnapshot.data() as Map<String, dynamic>;
            storeAnimal.add(d);
            d['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Container(
                padding: const EdgeInsets.all(0.0),
                child: Column(
                  children: [
                    for (var i = 0; i < storeAnimal.length; i++) ...[
                      SizedBox(
                        height: 80,
                        width: double.infinity,
                        child: InkWell(
                          onTap: () {},
                          child: Card(
                            shadowColor: Colors.lightGreen,
                            elevation: 10,
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        'Animal:',
                                        style: TextStyle(
                                            color: Colors.grey[800],
                                            fontSize: 17),
                                      ),
                                      Text('Number:',
                                          style: TextStyle(
                                              color: Colors.grey[800],
                                              fontSize: 17)),
                                      Text('Date:',
                                          style: TextStyle(
                                              color: Colors.grey[800],
                                              fontSize: 17)),
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 50,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        storeAnimal[i]['type'],
                                        style: TextStyle(
                                            color: Colors.lightGreen[800],
                                            fontSize: 17),
                                      ),
                                      Text(
                                        storeAnimal[i]['num'],
                                        style: TextStyle(
                                            color: Colors.lightGreen[800],
                                            fontSize: 17),
                                      ),
                                      Text(
                                        storeAnimal[i]['date'],
                                        style: TextStyle(
                                            color: Colors.lightGreen[800],
                                            fontSize: 17),
                                      ),
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 100,
                                  ),
                                  Column(
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            showDialog(
                                                context: context,
                                                builder: (context) {
                                                  return Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.end,
                                                    children: [
                                                      AlertDialog(
                                                        content: IntrinsicHeight(
                                                          child: Column(
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .start,
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .spaceBetween,
                                                            children: [
                                                              const Text(
                                                                'Add Income', textAlign: TextAlign.center, style: TextStyle(
                                                                  fontSize: 20, fontWeight: FontWeight.w600
                                                              ),
                                                              ),
                                                              Material(
                                                                child: InkWell(
                                                                  onTap: () {
                                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> AnimalIncome(id: storeAnimal[i]['id'],))).then((value) {
                                                                      Navigator.of(context).pop();
                                                                    });
                                                                  },
                                                                  child: const Text(
                                                                      'Add Income'),
                                                                ),
                                                              ),
                                                              const Divider(),
                                                              Material(
                                                                child: InkWell(
                                                                  onTap: () {
                                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> AnimalExpense(id: storeAnimal[i]['id'],))).then((value) {
                                                                      Navigator.of(context).pop();
                                                                    });
                                                                  },
                                                                  child: const Text(
                                                                      'Add Expense'),
                                                                ),
                                                              ),
                                                              const Divider(),
                                                              Material(
                                                                child: InkWell(
                                                                  onTap: () {
                                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> AnimalTreatment(id: storeAnimal[i]['id'],))).then((value) {
                                                                      Navigator.of(context).pop();
                                                                    });
                                                                  },
                                                                  child: const Text(
                                                                      'Treatment'),
                                                                ),
                                                              ),
                                                              const Divider(),
                                                              Material(
                                                                child: InkWell(
                                                                  onTap: () {
                                                                    showDialog(
                                                                        context:
                                                                            context,
                                                                        builder:
                                                                            (context) {
                                                                          return AlertDialog(
                                                                            title:
                                                                                const Text('Delete Animal'),
                                                                            content:
                                                                                SizedBox(
                                                                                  height: 105,
                                                                                  child: Column(
                                                                                    children: [
                                                                                      const Text('Are you sure you want to delete this animal? since all data about this animal will be cleared'),
                                                                                      Row(
                                                                                        mainAxisAlignment: MainAxisAlignment.end,
                                                                                        children: [
                                                                                          ElevatedButton(
                                                                                              style: ElevatedButton.styleFrom(
                                                                                                backgroundColor: Colors.orange
                                                                                              ),
                                                                                              onPressed: (){Navigator.of(context).pop();}, child: const Text('CANCEL')),
                                                                                          const SizedBox(width: 20,),
                                                                                          ElevatedButton(
                                                                                              style: ElevatedButton.styleFrom(
                                                                                                  backgroundColor: Colors.red
                                                                                              ),
                                                                                              onPressed: (){
                                                                                            deleteAnimal(storeAnimal[i]['id']).then((value) {
                                                                                              Navigator
                                                                                                  .of(
                                                                                                  context)
                                                                                                  .pop();
                                                                                            }
                                                                                            );
                                                                                          }, child: const Text('DELETE'))
                                                                                        ],
                                                                                      )
                                                                                    ],
                                                                                  ),
                                                                                ),
                                                                          );
                                                                        }).then((value) => Navigator.of(context).pop());
                                                                  },
                                                                  child: const Text(
                                                                      'Delete'),
                                                                ),
                                                              ),
                                                              const Divider(),
                                                              Material(
                                                                child: InkWell(
                                                                  onTap: () {
                                                                    Navigator.push(context, MaterialPageRoute(builder: (context)=> UpdateAnimal(id: storeAnimal[i]['id'])));
                                                                  },
                                                                  child:
                                                                      const Text(
                                                                          'Edit'),
                                                                ),
                                                              ),
                                                              const Divider(),
                                                            ],
                                                          ),
                                                        ),
                                                      )
                                                    ],
                                                  );
                                                });
                                          },
                                          icon: Icon(
                                            Icons.more_vert,
                                            size: 30,
                                            color: Colors.grey[800],
                                          ))
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ]
                  ],
                ),
              ));
        },
      ),
      // SingleChildScrollView(
      //   child: Container(
      //     padding: const EdgeInsets.all(20),
      //     child: Center(
      //       child: Column(
      //         children: [
      //           const SizedBox(
      //             height: 240,
      //           ),
      //           ElevatedButton(
      //               style: ElevatedButton.styleFrom(
      //                   fixedSize: const Size.fromWidth(150),
      //                   shape: RoundedRectangleBorder(
      //                       borderRadius: BorderRadius.circular(15)),
      //                   backgroundColor: Colors.lightGreen[900]),
      //               onPressed: () {
      //                 showDialog(
      //                     context: context,
      //                     builder: (context) => AlertDialog(
      //                           title: Text(
      //                             'MILK PRODUCTION',
      //                             style: TextStyle(
      //                                 fontStyle: FontStyle.italic,
      //                                 fontWeight: FontWeight.bold,
      //                                 color: Colors.grey[800]),
      //                           ),
      //                           content: SizedBox(
      //                             width: 400,
      //                             height: 250,
      //                             child: Column(
      //                               children: [
      //                                 TextFormField(
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Name Of Cow',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 TextFormField(
      //                                   keyboardType: TextInputType.number,
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Milk Capacity'),
      //                                 ),
      //                                 TextFormField(
      //                                   controller: _dateController,
      //                                   onTap: _handleDatePicker,
      //                                   readOnly: true,
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Date',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 const SizedBox(
      //                                   height: 20,
      //                                 ),
      //                                 Row(
      //                                   mainAxisAlignment:
      //                                       MainAxisAlignment.center,
      //                                   children: [
      //                                     ElevatedButton(
      //                                         style: ElevatedButton.styleFrom(
      //                                             fixedSize:
      //                                                 const Size.fromWidth(90),
      //                                             shape: RoundedRectangleBorder(
      //                                                 borderRadius:
      //                                                     BorderRadius.circular(
      //                                                         15)),
      //                                             backgroundColor:
      //                                                 Colors.lightGreen[900]),
      //                                         onPressed: () {},
      //                                         child: const Text('ADD')),
      //                                     const SizedBox(
      //                                       width: 30,
      //                                     ),
      //                                     ElevatedButton(
      //                                         style: ElevatedButton.styleFrom(
      //                                             fixedSize:
      //                                                 const Size.fromWidth(90),
      //                                             shape: RoundedRectangleBorder(
      //                                                 borderRadius:
      //                                                     BorderRadius.circular(
      //                                                         15)),
      //                                             backgroundColor:
      //                                                 Colors.lightGreen[900]),
      //                                         onPressed: () {},
      //                                         child: const Text('CANCEL')),
      //                                   ],
      //                                 ),
      //                               ],
      //                             ),
      //                           ),
      //                         ));
      //               },
      //               child: const Text('MILK RECORD')),
      //           ElevatedButton(
      //               style: ElevatedButton.styleFrom(
      //                   fixedSize: const Size.fromWidth(150),
      //                   backgroundColor: Colors.lightGreen[900],
      //                   shape: RoundedRectangleBorder(
      //                       borderRadius: BorderRadius.circular(10))),
      //               onPressed: () {
      //                 showDialog(
      //                     context: context,
      //                     builder: (context) => AlertDialog(
      //                           title: Text(
      //                             'VACCINATION PAGE',
      //                             style: TextStyle(
      //                                 fontStyle: FontStyle.italic,
      //                                 color: Colors.grey[800]),
      //                           ),
      //                           content: SizedBox(
      //                             width: 400,
      //                             height: 375,
      //                             child: Column(
      //                               children: [
      //                                 TextFormField(
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Name Of Cow',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 TextFormField(
      //                                   controller: _datesController,
      //                                   readOnly: true,
      //                                   onTap: _handleDatePicker,
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Date Of Vaccination',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 TextFormField(
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Name Of Vaccine',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 TextFormField(
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Cost Of Vaccination',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 TextFormField(
      //                                   decoration: const InputDecoration(
      //                                       labelText: 'Recommendation',
      //                                       labelStyle: TextStyle(
      //                                           fontStyle: FontStyle.italic)),
      //                                 ),
      //                                 const SizedBox(
      //                                   height: 30,
      //                                 ),
      //                                 Row(
      //                                   mainAxisAlignment:
      //                                       MainAxisAlignment.center,
      //                                   children: [
      //                                     ElevatedButton(
      //                                         style: ElevatedButton.styleFrom(
      //                                             fixedSize:
      //                                                 const Size.fromWidth(90),
      //                                             shape: RoundedRectangleBorder(
      //                                                 borderRadius:
      //                                                     BorderRadius.circular(
      //                                                         15)),
      //                                             backgroundColor:
      //                                                 Colors.lightGreen[900]),
      //                                         onPressed: () {},
      //                                         child: const Text('ADD')),
      //                                     const SizedBox(
      //                                       width: 30,
      //                                     ),
      //                                     ElevatedButton(
      //                                         style: ElevatedButton.styleFrom(
      //                                             fixedSize:
      //                                                 const Size.fromWidth(90),
      //                                             shape: RoundedRectangleBorder(
      //                                                 borderRadius:
      //                                                     BorderRadius.circular(
      //                                                         15)),
      //                                             backgroundColor:
      //                                                 Colors.lightGreen[900]),
      //                                         onPressed: () {},
      //                                         child: const Text('CANCEL')),
      //                                   ],
      //                                 ),
      //                               ],
      //                             ),
      //                           ),
      //                         ));
      //               },
      //               child: const Text('VACCINATION'))
      //         ],
      //       ),
      //     ),
      //   ),
      // ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen[800],
        onPressed: () {
          showDialog(
            context: context,
            builder: (context) => AlertDialog(
              title: Text(
                'Add Animal',
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.grey[800],
                    fontWeight: FontWeight.bold),
              ),
              content: SizedBox(
                height: 180,
                width: 70,
                child: Column(
                  children: [
                    Form(
                        key: _formKey,
                        child: Column(
                          children: [
                            TextFormField(
                              controller: _animalType,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[A-Z a-z]').hasMatch(value)) {
                                  return 'Enter valid animal type';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Type of Animal',
                              ),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.number,
                              controller: _noOfAnimal,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[0-9]').hasMatch(value)) {
                                  return 'Enter a valid number';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'No of Animal',
                              ),
                            ),
                            TextFormField(
                              keyboardType: TextInputType.datetime,
                              controller: _dateController,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Pick a valid date';
                                }
                                return null;
                              },
                              onTap: _handleDatePicker,
                              readOnly: true,
                              decoration: const InputDecoration(
                                labelText: 'Date',
                              ),
                            ),
                          ],
                        )),
                  ],
                ),
              ),
              actions: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.lightGreen[800]),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text('Cancel')),
                    const SizedBox(
                      width: 30,
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.lightGreen[800]),
                        onPressed: () {
                          if (_formKey.currentState!.validate()) {
                            setState(() {
                              type = _animalType.text;
                              num = _noOfAnimal.text;
                              date = _dateController.text;
                              addAnimal()
                                  .then((value) => Navigator.of(context).pop());
                            });
                          }
                        },
                        child: const Text('Add'))
                  ],
                )
              ],
            ),
          );
        },
        child: const Icon(
          Icons.add,
          size: 45,
        ),
      ),
    );
  }
}
