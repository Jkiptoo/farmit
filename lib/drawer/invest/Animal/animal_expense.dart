import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/Animal/view_treatment.dart';

class AnimalExpense extends StatefulWidget {
  final String id;
  const
  AnimalExpense({Key? key, required this.id}) : super(key: key);

  @override
  State<AnimalExpense> createState() => _AnimalExpenseState();
}

class _AnimalExpenseState extends State<AnimalExpense> {
  var firebaseUser = FirebaseAuth.instance.currentUser;
  final _formKey = GlobalKey<FormState>();
  final _descriptionController = TextEditingController();
  final _dateController = TextEditingController();
  final _costController = TextEditingController();

  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDate() async{
    DateTime? date = await showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2023), lastDate: DateTime(2070));
    if(date != null){
      _dateController.text = _dateFormatter.format(date);
    }
  }


  CollectionReference ref = FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');

  // ignore: avoid_types_as_parameter_names
  Future<void> getAnimal(id, type, num) async{

    ref.doc(id).update({
      'type': type,
      'num': num
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        leading: IconButton(icon: const Icon(Icons.arrow_back), onPressed: (){
          Navigator.pushNamedAndRemoveUntil(context, '/animal', (route) => false);
        },),
        title: const Text('Expense', ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(15),
          child: Form(child:
          FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
              future: FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data').doc(widget.id).get(),
              builder: (_, snapshot){
                if(snapshot.hasError){
                  return const Center(
                    child: Text('Something went wrong!'),
                  );
                }
                if(snapshot.connectionState == ConnectionState.waiting){
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }
                var data = snapshot.data!.data();
                var type = data!['type'];

                return Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        onChanged: (value){
                          type = value;
                        },
                        initialValue: type,
                        readOnly: true,
                        validator: (value) {
                          if(value == null || ! RegExp(r'^[a-z A-Z]+$').hasMatch(value)){
                            return 'Enter a valid type';
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                            labelText: 'type', border: OutlineInputBorder()
                        ),
                      ),const SizedBox(
                        height: 20,
                      ),

                      TextFormField(
                        controller: _costController,
                        validator: (value) {
                          if(value == null || ! RegExp(r'^[0-9]+$').hasMatch(value)){
                            return 'Enter a valid cost';
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                            labelText: 'Cost', border: OutlineInputBorder()
                        ),
                      ),

                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        validator: (value) {
                          if (value != null &&
                              value.isEmpty) {
                            return "Date can't be null";
                          }
                          return null;
                        },
                        readOnly: true,
                        controller: _dateController,
                        onTap: _handleDate,
                        decoration: const InputDecoration(
                          labelText: 'Date',
                          border: OutlineInputBorder(),
                        ),
                      ),
                      const SizedBox(
                        height: 20,
                      ),
                      TextFormField(
                        maxLines: 5,
                        controller: _descriptionController,
                        validator: (value) {
                          if(value == null || ! RegExp(r'^[a-z A-Z]+$').hasMatch(value)){
                            return 'Enter a valid description';
                          }
                          return null;
                        },
                        decoration: const InputDecoration(
                            labelText: 'Description', border: OutlineInputBorder()
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.lightGreen[900],
                              minimumSize: const Size(double.infinity, 50)
                          ),
                          onPressed: (){
                            if(_formKey.currentState!.validate()){
                              FirebaseFirestore.instance.collection('animal').doc('data').collection('expense').doc().set(
                                  {
                                    'rid': firebaseUser?.uid,
                                    'type': type,
                                    'cost': _costController.text,
                                    'date': _dateController.text,
                                    'desc': _descriptionController.text,
                                  }).then((value) {
                                ScaffoldMessenger.of(context).showSnackBar(const SnackBar(content: Text('Expense Successfully added')));
                                clearText();
                                type == null;
                                Navigator.push(context, MaterialPageRoute(builder: (context)=> const ViewTreatment()));
                              });
                            }
                          }, child: const Text('Save'))
                    ],
                  ),
                );
              })),
        ),
      ),
    );
  }
  clearText(){
    _dateController.clear();
    _costController.clear();
    _descriptionController.clear();
  }
}
