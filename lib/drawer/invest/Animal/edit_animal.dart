import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/Animal/animal.dart';

class UpdateAnimal extends StatefulWidget {
  final String id;
  const UpdateAnimal({Key? key, required this.id}) : super(key: key);

  @override
  State<UpdateAnimal> createState() => _UpdateAnimalState();
}

class _UpdateAnimalState extends State<UpdateAnimal> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference update = FirebaseFirestore.instance.collection('animal').doc('data').collection('animal-data');
  // ignore: avoid_types_as_parameter_names
  Future<void> animalUpdate(id, type, num) async {
    update.doc(id).update({
      'type': type,
      'num': num,

    }).then((value) => ScaffoldMessenger.of(context).showSnackBar(
        const SnackBar(content: Text('Animal Successfully Updated'))));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Edit Data'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('animal')
                  .doc('data')
                  .collection('animal-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Center(
                        child: Text('Something Went Wrong'),
                      );
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      ) ;
                    }
                    var data = snapshot.data!.data();
                    var type = data!['type'];
                    var num = data['num'];

                    return Column(
                      children: [
                        TextFormField(
                          onChanged: (val) {
                            type = val;
                          },
                          initialValue: type,
                          validator: (value) {
                            if (value == null ||
                                !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                              return 'Please Enter a valid Type';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Type', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        TextFormField(
                          onChanged: (val) {
                            num = val;
                          },
                          initialValue: num,
                          validator: (value) {
                            if (value!.isEmpty &&
                                !RegExp(r'^[0-9]').hasMatch(value)) {
                              return 'Enter a valid number';
                            }
                            return null;
                          },
                          decoration: const InputDecoration(
                              labelText: 'Number', border: OutlineInputBorder()),
                        ),
                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                  minimumSize: const Size(375, 50),
                                    backgroundColor: Colors.lightGreen[900],
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(10)),
                                    elevation: 2, // Adjust the elevation as needed
                                    shadowColor: Colors.black,
                                ),
                                onPressed: () {
                                  if (_formKey.currentState!.validate()) {
                                    animalUpdate(widget.id, type, num,
                                    )
                                        .then((value) => Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                            const ManageAnimal())));
                                  }
                                },
                                child: const Text('Edit',
                                  style: TextStyle(
                                    fontSize: 18,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white,
                                  ),))
                          ],
                        )
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),

    );
  }
}
