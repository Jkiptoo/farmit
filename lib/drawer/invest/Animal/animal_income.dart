import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/invest/Animal/view_animal_income.dart';


class AnimalIncome extends StatefulWidget {
  final String id;
  const AnimalIncome({Key? key, required this.id}) : super(key: key);

  @override
  State<AnimalIncome> createState() => _FarmIncomeState();
}

class _FarmIncomeState extends State<AnimalIncome> {
  final _formKey = GlobalKey<FormState>();
  CollectionReference<Map<String, dynamic>> update = FirebaseFirestore.instance
      .collection('animal')
      .doc('data')
      .collection('animal-data');

  String animalName = '';

  final _dateController = TextEditingController();
  final _costController = TextEditingController();
  final _descController = TextEditingController();

  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDate() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now(),
        builder: (BuildContext context, Widget? child) {
          return Theme(
              data: ThemeData.light().copyWith(
                colorScheme: const ColorScheme.light().copyWith(
                  // Customize the background color of the date picker
                  background: Colors.lightGreen[900],
                ),
                // Customize the text style of the date picker
                textTheme: const TextTheme(
                  // Customize the text style of the header/title
                  titleLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the selected date
                  bodyMedium: TextStyle(
                    color: Colors.white,
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                  // Customize the text style of the unselected dates
                  bodyLarge: TextStyle(
                    color: Colors.black,
                    fontSize: 18,
                  ),
                ),
              ),
              child: child!);
        });
    if (date != null) {
      _dateController.text = _dateFormatter.format(date);
    }
  }



  clearForm() {
    _costController.clear();
    _dateController.clear();
    _descController.clear();
     }


  // ignore: prefer_typing_uninitialized_variables
  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Add Income'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: FutureBuilder<DocumentSnapshot<Map<String, dynamic>>>(
                  future: FirebaseFirestore.instance
                      .collection('animal')
                      .doc('data')
                      .collection('animal-data')
                      .doc(widget.id)
                      .get(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Text('Something Went Wrong');
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const CircularProgressIndicator();
                    }
                    var data = snapshot.data!.data();
                    animalName = data!['type'];

                    return Column(
                      children: [
                        Column(
                          children: [
                            TextFormField(
                              onChanged: (val) {
                                setState(() {
                                  animalName = val;
                                });
                              },
                              readOnly: true,
                              initialValue: animalName,
                              validator: (value) {
                                if (value == null ||
                                    !RegExp(r'^[a-zA-Z ]+$').hasMatch(value)) {
                                  return 'Please enter a valid Animal name';
                                }
                                return null;
                              },
                              decoration: const InputDecoration(
                                labelText: 'Farm Name',
                                border: OutlineInputBorder(),
                              ),
                            ),

                            const SizedBox(
                              height: 20,
                            ),
                            TextFormField(
                              controller: _costController,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[0-9]+$').hasMatch(value)) {
                                  return 'Enter a valid cost';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.number,
                              decoration: const InputDecoration(
                                hintText: 'Amount',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              validator: (value) {
                                if (value != null && value.isEmpty) {
                                  return "Date can't be null";
                                }
                                return null;
                              },
                              readOnly: true,
                              controller: _dateController,
                              onTap: _handleDate,
                              decoration: const InputDecoration(
                                labelText: 'Date',
                                border: OutlineInputBorder(),
                              ),
                            ),
                            const SizedBox(height: 20),
                            TextFormField(
                              maxLines: 5,
                              controller: _descController,
                              validator: (value) {
                                if (value!.isEmpty &&
                                    !RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                                  return 'Enter a valid description';
                                }
                                return null;
                              },
                              keyboardType: TextInputType.text,
                              decoration: const InputDecoration(
                                hintText: 'Description',
                                border: OutlineInputBorder(),
                              ),
                            ),
                          ],
                        ),

                        const SizedBox(
                          height: 20,
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.lightGreen[900],
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(4),
                                  ),
                                  minimumSize: const Size(120, 40)
                              ),
                              onPressed: () {
                                if (_formKey.currentState!.validate()) {
                                  FirebaseFirestore.instance
                                      .collection('animal')
                                      .doc('data')
                                      .collection('animal-income')
                                      .doc()
                                      .set({
                                    'rid': firebaseUser?.uid,
                                    'animal': animalName,
                                    'desc': _descController.text,
                                    'cost': _costController.text,
                                    'date': _dateController.text,
                                  })
                                  // ignore: avoid_print
                                      .then((value) {
                                    clearForm();
                                    Navigator.push(context, MaterialPageRoute(builder: (context)=>ViewAnimalIncome(id: widget.id)));
                                  }).catchError((error) {
                                    // ignore: avoid_print
                                    print('Something went wrong: $error');
                                  });
                                }
                              },
                              child: const Text('Save', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.2),),
                            ),
                            ElevatedButton(
                                style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4),),
                                    backgroundColor: Colors.lightGreen[900],
                                    minimumSize: const Size(120, 40)
                                ),
                                onPressed: (){
                                  Navigator.push(context, MaterialPageRoute(builder: (context)=> ViewAnimalIncome(id: widget.id)));

                                }, child: const Text('View', style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.white, letterSpacing: 1.2),))
                          ],
                        ),
                      ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
