import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/drawer/invest/Animal/animal_income.dart';

class ViewExpense extends StatefulWidget {
  final String id;
  const ViewExpense({Key? key, required this.id}) : super(key: key);

  @override
  State<ViewExpense> createState() => _ViewFarmState();
}

class _ViewFarmState extends State<ViewExpense> {
  final _formKey = GlobalKey<FormState>();
  var firebaseUser = FirebaseAuth.instance.currentUser;


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
        title: const Text('Extra Expense'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              Form(
                key: _formKey,
                child: StreamBuilder<QuerySnapshot<Map<String, dynamic>>>(
                  stream: getOwnedDocumentsStream(),
                  builder: (_, snapshot) {
                    if (snapshot.hasError) {
                      return const Center(
                        child: Text('Something Went Wrong'),
                      );
                    }
                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return const Center(
                        child: CircularProgressIndicator(),
                      );
                    }

                    // Extract the filtered documents from the snapshot
                    var documents = snapshot.data!.docs;

                    // Display the filtered documents
                    return ListView.builder(
                      shrinkWrap: true,
                      itemCount: documents.length,
                      itemBuilder: (_, index) {
                        var data = documents[index].data();
                        var date = data['date'] ?? '';
                        var desc = data['desc'] ?? '';
                        var cost = data['expense'] ?? '';

                        return SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Container(
                            padding: const EdgeInsets.all(0),
                            child: Column(
                              children: [
                                SizedBox(
                                  height: 200,
                                  width: double.infinity,
                                  child: Card(
                                    child: InkWell(
                                      onTap: () {

                                      },
                                      child: SingleChildScrollView(
                                        scrollDirection: Axis.horizontal,
                                        child: Row(
                                          children: [
                                            const SizedBox(
                                              width: 20,
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                              children: [

                                                Text(
                                                  "Expense:",
                                                  style: TextStyle(
                                                    color: Colors.grey[800],
                                                  ),
                                                ),
                                                Text(
                                                  "Date:",
                                                  style: TextStyle(
                                                    color: Colors.grey[800],
                                                  ),
                                                ),
                                                Text(
                                                  "Description:",
                                                  style: TextStyle(
                                                    color: Colors.grey[800],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              width: 35,
                                            ),
                                            Column(
                                              crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                              MainAxisAlignment
                                                  .spaceBetween,
                                              children: [

                                                Text(
                                                  cost,
                                                  style: TextStyle(
                                                    color: Colors.green[800],
                                                  ),
                                                ),

                                                Text(
                                                  date,
                                                  style: TextStyle(
                                                    color: Colors.green[800],
                                                  ),
                                                ),
                                                Text(
                                                  desc,
                                                  style: TextStyle(
                                                    color: Colors.green[800],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            const SizedBox(
                                              width: 100,
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      },
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.lightGreen[900],
        onPressed: (){
          Navigator.push(context, MaterialPageRoute(builder: (context)=>AnimalIncome(id: widget.id)));
        },
        child: const Icon(Icons.add, size: 35,),
      ),
    );
  }

  Stream<QuerySnapshot<Map<String, dynamic>>> getOwnedDocumentsStream() {
    // Create a reference to the parent document
    DocumentReference<Map<String, dynamic>> parentDocRef =
    FirebaseFirestore.instance.collection('expense').doc('data');

    // Create a reference to the subcollection
    CollectionReference<Map<String, dynamic>> subcollectionRef =
    parentDocRef.collection('extra-expense');

    // Create a query to filter documents where 'id' field is equal to the widget id
    Query<Map<String, dynamic>> query = subcollectionRef.where('rid', isEqualTo: firebaseUser?.uid
    );

    // Return the stream of documents in the filtered subcollection
    return query.snapshots();
  }
}

