import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/drawer/transaction/view_expense.dart';

class ExpenseApp extends StatefulWidget {
  const ExpenseApp({Key? key}) : super(key: key);

  @override
  State<ExpenseApp> createState() => _ExpenseAppState();
}

class _ExpenseAppState extends State<ExpenseApp> {
  final _formKey = GlobalKey<FormState>();

  final _costController = TextEditingController();
  final _dateController = TextEditingController();
  final _descController = TextEditingController();

  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');

  _handleDatePicker() async {
    final DateTime? date = await showDatePicker(
        context: context,
        initialDate: _time,
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    } else {
      // ignore: use_build_context_synchronously
      ScaffoldMessenger.of(context)
          .showSnackBar(const SnackBar(content: Text('No Date Picked')));
    }
    _dateController.text = _dateFormatter.format(date!);
  }

  clearForm(){
    _costController.clear();
    _dateController.clear();
    _descController.clear();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Additional Expense'),
        backgroundColor: Colors.lightGreen[900],
      ),
      body: SingleChildScrollView
        (
        child: Padding(
          padding: const EdgeInsets.all(10),
          child: Form(
              key: _formKey,
              child: Column(
                children: [
                  const SizedBox(height: 20),
                  TextFormField(
                    controller: _costController,
                    validator: (value) {
                      if (value!.isEmpty &&
                          !RegExp(r'^[0-9]+$')
                              .hasMatch(value)) {
                        return 'Enter a valid ammount';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      hintText: 'Cost',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    validator: (value) {
                      if (value != null &&
                          value.isEmpty) {
                        return "Date can't be null";
                      }
                      return null;
                    },
                    readOnly: true,
                    controller: _dateController,
                    onTap: _handleDatePicker,
                    decoration: const InputDecoration(
                      labelText: 'Date Added',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 20),
                  TextFormField(
                    maxLines: 5,
                    controller: _descController,
                    validator: (value) {
                      if (value!.isEmpty &&
                          !RegExp(r"^[A-Z a-z]+$")
                              .hasMatch(value)) {
                        return 'Enter a valid description';
                      }
                      return null;
                    },
                    keyboardType: TextInputType.text,
                    decoration: const InputDecoration(
                      hintText: 'Description',
                      border: OutlineInputBorder(),
                    ),
                  ),
                  const SizedBox(height: 20),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize: const Size(120, 40),
                              backgroundColor:
                              Colors.lightGreen[900],
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.circular(
                                      7))),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {

                              FirebaseFirestore.instance
                                  .collection('expense')
                                  .doc('data')
                                  .collection('extra-expense')
                                  .doc()
                                  .set({
                                'rid': FirebaseAuth.instance.currentUser?.uid,
                                'expense': _costController.text,
                                'desc': _descController.text,
                                'cost': _costController.text,
                                'date': _dateController.text,
                              })
                              // ignore: avoid_print
                                  .then((value) {
                                clearForm();
                                Navigator.push(context, MaterialPageRoute(builder: (context)=>const ViewExpense(id: '')));
                              }).catchError((error) {
                                // ignore: avoid_print
                                print('Something went wrong: $error');
                              });
                            }
                          },
                          child: const Text('Save')),
                      ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              minimumSize: const Size(120, 40),
                              backgroundColor:
                              Colors.lightGreen[900],
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                  BorderRadius.circular(
                                      7))),
                          onPressed: (){
                            Navigator.push(context, MaterialPageRoute(builder: (context)=>const ViewExpense(id: '')));
                      }, child: const Text('View'))
                    ],
                  )
                ],
              )),
        ),
      ),
    );
  }
}
