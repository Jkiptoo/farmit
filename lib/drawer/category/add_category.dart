import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:meek/farm_dash/home.dart';
import 'package:meek/drawer/invest/crop/add_crop.dart';

class AddCategory extends StatefulWidget {
  const AddCategory({Key? key}) : super(key: key);

  @override
  State<AddCategory> createState() => _AddCategoryState();
}

class _AddCategoryState extends State<AddCategory> {
  final _dateController = TextEditingController();
  final DateTime _time = DateTime.now();
  final _dateFormatter = DateFormat('MMM dd yyyy');
  _handleDate() async {
    DateTime? date = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime.now(),
        lastDate: DateTime.now());
    if (date != null && date != _time) {
      date;
    }
    _dateController.text = _dateFormatter.format(date!);
  }

  var category = '';
  var dates = '';
  final _formKey = GlobalKey<FormState>();
  final _categoryName = TextEditingController();
  CollectionReference reference =
      FirebaseFirestore.instance.collection('category');
  Future<void> categoryAdd() async {
    return await reference.add({
      'rid': firebaseUser?.uid,
      'category': category,
    'dates': dates,
    }).then((value) {
      ScaffoldMessenger.of(context).showSnackBar(
          const SnackBar(content: Text('Category added Successfully')));
    }).catchError((error) {
      Text(('Error message: $error'));
    });
  }
  var firebaseUser = FirebaseAuth.instance.currentUser;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => const HomePage()));
            },
            icon: const Icon(Icons.arrow_back)),
        title: const Text('Add Category'),
        backgroundColor: Colors.lightGreen[900],
        centerTitle: true,
      ),
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestore.instance.collection('category').where('rid', isEqualTo: FirebaseAuth.instance.currentUser?.uid).snapshots(),
        builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
          final List storeCategory = [];
          snapshot.data?.docs.map((DocumentSnapshot documentSnapshot) {
            Map c = documentSnapshot.data() as Map<String, dynamic>;
            storeCategory.add(c);
            c['id'] = documentSnapshot.id;
          }).toList();
          return SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Container(
              padding: const EdgeInsets.all(10),
              child: Column(
                children: [
                  for (var i = 0; i < storeCategory.length; i++) ...[
                    SizedBox(
                      height: 70,
                      width: 370,
                      child: Material(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> const AddCrop()));
                          },
                          child: Card(
                            child: SingleChildScrollView(
                              scrollDirection: Axis.horizontal,
                              child: Row(
                                children: [
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly                                                                      ,
                                    children: [
                                      Text(
                                        "Category:",
                                        style: TextStyle(color: Colors.grey[800]),
                                      ),
                                      Text(
                                        "Date:",
                                        style: TextStyle(color: Colors.grey[800]),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 35,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children: [
                                      Text(
                                        storeCategory[i]['category'],
                                        style: TextStyle(
                                          color: Colors.green[800],
                                        ),
                                      ),
                                      Text(storeCategory[i]['dates'],style: TextStyle(
                                          color: Colors.green[800]
                                      ))
                                    ],
                                  ),
                                  const SizedBox(
                                    width: 100,
                                  ),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      IconButton(
                                          onPressed: () {
                                            showDialog(context: context, builder: (BuildContext context){
                                              return Container(
                                                height: 100,
                                              );
                                            });
                                          },
                                          icon: Icon(Icons.more_vert, color: Colors.grey[800], size: 40,))
                                    ],
                                  )
                                ],
                              ),
                            )
                          ),
                        ),
                      ),
                    )
                  ]
                ],
              ),
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.green[900],
        onPressed: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: Text(
                    'Add Category',
                    style: TextStyle(fontSize: 20, color: Colors.grey[800]),
                  ),
                  content: Form(
                    key: _formKey,
                    child: SizedBox(
                        height: 180,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.vertical,
                          child: Column(
                            children: [
                              TextFormField(
                                controller: _categoryName,
                                decoration: const InputDecoration(
                                  labelText: 'Category name',
                                ),
                              ),
                              TextFormField(
                                validator: (value){
                                  if(value!.isEmpty){
                                    return 'Date cannot be null';
                                  }
                                  return null;
                                },
                                readOnly: true,
                                controller: _dateController,
                                onTap: _handleDate,
                                decoration: const InputDecoration(
                                  labelText: 'Date',
                                ),
                              ),
                              const SizedBox(
                                height: 20,
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                          backgroundColor:
                                              Colors.lightGreen[900],
                                          shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5))),
                                      onPressed: () {
                                        if (_formKey.currentState!.validate()) {
                                          setState(() {
                                            category = _categoryName.text;
                                            dates = _dateController.text;
                                            categoryAdd().then((value) {
                                              Navigator.of(context).pop();
                                            });
                                          });
                                        }
                                      },
                                      child: const Text('Add')),
                                  const SizedBox(
                                    width: 20,
                                  ),
                                  ElevatedButton(
                                      style: ElevatedButton.styleFrom(
                                        backgroundColor: Colors.lightGreen[900],
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                      ),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                      child: const Text(
                                        'Cancel',
                                        style: TextStyle(
                                          fontSize: 18,
                                        ),
                                      ))
                                ],
                              )
                            ],
                          ),
                        )),
                  ),
                );
              });
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
