import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:meek/authentication/users.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  State<SignUp> createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  String? _email;
  String? _password;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final _nameController = TextEditingController();
  final _emailController = TextEditingController();
  final _phoneController = TextEditingController();
  bool isLoading = false;
  bool hideText = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                  children: [
              Container(
              padding: const EdgeInsets.all(30),
              height: 250,
              child: const Image(
                image: AssetImage('assets/icons/add-user.png'),
              ),
            ),
            Container(
                margin: const EdgeInsets.all(20),
                child: TextFormField(
                    controller: _nameController,
                    onSaved: (value) {
                      _nameController.text = value!;
                    },
                    validator: (value) {
                      if (value!.isEmpty ||
                          !RegExp(r'^[a-z A-Z]+<span class="math-inline">').hasMatch(value)){
                      return 'Please Enter Correct Name';
                      } else {
                      return null;
                      }
                      },
                    decoration: InputDecoration(
                labelText:'User',
                border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(20))),
        ),
    ),
    Container(
    margin: const EdgeInsets.all(20),
    child: TextFormField(
    controller: _emailController,
    onSaved: (value) {
    _emailController.text = value!;
    },
    onChanged: (value) {
    setState(() {
    _email = value;
    });
    },
    validator: (value) {
    if (value!.isEmpty ||
    !RegExp(r'^\[\\w\-\]\+\(\\\.\[\\w\-\]\+\)\*@\(\[\\w\-\]\+\\\.\)\+\[a\-zA\-Z0\-9\]\{2,7\}</span>')
        .hasMatch(value)) {
    return 'Enter a valid email address';
    } else {
    return null;
    }
    },
    decoration: InputDecoration(
    labelText: 'Email',
    border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(20))),
    ),
    ),
    Container(
    margin: const EdgeInsets.all(20),
    child: TextFormField(
    controller: _phoneController,
    onSaved: (val) {
    _phoneController.text = val!;
    },
    keyboardType: TextInputType.phone,
    validator: (value) {
    if (value!.isEmpty ||
    !RegExp(r'^(?:[+0]9)?[0-9]{10}$').hasMatch(value)) {
    return 'Please enter a valid phone number';
    }
    return null;
    },
    decoration: InputDecoration(
    labelText: 'Phone Number',
    hintText: '0712345678',
    border: OutlineInputBorder(
    borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  child: TextFormField(
                    onChanged: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                    validator: (value) {
                      String missings = '';
                      if (value!.length < 6) {
                        missings += "Password needs at least 6 characters\n";
                      }
                      //if there is password input errors return error string
                      if (missings != "") {
                        return missings;
                      } else {
                        return null;
                      }
                    },
                    obscureText: hideText,
                    decoration: InputDecoration(
                        labelText: 'Password',
                        suffixIcon: GestureDetector(
                          onTap: () {
                            setState(() {
                              hideText = !hideText;
                            });
                            FocusScope.of(context).unfocus();
                          },
                          child: Icon(
                            hideText == false
                                ? Icons.visibility
                                : Icons.visibility_off,
                            color: Colors.grey[800],
                          ),
                        ),
                        border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(20))),
                  ),
                ),
                !isLoading
                    ? Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          ElevatedButton(
                              style: ElevatedButton.styleFrom(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(5),
                                  ),
                                  backgroundColor: Colors.lightGreen[900]),
                              onPressed: () async {
                                if (_formKey.currentState!.validate()) {
                                  setState(() {
                                    isLoading = true;
                                  });

                                  try {
                                    UserCredential userCredential =
                                        await FirebaseAuth.instance
                                            .createUserWithEmailAndPassword(
                                      email: _email!,
                                      password: _password!,
                                    );

                                    String userId = userCredential.user!.uid;

                                    await FirebaseFirestore.instance
                                        .collection('users')
                                        .doc(userId)
                                        .set({
                                      'user': _nameController.text,
                                      'email': _emailController.text,
                                      'phone': _phoneController.text,
                                      'userId':
                                          userId, // Store the user ID in Firestore
                                    }).then((value){
                                      UserManagement().storeNewUser(
                                          userCredential.user!, context);
                                      ScaffoldMessenger.of(context).showSnackBar(
                                        SnackBar(
                                          content: Text(
                                            'User registered successfully',
                                            style: TextStyle(
                                                color: Colors.grey[400]),
                                          ),
                                        ),
                                      );
                                    });
                                  } catch (e) {
                                    showDialog(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return AlertDialog(
                                          title: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                "Error",
                                                style: TextStyle(
                                                  color: Colors.red[300],
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 20,
                                                ),
                                              ),
                                            ],
                                          ),
                                          content: Text(
                                            '$e',
                                            style: TextStyle(
                                              color: Colors.grey[700],
                                              fontSize: 17,
                                            ),
                                          ),
                                          actions: [
                                            Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: [
                                                ElevatedButton(
                                                  style:
                                                      ElevatedButton.styleFrom(
                                                    backgroundColor:
                                                        Colors.lightGreen[700],
                                                    shape:
                                                        RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              2),
                                                    ),
                                                  ),
                                                  onPressed: () {
                                                    Navigator.of(context).pop();
                                                  },
                                                  child: Text(
                                                    'OK',
                                                    style: TextStyle(
                                                      color: Colors.grey[300],
                                                      fontSize: 16,
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            )
                                          ],
                                        );
                                      },
                                    );
                                  } finally {
                                    setState(() {
                                      isLoading = false;
                                    });
                                  }
                                }
                              },
                              child: const Text(
                                'Register',
                                style: TextStyle(
                                  fontStyle: FontStyle.normal,
                                  fontSize: 20,
                                ),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              const Text('Already with Account?'),
                              TextButton(
                                  onPressed: () {
                                    Navigator.of(context)
                                        .pushNamedAndRemoveUntil(
                                            '/login', (route) => false);
                                  },
                                  child: Text(
                                    ' LOGIN',
                                    style: TextStyle(
                                        color: Colors.lightGreen[800]),
                                  ))
                            ],
                          )
                        ],
                      )
                    : const Center(
                        child: CircularProgressIndicator(),
                      )
              ],
            )),
      ),
    );
  }
}
