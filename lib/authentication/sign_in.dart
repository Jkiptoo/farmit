import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({Key? key}) : super(key: key);

  @override
  _SignInPageState createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  String? _email;
  String? _password;
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  bool? newUser;
  bool hideText = true;
  final _auth = FirebaseAuth.instance;
  String get currUid => FirebaseAuth.instance.currentUser?.uid ?? "";

  Widget _buildEmail() {
    return TextFormField(
      validator: (value) {
        if (value!.isEmpty ||
            !RegExp(r'^[\w-]+(\.[\w-]+)*@([\w-]+\.)+[a-zA-Z]{2,7}$')
                .hasMatch(value)) {
          return 'Enter a valid email address';
        } else {
          return null;
        }
      },
      controller: _emailController,
      onSaved: (value) {
        _emailController.text = value!;
      },
      onChanged: (val) {
        setState(() {
          _email = val;
        });
      },
      decoration: InputDecoration(
        labelText: 'Email',
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
    );
  }

  Widget _buildPassword() {
    return TextFormField(
      onChanged: (val) {
        setState(() {
          _password = val;
        });
      },
      controller: _passwordController,
      decoration: InputDecoration(
        labelText: 'Password',
        suffixIcon: GestureDetector(
          onTap: () {
            setState(() {
              hideText = !hideText;
            });
            FocusScope.of(context).unfocus();
          },
          child: Icon(
            hideText == false ? Icons.visibility : Icons.visibility_off,
            color: Colors.grey[800],
          ),
        ),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(10)),
      ),
      validator: (value) {
        String missings = '';
        if (value!.length < 6) {
          missings += "Password needs at least 6 characters\n";
        }
        //if there is password input errors return error string
        if (missings.isNotEmpty) {
          return missings;
        } else {
          return null;
        }
      },
      obscureText: hideText,
    );
  }

  @override
  void initState() {
    super.initState();
    // Check if user is logged in
    _checkIfUserLoggedIn();
  }

  void _checkIfUserLoggedIn() async {
    // Check if user is already logged in
    bool isLoggedIn = await _auth.currentUser != null;
    if (isLoggedIn) {
      // Navigate to home page if user is logged in
      Navigator.of(context).pushNamedAndRemoveUntil(
        '/home',
            (route) => false,
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Container(
                padding: const EdgeInsets.all(50),
                height: 250,
                child: const Image(
                  image: AssetImage('assets/icons/key.png'),
                ),
              ),
              Container(
                padding: const EdgeInsets.all(20),
                child: Form(
                  key: _formKey,
                  child: Column(
                    children: <Widget>[
                      Column(
                        children: <Widget>[
                          _buildEmail(),
                          const SizedBox(height: 20),
                          _buildPassword(),
                        ],
                      ),
                      const SizedBox(height: 20),
                      ButtonTheme(
                        minWidth: 150,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.lightGreen[900],
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5),
                            ),
                          ),
                          onPressed: () {
                            if (_formKey.currentState!.validate()) {
                              String useremail = _emailController.text;
                              if (useremail.isNotEmpty) {
                                print('Successful');
                                print(currUid);
                              }
                              _auth
                                  .signInWithEmailAndPassword(
                                email: _email!,
                                password: _password!,
                              )
                                  .catchError((err) {
                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return AlertDialog(
                                      title: const Text("Error"),
                                      content: Text("$err"),
                                      actions: [
                                        ElevatedButton(
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                          },
                                          child: const Text("Ok"),
                                        ),
                                      ],
                                    );
                                  },
                                );
                              }).then((value) {
                                Navigator.of(context).pushNamedAndRemoveUntil(
                                  '/home',
                                      (route) => false,
                                );
                              });
                            }
                          },
                          child: Text(
                            'LOGIN',
                            style: TextStyle(
                              color: Colors.grey[200],
                              fontSize: 18,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                '/reset',
                                    (route) => false,
                              );
                            },
                            child: Text(
                              'Forgot Password?',
                              style: TextStyle(
                                color: Colors.lightGreen[600],
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            'Without Account?',
                            style: TextStyle(
                              color: Colors.grey[800],
                              fontSize: 17,
                            ),
                          ),
                          TextButton(
                            onPressed: () {
                              Navigator.of(context).pushNamedAndRemoveUntil(
                                '/register',
                                    (route) => false,
                              );
                            },
                            child: Text(
                              'Sign Up',
                              style: TextStyle(
                                color: Colors.lightGreen[700],
                                fontSize: 18,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
