import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:get/get.dart';
import 'package:meek/authentication/sign_in.dart';

class UserManagement {
  storeNewUser(user, context) async {
    // ignore: await_only_futures
    var firebaseUser = await FirebaseAuth.instance.currentUser;
    FirebaseFirestore.instance
        .collection('users')
        .doc(firebaseUser!.uid)
        .set({"email": user.email, 'uid': user.uid}).then(
            (value) => Get.to(() => const SignInPage()));
  }
}
